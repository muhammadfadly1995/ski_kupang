-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Okt 2022 pada 20.20
-- Versi server: 10.4.24-MariaDB
-- Versi PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ski2`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `billing`
--

CREATE TABLE `billing` (
  `id` int(11) NOT NULL,
  `no_billing` varchar(255) NOT NULL,
  `subtotal` float NOT NULL DEFAULT 0,
  `status_billing` int(11) NOT NULL DEFAULT 0,
  `id_top` int(11) DEFAULT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `tanggal_billing` date DEFAULT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0,
  `no_line` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `billing`
--

INSERT INTO `billing` (`id`, `no_billing`, `subtotal`, `status_billing`, `id_top`, `create_by`, `create_date`, `tanggal_billing`, `update_by`, `update_date`, `delete_mark`, `no_line`) VALUES
(1, 'B-000000001', 6993000, 1, 1, 'admin', '2022-10-10', '2022-10-10', 'admin', '2022-10-10', 0, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `child_billing`
--

CREATE TABLE `child_billing` (
  `id` int(11) NOT NULL,
  `id_billing` int(11) NOT NULL,
  `no_spj` int(11) NOT NULL,
  `subtotal` float NOT NULL DEFAULT 0,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `child_billing`
--

INSERT INTO `child_billing` (`id`, `id_billing`, `no_spj`, `subtotal`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`) VALUES
(1, 1, 1, 6993000, 'admin', '2022-10-10', 'admin', '2022-10-10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `child_matching`
--

CREATE TABLE `child_matching` (
  `id` int(11) NOT NULL,
  `no_do` varchar(255) NOT NULL,
  `id_matching` int(11) NOT NULL,
  `id_so` int(11) NOT NULL,
  `id_child_so` int(11) NOT NULL,
  `quantity` float NOT NULL,
  `harga` float NOT NULL,
  `id_tax` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `status` int(11) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0,
  `line_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `child_matching`
--

INSERT INTO `child_matching` (`id`, `no_do`, `id_matching`, `id_so`, `id_child_so`, `quantity`, `harga`, `id_tax`, `subtotal`, `status`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`, `line_order`) VALUES
(1, 'DO-000000001', 1, 1, 1, 60, 70000, 1, 4662000, 0, 'admin', '2022-10-10', 'admin', '2022-10-10', 0, 1),
(2, 'DO-000000002', 1, 1, 2, 30, 70000, 1, 2331000, 0, 'admin', '2022-10-10', 'admin', '2022-10-10', 0, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `child_o_pp_ski`
--

CREATE TABLE `child_o_pp_ski` (
  `ID` int(11) NOT NULL,
  `id_pp` int(11) NOT NULL,
  `id_soldto` int(11) NOT NULL,
  `id_shipto` int(11) NOT NULL,
  `id_plant` int(11) NOT NULL,
  `id_material` int(11) NOT NULL,
  `id_angkut` int(11) NOT NULL,
  `quantity` float NOT NULL,
  `sisa_quantity` float NOT NULL,
  `date_order` date NOT NULL,
  `time_order` time NOT NULL,
  `date_receipt` date NOT NULL,
  `id_top` int(11) NOT NULL,
  `harga` float NOT NULL,
  `id_tax` int(11) NOT NULL,
  `subtotal` float NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `time_create` time NOT NULL,
  `uom` varchar(255) NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0,
  `status_konfirmasi` varchar(255) NOT NULL,
  `approval_by` varchar(255) NOT NULL,
  `approval_date` varchar(255) NOT NULL,
  `approval_reject` varchar(255) NOT NULL,
  `approval_date_reject` date DEFAULT NULL,
  `line_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `child_o_pp_ski`
--

INSERT INTO `child_o_pp_ski` (`ID`, `id_pp`, `id_soldto`, `id_shipto`, `id_plant`, `id_material`, `id_angkut`, `quantity`, `sisa_quantity`, `date_order`, `time_order`, `date_receipt`, `id_top`, `harga`, `id_tax`, `subtotal`, `create_by`, `create_date`, `time_create`, `uom`, `delete_mark`, `status_konfirmasi`, `approval_by`, `approval_date`, `approval_reject`, `approval_date_reject`, `line_number`) VALUES
(1, 1, 1, 1, 1, 1, 1, 60, 0, '2022-10-10', '10:35:38', '2022-11-10', 1, 70000, 1, 4662000, 'admin', '2022-10-10', '10:35:38', 'ZAK', 0, 'Approval', 'admin', '2022-10-10', '', NULL, 1),
(2, 1, 1, 1, 1, 1, 1, 30, 0, '2022-10-10', '10:38:06', '2022-11-17', 1, 70000, 1, 2331000, 'admin', '2022-10-10', '10:38:06', 'ZAK', 0, 'Approval', 'admin', '2022-10-10', '', NULL, 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `child_sholdto`
--

CREATE TABLE `child_sholdto` (
  `ID` int(11) NOT NULL,
  `ID_SHOLDTO` int(11) NOT NULL,
  `NAMA_PEMILIK` varchar(255) NOT NULL,
  `KODE_SHIPTO` varchar(255) NOT NULL,
  `NAMA_SHIPTO` varchar(255) NOT NULL,
  `ALAMAT` text NOT NULL,
  `DISTRIK_CODE` int(11) NOT NULL,
  `PHONE` varchar(255) NOT NULL,
  `EMAIL` varchar(255) NOT NULL,
  `NILAI_JAMINAN` float NOT NULL DEFAULT 0,
  `CREATE_BY` varchar(255) NOT NULL,
  `UPDATE_BY` varchar(255) NOT NULL,
  `UPDATE_DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `child_sholdto`
--

INSERT INTO `child_sholdto` (`ID`, `ID_SHOLDTO`, `NAMA_PEMILIK`, `KODE_SHIPTO`, `NAMA_SHIPTO`, `ALAMAT`, `DISTRIK_CODE`, `PHONE`, `EMAIL`, `NILAI_JAMINAN`, `CREATE_BY`, `UPDATE_BY`, `UPDATE_DATE`) VALUES
(1, 1, 'Wahidin', '1380001000', 'Toko SKI 1', '-', 1, '0', 'SKI@gmail.com', 1000000, 'admin', 'admin', '2022-10-10');

-- --------------------------------------------------------

--
-- Struktur dari tabel `conveyor`
--

CREATE TABLE `conveyor` (
  `id` int(11) NOT NULL,
  `code_conveyor` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `conveyor`
--

INSERT INTO `conveyor` (`id`, `code_conveyor`, `deskripsi`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`) VALUES
(1, 'C-001', 'Bag-01', 'admin', '2022-10-10', 'admin', '2022-10-10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_po`
--

CREATE TABLE `data_po` (
  `id` int(11) NOT NULL,
  `id_com` int(11) NOT NULL,
  `id_plant` int(11) NOT NULL,
  `no_po` varchar(255) NOT NULL,
  `vendor` int(11) NOT NULL,
  `no_pol` varchar(255) DEFAULT NULL,
  `material` int(11) NOT NULL,
  `qty` float NOT NULL,
  `sisa_qty` float NOT NULL,
  `sloc` int(11) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` datetime NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0,
  `status` varchar(255) NOT NULL,
  `line_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_po`
--

INSERT INTO `data_po` (`id`, `id_com`, `id_plant`, `no_po`, `vendor`, `no_pol`, `material`, `qty`, `sisa_qty`, `sloc`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`, `status`, `line_number`) VALUES
(1, 7000, 1, 'PO-0001', 1, NULL, 1, 30, 0, 1, 'admin', '2022-10-10 14:08:42', 'admin', '2022-10-10 14:08:42', 0, 'Close', 1),
(2, 7000, 1, 'PO-0002', 1, NULL, 1, 20, 0, 1, 'admin', '2022-10-10 14:21:25', 'admin', '2022-10-10 14:21:25', 0, 'Close', 2),
(3, 9000, 1, 'PO-00010', 1, NULL, 1, 60, 0, 1, 'admin', '2022-10-11 13:03:12', 'admin', '2022-10-11 13:03:12', 0, 'Open', 3),
(4, 9000, 1, 'PO-000111', 1, NULL, 1, 62, 0, 1, 'admin', '2022-10-11 13:50:22', 'admin', '2022-10-11 13:50:22', 0, 'Open', 4);

-- --------------------------------------------------------

--
-- Struktur dari tabel `data_truck`
--

CREATE TABLE `data_truck` (
  `id` int(11) NOT NULL,
  `id_expeditur` int(11) NOT NULL,
  `id_supir` int(11) NOT NULL,
  `no_pol` varchar(255) NOT NULL,
  `id_type_truck` int(11) NOT NULL,
  `merek_truck` varchar(255) NOT NULL,
  `no_rfid` varchar(255) NOT NULL,
  `tahun_pembuatan` varchar(4) NOT NULL,
  `jumlah_ban` int(11) NOT NULL,
  `no_stnk` varchar(255) NOT NULL,
  `tanggal_berlaku_stnk` date NOT NULL,
  `no_mesin` varchar(255) NOT NULL,
  `no_rangka` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0,
  `kapasitas_angkut` int(11) NOT NULL,
  `satuan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `data_truck`
--

INSERT INTO `data_truck` (`id`, `id_expeditur`, `id_supir`, `no_pol`, `id_type_truck`, `merek_truck`, `no_rfid`, `tahun_pembuatan`, `jumlah_ban`, `no_stnk`, `tanggal_berlaku_stnk`, `no_mesin`, `no_rangka`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`, `kapasitas_angkut`, `satuan`) VALUES
(1, 1, 1, 'BA-0098-NN', 1, 'HINO', '23948SDF', '2020', 6, '3534534FSF', '2022-12-10', '4534FFS', '4534FSF', 'admin', '2022-10-10', 'admin', '2022-10-10', 0, 60, 'TO');

-- --------------------------------------------------------

--
-- Struktur dari tabel `harga`
--

CREATE TABLE `harga` (
  `ID` int(11) NOT NULL,
  `PLANT` varchar(255) NOT NULL,
  `SOLD_TO` varchar(255) NOT NULL,
  `SHIP_TO` varchar(255) NOT NULL,
  `DISTRIK` varchar(255) NOT NULL,
  `KODE` varchar(255) NOT NULL,
  `DESKRIPSI` text NOT NULL,
  `HARGA` int(11) NOT NULL,
  `VALID_FROM` date NOT NULL,
  `VALID_TO` date NOT NULL,
  `CREATE_BY` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `LAST_UPDATE_BY` varchar(255) NOT NULL,
  `LAST_UPDATE_DATE` date NOT NULL,
  `DELETE_MARK` int(1) NOT NULL,
  `TAX_PPN` float NOT NULL,
  `TAX2` float NOT NULL,
  `TOTAL` float NOT NULL,
  `nominal_tax` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `harga`
--

INSERT INTO `harga` (`ID`, `PLANT`, `SOLD_TO`, `SHIP_TO`, `DISTRIK`, `KODE`, `DESKRIPSI`, `HARGA`, `VALID_FROM`, `VALID_TO`, `CREATE_BY`, `CREATE_DATE`, `LAST_UPDATE_BY`, `LAST_UPDATE_DATE`, `DELETE_MARK`, `TAX_PPN`, `TAX2`, `TOTAL`, `nominal_tax`) VALUES
(1, '1', '1', '1', '1', '1', '-', 70000, '2022-11-04', '2022-11-05', 'admin', '2022-10-10', 'admin', '2022-10-10', 0, 1, 0, 840000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_angkut`
--

CREATE TABLE `master_angkut` (
  `id` int(11) NOT NULL,
  `code_angkut` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_angkut`
--

INSERT INTO `master_angkut` (`id`, `code_angkut`, `deskripsi`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`) VALUES
(1, 'FOT', 'Free On Truck', 'admin', '2022-10-10', 'admin', '2022-10-10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_tax`
--

CREATE TABLE `master_tax` (
  `id` int(11) NOT NULL,
  `tax_code` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `nominal_tax` float NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_tax`
--

INSERT INTO `master_tax` (`id`, `tax_code`, `deskripsi`, `nominal_tax`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`) VALUES
(1, 'BM', 'PPN', 11, 'admin', '2022-10-10', 'admin', '2022-10-10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_top`
--

CREATE TABLE `master_top` (
  `id` int(11) NOT NULL,
  `code_top` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_top`
--

INSERT INTO `master_top` (`id`, `code_top`, `deskripsi`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`) VALUES
(1, 'ZT37', 'Overdue 37 days', 'admin', '2022-10-10', 'admin', '2022-10-10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `o_pp_ski`
--

CREATE TABLE `o_pp_ski` (
  `ID` int(11) NOT NULL,
  `order_type` varchar(255) NOT NULL,
  `order_number` varchar(255) NOT NULL,
  `line_number` int(11) NOT NULL,
  `id_soldto` int(11) DEFAULT NULL,
  `nama_soldto` varchar(255) DEFAULT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `time_create` time NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `time_update` time NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0,
  `status_op` int(11) NOT NULL DEFAULT 0,
  `status_timbangan_keluar` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `o_pp_ski`
--

INSERT INTO `o_pp_ski` (`ID`, `order_type`, `order_number`, `line_number`, `id_soldto`, `nama_soldto`, `create_by`, `create_date`, `time_create`, `update_by`, `update_date`, `time_update`, `delete_mark`, `status_op`, `status_timbangan_keluar`) VALUES
(1, 'ZOR', 'SO-00000001', 1, 1, 'Distributor SKI', 'admin', '2022-10-10', '10:34:06', 'admin', '2022-10-10', '10:34:06', 0, 3, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `proses_timbangan`
--

CREATE TABLE `proses_timbangan` (
  `id` int(11) NOT NULL,
  `no_spj` varchar(255) NOT NULL,
  `id_matching` varchar(255) NOT NULL,
  `id_conveyor` int(11) NOT NULL,
  `total_nilai` float NOT NULL,
  `nilai_masuk` float NOT NULL,
  `nilai_keluar` float NOT NULL DEFAULT 0,
  `nilai_akhir` float NOT NULL DEFAULT 0,
  `tanggal_masuk` date NOT NULL,
  `tanggal_keluar` date DEFAULT NULL,
  `jam_masuk` time NOT NULL,
  `jam_keluar` time DEFAULT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0,
  `line_number` int(11) NOT NULL,
  `status_billing` int(11) NOT NULL DEFAULT 0,
  `id_soldto` int(11) DEFAULT NULL,
  `id_c_pp` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `proses_timbangan`
--

INSERT INTO `proses_timbangan` (`id`, `no_spj`, `id_matching`, `id_conveyor`, `total_nilai`, `nilai_masuk`, `nilai_keluar`, `nilai_akhir`, `tanggal_masuk`, `tanggal_keluar`, `jam_masuk`, `jam_keluar`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`, `line_number`, `status_billing`, `id_soldto`, `id_c_pp`) VALUES
(1, 'SPJ-00000001', '1', 1, 90, 13000, 23000, 10000, '2022-10-10', '2022-10-10', '10:53:04', '10:55:06', 'admin', '2022-10-10', 'admin', '2022-10-10', 0, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pt_master_distrik_ski`
--

CREATE TABLE `pt_master_distrik_ski` (
  `ID` int(11) NOT NULL,
  `KODE_DISTRIK` varchar(255) NOT NULL,
  `DISTRIK` varchar(255) NOT NULL,
  `CREATE_BY` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `LAST_UPDATE_BY` varchar(255) NOT NULL,
  `LAST_UPDATE` date NOT NULL,
  `DELETE_MARK` int(1) NOT NULL,
  `PARENT_CODE` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pt_master_distrik_ski`
--

INSERT INTO `pt_master_distrik_ski` (`ID`, `KODE_DISTRIK`, `DISTRIK`, `CREATE_BY`, `CREATE_DATE`, `LAST_UPDATE_BY`, `LAST_UPDATE`, `DELETE_MARK`, `PARENT_CODE`) VALUES
(1, '251001', 'Gresik', 'admin', '2022-10-10', 'admin', '2022-10-10', 0, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rfc_z_zapp_select_sysplan_ski`
--

CREATE TABLE `rfc_z_zapp_select_sysplan_ski` (
  `ID` int(11) NOT NULL,
  `WERKS` varchar(255) NOT NULL,
  `NAME1` varchar(255) NOT NULL,
  `ALAMAT` text NOT NULL,
  `ID_DISTRIK` int(11) NOT NULL,
  `LONGITUDE` text NOT NULL,
  `LATITUDE` text NOT NULL,
  `CREATE_BY` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `LAST_UPDATE_DATE` date NOT NULL,
  `LAST_UPDATE_BY` varchar(255) NOT NULL,
  `DELETE_MARK` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `rfc_z_zapp_select_sysplan_ski`
--

INSERT INTO `rfc_z_zapp_select_sysplan_ski` (`ID`, `WERKS`, `NAME1`, `ALAMAT`, `ID_DISTRIK`, `LONGITUDE`, `LATITUDE`, `CREATE_BY`, `CREATE_DATE`, `LAST_UPDATE_DATE`, `LAST_UPDATE_BY`, `DELETE_MARK`) VALUES
(1, '7403', 'CP.Tuban', '-', 1, '-', '-', 'admin', '2022-10-10', '2022-10-10', 'admin', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rfc_z_zcsd_list_mat_sales_ski`
--

CREATE TABLE `rfc_z_zcsd_list_mat_sales_ski` (
  `ID` int(11) NOT NULL,
  `KODE_MATERIAL` varchar(255) NOT NULL,
  `DESKRIPSI` text NOT NULL,
  `TYPE_MATERIAL` varchar(255) NOT NULL,
  `BERAT` float NOT NULL,
  `UOM` varchar(255) NOT NULL,
  `CREATE_BY` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `LAST_UPDATE_BY` varchar(255) NOT NULL,
  `LAST_UPDATE_DATE` varchar(255) NOT NULL,
  `DELETE_MARK` int(1) NOT NULL,
  `BERAT_KONFERSI` float NOT NULL,
  `UOM_KONFERSI` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `rfc_z_zcsd_list_mat_sales_ski`
--

INSERT INTO `rfc_z_zcsd_list_mat_sales_ski` (`ID`, `KODE_MATERIAL`, `DESKRIPSI`, `TYPE_MATERIAL`, `BERAT`, `UOM`, `CREATE_BY`, `CREATE_DATE`, `LAST_UPDATE_BY`, `LAST_UPDATE_DATE`, `DELETE_MARK`, `BERAT_KONFERSI`, `UOM_KONFERSI`) VALUES
(1, '121-301-0050', 'SEMEN PCC ZAK 40 KG', 'ZAK', 1, 'ZAK', 'admin', '2022-10-10', 'admin', '2022-10-10', 0, 40, 'KG');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rfc_z_zcsd_shipto_ski`
--

CREATE TABLE `rfc_z_zcsd_shipto_ski` (
  `ID` int(11) NOT NULL,
  `KODE_SOLDTO` varchar(255) NOT NULL,
  `NAMA_SOLDTO` varchar(255) NOT NULL,
  `NILAI_JAMINAN` float NOT NULL DEFAULT 0,
  `KODE_SHIPTO` varchar(255) DEFAULT NULL,
  `NAMA_SHIPTO` varchar(255) DEFAULT NULL,
  `ALAMAT` text NOT NULL,
  `KODE_DSITRIK` varchar(255) NOT NULL,
  `NAMA_DISTRIK` varchar(255) NOT NULL,
  `NAMA_PEMILIK` varchar(255) NOT NULL,
  `CREATE_BY` varchar(255) NOT NULL,
  `CREATE_DATE` date NOT NULL,
  `LAST_UPDATE_BY` varchar(255) NOT NULL,
  `LAST_UPDATE_DATE` date NOT NULL,
  `DELETE_MARK` int(1) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `rfc_z_zcsd_shipto_ski`
--

INSERT INTO `rfc_z_zcsd_shipto_ski` (`ID`, `KODE_SOLDTO`, `NAMA_SOLDTO`, `NILAI_JAMINAN`, `KODE_SHIPTO`, `NAMA_SHIPTO`, `ALAMAT`, `KODE_DSITRIK`, `NAMA_DISTRIK`, `NAMA_PEMILIK`, `CREATE_BY`, `CREATE_DATE`, `LAST_UPDATE_BY`, `LAST_UPDATE_DATE`, `DELETE_MARK`, `id_user`) VALUES
(1, '0000000138', 'Distributor SKI', 1000000, NULL, NULL, '-', '1', 'Gresik', 'Hafid', 'admin', '2022-10-10', 'admin', '2022-10-10', 0, 0),
(2, '0000000134', 'Distributor SKI 3', 1000000000, NULL, NULL, 'Jln. ok', '1', 'Gresik', 'Fadly', 'admin', '2022-10-13', 'admin', '2022-10-13', 0, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `staging_stock`
--

CREATE TABLE `staging_stock` (
  `id` int(11) NOT NULL,
  `id_plant` int(11) NOT NULL,
  `id_mat` int(11) NOT NULL,
  `qty` float NOT NULL,
  `last_update` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `staging_stock`
--

INSERT INTO `staging_stock` (`id`, `id_plant`, `id_mat`, `qty`, `last_update`) VALUES
(1, 1, 1, 50999.1, '2022-10-11 02:12:24');

-- --------------------------------------------------------

--
-- Struktur dari tabel `supir`
--

CREATE TABLE `supir` (
  `id` int(11) NOT NULL,
  `nama_supir` varchar(255) NOT NULL,
  `no_sim` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `supir`
--

INSERT INTO `supir` (`id`, `nama_supir`, `no_sim`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`) VALUES
(1, 'Fadly', '12345678', 'admin', '2022-10-10', 'admin', '2022-10-10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_expeditur`
--

CREATE TABLE `table_expeditur` (
  `id` int(11) NOT NULL,
  `code_expeditur` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `table_expeditur`
--

INSERT INTO `table_expeditur` (`id`, `code_expeditur`, `deskripsi`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`) VALUES
(1, 'E-001', 'Expeditur SI', 'admin', '2022-10-10', 'admin', '2022-10-10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_matching`
--

CREATE TABLE `table_matching` (
  `id` int(11) NOT NULL,
  `no_matching` varchar(255) DEFAULT NULL,
  `no_so` varchar(255) NOT NULL,
  `id_truck` int(11) NOT NULL,
  `id_supir` int(11) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `time_create` time NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `time_update` time NOT NULL,
  `delete_mark` int(11) NOT NULL,
  `line_number` int(11) NOT NULL,
  `id_expeditur` int(11) NOT NULL,
  `status_selesai` int(11) NOT NULL DEFAULT 0,
  `status_matching` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `table_matching`
--

INSERT INTO `table_matching` (`id`, `no_matching`, `no_so`, `id_truck`, `id_supir`, `create_by`, `create_date`, `time_create`, `update_by`, `update_date`, `time_update`, `delete_mark`, `line_number`, `id_expeditur`, `status_selesai`, `status_matching`) VALUES
(1, 'MC-000000001', '', 1, 1, 'admin', '2022-10-10', '10:48:34', 'admin', '2022-10-10', '10:48:34', 0, 1, 1, 1, 50);

-- --------------------------------------------------------

--
-- Struktur dari tabel `table_sloc`
--

CREATE TABLE `table_sloc` (
  `id` int(11) NOT NULL,
  `keterangan` varchar(255) NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `table_sloc`
--

INSERT INTO `table_sloc` (`id`, `keterangan`, `delete_mark`) VALUES
(1, 'Sloc 1', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `timbangan_incoming`
--

CREATE TABLE `timbangan_incoming` (
  `id` int(11) NOT NULL,
  `no_spj` varchar(255) NOT NULL,
  `no_pol` varchar(255) NOT NULL,
  `id_po` int(11) NOT NULL,
  `tanggal_masuk` date NOT NULL,
  `tanggal_keluar` date DEFAULT NULL,
  `jam_masuk` time NOT NULL,
  `jam_keluar` time DEFAULT NULL,
  `nilai_masuk` float NOT NULL,
  `nilai_keluar` float NOT NULL DEFAULT 0,
  `subtotal` float NOT NULL DEFAULT 0,
  `qty_po` float NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0,
  `line_order` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `timbangan_incoming`
--

INSERT INTO `timbangan_incoming` (`id`, `no_spj`, `no_pol`, `id_po`, `tanggal_masuk`, `tanggal_keluar`, `jam_masuk`, `jam_keluar`, `nilai_masuk`, `nilai_keluar`, `subtotal`, `qty_po`, `create_by`, `update_by`, `delete_mark`, `line_order`) VALUES
(1, 'SPJI-00000001', 'BA-0099-MM', 1, '2022-10-10', '2022-10-10', '14:09:25', '14:10:58', 40000, 10000, 30000, 0, 'admin', 'admin', 0, 1),
(2, 'SPJI-00000002', 'BA-0091-MM', 2, '2022-10-10', '2022-10-10', '14:22:49', '14:23:44', 30000, 10000, 20000, 0, 'admin', 'admin', 0, 2),
(3, 'SPJI-00000003', 'BA-0098-MM', 3, '2022-10-11', '2022-10-11', '12:53:12', '14:03:31', 45000, 45000, 0, 30, 'admin', 'admin', 0, 3),
(4, 'SPJI-00000004', 'BA-0091-MM', 0, '2022-10-11', '2022-10-11', '13:04:59', '14:05:03', 0, 0, 0, 0, 'admin', 'admin', 0, 4),
(5, 'SPJI-00000005', 'BA-0095-MM', 3, '2022-10-11', '2022-10-11', '13:56:43', '14:05:47', 45000, 44001, 999.001, 30, 'admin', 'admin', 0, 5),
(6, 'SPJI-00000006', 'BA-0038-MM', 4, '2022-10-11', '2022-10-11', '14:02:51', '14:12:02', 65000, 64999.9, 0.1, 50, 'admin', 'admin', 0, 6);

-- --------------------------------------------------------

--
-- Struktur dari tabel `type_master`
--

CREATE TABLE `type_master` (
  `id` int(11) NOT NULL,
  `code_type` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `type_master`
--

INSERT INTO `type_master` (`id`, `code_type`, `deskripsi`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`) VALUES
(1, 'ZOR', 'Sales Standart', 'admin', '2022-10-10', 'admin', '2022-10-10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `type_truck`
--

CREATE TABLE `type_truck` (
  `id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `create_date` date NOT NULL,
  `update_by` varchar(255) NOT NULL,
  `update_date` date NOT NULL,
  `delete_mark` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `type_truck`
--

INSERT INTO `type_truck` (`id`, `code`, `deskripsi`, `create_by`, `create_date`, `update_by`, `update_date`, `delete_mark`) VALUES
(1, '307', 'Trailer Panjang', 'admin', '2022-10-10', 'admin', '2022-10-10', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `username`, `name`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `photo`, `status`, `created_at`, `updated_at`, `role`) VALUES
(1, 'admin', 'Admin', 'UlY8F83Uf9FZg0i8DDu4dFcKoP4DWUXC', '$2y$13$ZL8iDhxq/DnuyH5LQ3fZZu9gfsejhmGtmjID9xO71YpuGMFAkEMWC', '', 'admin@gmail.com', '1660557708adminSIG_Logo.svg.png', 10, 0, 1660557708, 'admin'),
(6, 'Fadly78Oct13', 'Fadly', 'w4AYwzc3bCd5fc981g3qqXCFmjcOHNSI', '$2y$13$fxOmGUsTstdaYuJ5JCAWfeRUJIlG2jTxD4zAFpyKNLwlmPE/gXBcK', NULL, 'Fadly78Oct13@gmail.com', '', 10, 1665596428, 1665596428, 'Distributor'),
(7, 'petugastimbang', 'Petugas Timbang', 'genKwEkMYLboLJteADWZ_2bu5oTq8VCJ', '$2y$13$AcnQ3DObnmxiv3VwzcS68e41uKJhZmipPWDqDyjoDDivLlYMs4Aea', NULL, 'petugastimbang@gmail.com', '', 10, 1665597420, 1665597420, 'Petugas Timbang'),
(8, 'penjualan', 'penjualan', 'TH-8ZnM4Gcg9jL44Oz_90NHV5eR83ehd', '$2y$13$s5NUkuKsjrwvS1VK5O4SA.RXhdV.yAeyng5mYUgsY92DlP3GOWQFm', NULL, 'penjualan123@gmail.com', '', 10, 1665597553, 1665597553, 'Penjualan');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `billing`
--
ALTER TABLE `billing`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `child_billing`
--
ALTER TABLE `child_billing`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `child_matching`
--
ALTER TABLE `child_matching`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `child_o_pp_ski`
--
ALTER TABLE `child_o_pp_ski`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `child_sholdto`
--
ALTER TABLE `child_sholdto`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `conveyor`
--
ALTER TABLE `conveyor`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_po`
--
ALTER TABLE `data_po`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `data_truck`
--
ALTER TABLE `data_truck`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `harga`
--
ALTER TABLE `harga`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `master_angkut`
--
ALTER TABLE `master_angkut`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_tax`
--
ALTER TABLE `master_tax`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `master_top`
--
ALTER TABLE `master_top`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `o_pp_ski`
--
ALTER TABLE `o_pp_ski`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `proses_timbangan`
--
ALTER TABLE `proses_timbangan`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `pt_master_distrik_ski`
--
ALTER TABLE `pt_master_distrik_ski`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `rfc_z_zapp_select_sysplan_ski`
--
ALTER TABLE `rfc_z_zapp_select_sysplan_ski`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `rfc_z_zcsd_list_mat_sales_ski`
--
ALTER TABLE `rfc_z_zcsd_list_mat_sales_ski`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `rfc_z_zcsd_shipto_ski`
--
ALTER TABLE `rfc_z_zcsd_shipto_ski`
  ADD PRIMARY KEY (`ID`);

--
-- Indeks untuk tabel `staging_stock`
--
ALTER TABLE `staging_stock`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `supir`
--
ALTER TABLE `supir`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `table_expeditur`
--
ALTER TABLE `table_expeditur`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `table_matching`
--
ALTER TABLE `table_matching`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `table_sloc`
--
ALTER TABLE `table_sloc`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `timbangan_incoming`
--
ALTER TABLE `timbangan_incoming`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `type_master`
--
ALTER TABLE `type_master`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `type_truck`
--
ALTER TABLE `type_truck`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `billing`
--
ALTER TABLE `billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `child_billing`
--
ALTER TABLE `child_billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `child_matching`
--
ALTER TABLE `child_matching`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `child_o_pp_ski`
--
ALTER TABLE `child_o_pp_ski`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `child_sholdto`
--
ALTER TABLE `child_sholdto`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `conveyor`
--
ALTER TABLE `conveyor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `data_po`
--
ALTER TABLE `data_po`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `data_truck`
--
ALTER TABLE `data_truck`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `harga`
--
ALTER TABLE `harga`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `master_angkut`
--
ALTER TABLE `master_angkut`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `master_tax`
--
ALTER TABLE `master_tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `master_top`
--
ALTER TABLE `master_top`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `o_pp_ski`
--
ALTER TABLE `o_pp_ski`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `proses_timbangan`
--
ALTER TABLE `proses_timbangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pt_master_distrik_ski`
--
ALTER TABLE `pt_master_distrik_ski`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rfc_z_zapp_select_sysplan_ski`
--
ALTER TABLE `rfc_z_zapp_select_sysplan_ski`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rfc_z_zcsd_list_mat_sales_ski`
--
ALTER TABLE `rfc_z_zcsd_list_mat_sales_ski`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `rfc_z_zcsd_shipto_ski`
--
ALTER TABLE `rfc_z_zcsd_shipto_ski`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `staging_stock`
--
ALTER TABLE `staging_stock`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `supir`
--
ALTER TABLE `supir`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `table_expeditur`
--
ALTER TABLE `table_expeditur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `table_matching`
--
ALTER TABLE `table_matching`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `table_sloc`
--
ALTER TABLE `table_sloc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `timbangan_incoming`
--
ALTER TABLE `timbangan_incoming`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `type_master`
--
ALTER TABLE `type_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `type_truck`
--
ALTER TABLE `type_truck`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
