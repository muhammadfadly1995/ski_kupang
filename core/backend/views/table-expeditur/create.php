<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TableExpeditur */

$this->title = 'Create Table Expeditur';
$this->params['breadcrumbs'][] = ['label' => 'Table Expediturs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-expeditur-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
