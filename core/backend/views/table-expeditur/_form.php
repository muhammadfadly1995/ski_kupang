<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TableExpeditur */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="table-expeditur-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'code_expeditur')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'deskripsi')->textInput(['maxlength' => true]) ?>



            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>