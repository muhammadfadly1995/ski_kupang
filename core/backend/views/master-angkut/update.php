<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterAngkut */

$this->title = 'Update Master Angkut: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Master Angkuts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="master-angkut-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
