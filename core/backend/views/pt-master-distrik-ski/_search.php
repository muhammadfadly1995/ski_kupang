<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PtMasterDistrikSkiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pt-master-distrik-ski-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'KODE_DISTRIK') ?>

    <?= $form->field($model, 'DISTRIK') ?>

    <?= $form->field($model, 'CREATE_BY') ?>

    <?= $form->field($model, 'CREATE_DATE') ?>

    <?php // echo $form->field($model, 'LAST_UPDATE_BY') ?>

    <?php // echo $form->field($model, 'LAST_UPDATE') ?>

    <?php // echo $form->field($model, 'DELETE_MARK') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
