<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PtMasterDistrikSki */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'KODE_DISTRIK')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'DISTRIK')->textInput(['maxlength' => true]) ?>

              
            </div>
            <div class="col-md-6">
            <?= $form->field($model, 'CREATE_BY')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
         
                <?= $form->field($model, 'CREATE_DATE')->textInput(['readOnly' => true]) ?>

                <?= $form->field($model, 'LAST_UPDATE_BY')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>