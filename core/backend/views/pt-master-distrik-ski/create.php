<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PtMasterDistrikSki */

$this->title = 'Tambah Data Distrik';
$this->params['breadcrumbs'][] = ['label' => 'Data Distrik', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pt-master-distrik-ski-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
