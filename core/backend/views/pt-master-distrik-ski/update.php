<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PtMasterDistrikSki */

$this->title = 'Update Pt Master Distrik Ski: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Pt Master Distrik Skis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pt-master-distrik-ski-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
