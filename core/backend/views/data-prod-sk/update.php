<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DataProdSk */

$this->title = 'Update Data Prod Sk: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Data Prod Sks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="data-prod-sk-update">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
