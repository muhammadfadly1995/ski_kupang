<?php
 
use common\models\ChildSholdto;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Prod SK';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
              
        
              //  'id',
                'data_type',
                'data_version',
                'date_prod',
                [
                    'attribute' => 'material_code',
                    'value' => function ($model) {
                        $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->material_code])->one();
                        $status = $datakategori->KODE_MATERIAL . ' / ' . $datakategori->DESKRIPSI;
                        return $status;
                    }
                ],
            //    'material_code',
                'remark',
                [
                    'attribute' => 'plant',
                    'value' => function ($model) {
                        $datakategori = RfcZZappSelectSysplanSki::find()->where(['ID' => $model->plant])->one();
                        $status = $datakategori->WERKS . ' / ' . $datakategori->NAME1;
                        return $status;
                    }
                ],
                [
                    'attribute' => 'stock_awal',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->plant])->one();
                      
                        return  number_format($model->stock_awal). ' '.$datakategori->UOM;
                    }
                ],
                [
                    'attribute' => 'recive',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->plant])->one();
                        return  number_format($model->recive). ' '.$datakategori->UOM;
                    }
                ],
                [
                    'attribute' => 'issued',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->plant])->one();
                        return  number_format($model->issued). ' '.$datakategori->UOM;
                    }
                ],
                [
                    'attribute' => 'stock_akhir',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->plant])->one();
                        return  number_format($model->stock_akhir). ' '.$datakategori->UOM;
                    }
                ],
                //'create_by',
                //'create_date',
                //'update_by',
                //'update_date',
                //'delete_mark',
                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
    </div>
</div>