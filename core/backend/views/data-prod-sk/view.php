<?php


use yii\widgets\DetailView;
use common\models\ChildSholdto;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
/* @var $this yii\web\View */
/* @var $model common\models\DataProdSk */

$this->title = 'Detail Prod';
$this->params['breadcrumbs'][] = ['label' => 'Data Prod Sks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="data-prod-sk-view">

            <h4><?= Html::encode($this->title) ?></h4>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'data_type',
                        'data_version',
                        'date_prod',
                        [
                            'attribute' => 'material_code',
                            'value' => function ($model) {
                                $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->material_code])->one();
                                $status = $datakategori->KODE_MATERIAL . ' / ' . $datakategori->DESKRIPSI;
                                return $status;
                            }
                        ],
                        'remark',
                        [
                            'attribute' => 'plant',
                            'value' => function ($model) {
                                $datakategori = RfcZZappSelectSysplanSki::find()->where(['ID' => $model->plant])->one();
                                $status = $datakategori->WERKS . ' / ' . $datakategori->NAME1;
                                return $status;
                            }
                        ],
                        [
                            'attribute' => 'stock_awal',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->plant])->one();
                              
                                return  number_format($model->stock_awal). ' '.$datakategori->UOM;
                            }
                        ],
                      
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
               
                   
                    [
                        'attribute' => 'recive',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->plant])->one();
                            return  number_format($model->recive). ' '.$datakategori->UOM;
                        }
                    ],
                    [
                        'attribute' => 'issued',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->plant])->one();
                            return  number_format($model->issued). ' '.$datakategori->UOM;
                        }
                    ],
                    [
                        'attribute' => 'stock_akhir',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->plant])->one();
                            return  number_format($model->stock_akhir). ' '.$datakategori->UOM;
                        }
                    ],
                    // 'recive',
                    // 'issued',
                    // 'stock_akhir',
                    'create_by',
                    'create_date',
                    'update_by',
                    'update_date',
                    //   'delete_mark',
                ],
            ]) ?>
            </div>
           
            <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
        </div>
       
    </div>
</div>