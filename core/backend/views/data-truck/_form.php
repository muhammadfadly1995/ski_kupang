<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\DataTruck */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="data-truck-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
            <?= $form->field($model, 'id_expeditur')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\TableExpeditur::find()->asArray()->all(), 'id','deskripsi', 'code_expeditur'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Expeditur Truk');
                ?>
                 <?= $form->field($model, 'id_supir')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\Supir::find()->asArray()->all(), 'id','nama_supir'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Supir Truk');
                ?>
                <?= $form->field($model, 'no_pol')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'id_type_truck')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\TypeTruck::find()->asArray()->all(), 'id', 'code','deskripsi'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Tipe Truk');
                ?>

                <?= $form->field($model, 'merek_truck')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'no_rfid')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'tahun_pembuatan')->textInput(['maxlength' => true]) ?>
               
            </div>
            <div class="col-md-6">
            <?= $form->field($model, 'kapasitas_angkut')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'satuan')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ['TO'=>'TO','KG'=>'KG'],
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Satuan Berat');
                ?>
                <?= $form->field($model, 'jumlah_ban')->textInput() ?>

                <?= $form->field($model, 'no_stnk')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'tanggal_berlaku_stnk')->widget(DatePicker::className(), [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd', 'target' => '_Blank']
                ]);
                ?>

                <?= $form->field($model, 'no_mesin')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'no_rangka')->textInput(['maxlength' => true]) ?>


            </div>
            <div class="col-md-12">
                <div class="form-group">

                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
       
    </div>
</div>