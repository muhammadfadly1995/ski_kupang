<?php

use common\models\ChildSholdto;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
use common\models\Supir;
use common\models\TableExpeditur;
use common\models\TypeTruck;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Table Truk';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                //'ID',
             //   'id',
             [
                'attribute' => 'id_expeditur',
                'label'=>'Expeditur Truck',
                'contentOptions' => ['class' => 'text-left'],
                'value' => function ($model) {
                   $dataexpeditur=TableExpeditur::find()->where(['id'=>$model->id_expeditur])->one();
                   if($dataexpeditur){
                    $expeditur=$dataexpeditur->code_expeditur.'/'.$dataexpeditur->deskripsi;
                   }else{
                    $expeditur='Data Tidak Ditemukan';
                   }
                    return $expeditur;
                }
            ],
            [
                'attribute' => 'id_supir',
                'label'=>'Supir Truck',
                'contentOptions' => ['class' => 'text-left'],
                'value' => function ($model) {
                   $datasupir=Supir::find()->where(['id'=>$model->id_supir])->one();
                   if($datasupir){
                    $supir=$datasupir->nama_supir;
                   }else{
                    $supir='Data Tidak Ditemukan';
                   }
                    return $supir;
                }
            ],
                'no_pol',
                [
                    'attribute' => 'id_type_truck',
                    'label'=>'Tipe Truck',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatipe=TypeTruck::find()->where(['id'=>$model->id_type_truck])->one();
                        return $datatipe->code .' '.$datatipe->deskripsi;
                    }
                ],
                'merek_truck',
                'no_rfid',
                'tahun_pembuatan',
                'jumlah_ban',
                'no_stnk',
                'tanggal_berlaku_stnk',
                'no_mesin',
                'no_rangka',
                [
                    'attribute' => 'kapasitas_angkut',
                    //'label'=>'Tipe Truck',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                       
                        return $model->kapasitas_angkut .' '.$model->satuan;
                    }
                ],
              //  'kapasitas_angkut',
                'create_by',
                'create_date',
                'update_by',
                'update_date',
                //'delete_mark',

                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
    </div>
</div>