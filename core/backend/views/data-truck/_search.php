<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DataTruckSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-truck-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'no_pol') ?>

    <?= $form->field($model, 'id_type_truck') ?>

    <?= $form->field($model, 'merek_truck') ?>

    <?= $form->field($model, 'no_rfid') ?>

    <?php // echo $form->field($model, 'tahun_pembuatan') ?>

    <?php // echo $form->field($model, 'jumlah_ban') ?>

    <?php // echo $form->field($model, 'no_stnk') ?>

    <?php // echo $form->field($model, 'tanggal_berlaku_stnk') ?>

    <?php // echo $form->field($model, 'no_mesin') ?>

    <?php // echo $form->field($model, 'no_rangka') ?>

    <?php // echo $form->field($model, 'create_by') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'update_by') ?>

    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'delete_mark') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
