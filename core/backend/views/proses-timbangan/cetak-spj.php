<!DOCTYPE html>
<html>

<head>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link href="<?= Yii::getAlias('@web/css/bootstrap.min.css') ?>" rel="stylesheet" media="print">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        body {
            font-family: "Times New Roman", Times, serif;
            margin-left: 0cm;
            margin-right: 0cm;
            margin-top: 0cm;
            margin-bottom: 0cm;

        }

        .mar-papper {
            /* margin: 1cm 3cm 2cm; */
        }

        h4 {
            font-family: "Times New Roman", Times, serif;
            font-size: 16pt;
            margin: 0px auto;
            font-weight: normal;

        }

        h2 {
            font-family: "Times New Roman", Times, serif;
            font-size: 14pt;
            font-weight: bold;
            margin: 0px auto;
        }

        p {
            font-family: "Times New Roman", Times, serif;
            font-size: 14px;
            margin: 0px auto;
            font-weight: normal;

        }

        th {
            font-family: "Times New Roman", Times, serif;
            font-size: 14px !important;
            font-weight: normal;
            line-height: 14px;
        }

        td {
            font-family: "Times New Roman", Times, serif;
            font-size: 14px !important;
            font-weight: normal;
            line-height: 14px;
        }

        table {
            font-family: "Times New Roman", Times, serif;
        }

        .table-padd .table tbody tr td {
            border: none;
            padding: 1px !important;
        }

        hr {
            border: 0;
            height: 2px;
            border-top: 2px solid rgb(0 0 0 / 80%);
            border-bottom: 1px solid rgba(255, 255, 255, 0.3);
            margin: 0px 0px 0px 0px;
        }

        .tembusan tr th {
            font-size: 10px !important;
            line-height: 10px;
        }

        @media print {
            html,body{
                height: 100%;
                overflow: hidden;
                page-break-after: always;
            }

            /* page-break-after works, as well */
        }

        .brand-box {
            background-color: #c2c2c2 !important;
        }

        @media print {
            .brand-box {
                box-shadow: inset 0 0 0 100em rgba(2, 132, 130, 0.2) !important;
            }

            body {
                width: auto;
                height: auto;
            }
        }

        .wrapper-p p {
            font-size: 18px;
        }

        @media print {
            @page {
                size: 21.59cm 16.02cm portrait;
                /* margin: 30px 80px 10px 120px; */
            }

        }
    </style>
</head>

<body onload="window.print()">

    <div class="wrapper wrapper-p">
        <div class="pagebreak">
            <section class="invoice">
                <div class="row">
                <div class="col-xs-12">

<table style="width:100%;">
    <tr>
        <td><img src="<?= Yii::getAlias('@web/background/ski.jpg') ?>" class="img-responsive" width="75px" alt="" style="padding-left: 30px;"></td>

        <td>
            <!-- <h4 class="text-center" style="text-transform: uppercase;text-align: center;">
                <?= 'PT. Semen Kupang Indonesia' ?></h4>
            <h4 class="text-center" style="text-transform: uppercase;text-align: center;">
                <?= 'PT. Semen Kupang Tbk' ?></h4> -->
            <h2 class="text-center" style="text-transform: uppercase;text-align: center;">
                <?php
                if($model->nilai_keluar==0){
                    echo  'Surat Izin Pemuatan Semen';
                }else{
                    echo  'Surat Perintah Jalan OUT';
                }
                ?></h2>
            <hr>
            <p style="text-align: center;">Nomor : <?= $model->no_spj ?></p>

        </td>
        <td style="text-align:right;"><img src="<?= Yii::getAlias('@web/background/Semen_Kupang.webp') ?>" class="img-responsive" width="75px" alt="" style="padding-right: 30px;"></td>
        
    </tr>
</table>
</div>
                </div>
                <br>
                <div class="row invoice-info">
                    <div class="col-sm-12 table-padd">

                        <table class="table">
                            <tbody style="text-transform: capitalize">
                                <tr>
                                    <td style="width: 22%">No Truck/Gerbong </td>
                                    <td>:</td>
                                    <td style="font-weight: bold"> <?php

                                                                   // $datatruck = \common\models\DataTruck::find()->where(['id' => $model2->id_truck])->one();
                                                                    echo $model2->id_truck; ?>
                                    </td>
                                    <td></td>
                                    <td style="width: 22%">Tgl Timbang Masuk/Keluar </td>
                                    <td>:</td>
                                    <td style="font-weight: bold"><?= $model->tanggal_masuk.'/'.$model->tanggal_keluar ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 22%">Nama Pengemudi </td>
                                    <td>:</td>
                                    <td style="font-weight: bold">
                                        <?php
                                        // $datatruck = \common\models\DataTruck::find()->where(['id' => $model2->id_truck])->one();
                                        // $datasupir = \common\models\Supir::find()->where(['id' => $datatruck->id_supir])->one();
                                        echo $model2->id_supir;  ?>
                                    </td>
                                    <td></td>
                                    <td style="width: 22%">Jam Masuk/Keluar </td>
                                    <td>:</td>
                                    <td style="font-weight: bold"><?php
                                                                    echo $model->jam_masuk.'/'.$model->jam_keluar  ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 22%">Distributor </td>
                                    <td>:</td>
                                    <td style="font-weight: bold">Semen Kupang IND
                                    </td>
                                    <td></td>
                                    <td style="width: 22%">Conveyor </td>
                                    <td>:</td>
                                    <td style="font-weight: bold">
                                        <?php
                                        $datatruck = \common\models\Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                                        if ($datatruck) {
                                            echo $datatruck->code_conveyor . '/' . $datatruck->deskripsi;
                                        }  ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 22%">Nama Soldto</td>
                                    <td>:</td>
                                    <td style="font-weight: bold">
                                        <?php
                                        $datasoldto = \common\models\RfcZZcsdShiptoSki::find()->where(['ID' => $datachildsoldto->id_soldto])->one();
                                        echo $datasoldto->KODE_SOLDTO . '/' . $datasoldto->NAMA_SOLDTO;

                                        ?>
                                    </td>
                                    <td></td>
                                    <td style="width: 22%">Timbangan Masuk</td>
                                    <td>:</td>
                                    <td style="font-weight: bold">
                                        <?php
                                      //  $datasoldto = \common\models\RfcZZcsdShiptoSki::find()->where(['ID' => $datachildsoldto->id_soldto])->one();
                                        echo $model->nilai_masuk . ' KG';

                                        ?>
                                    </td>

                                </tr>
                                <tr>
                                    <td style="width: 22%">Expeditur </td>
                                    <td>:</td>
                                    <td style="font-weight: bold">
                                        <?php
                                        $dataexpeditur = \common\models\TableExpeditur::find()->where(['id' => $model2->id_expeditur])->one();
                                        // $datasupir=Supir::find()->where(['id'=>$datatruck->id_supir])->one();
                                        echo $dataexpeditur->code_expeditur . '/' . $dataexpeditur->deskripsi;

                                        ?>
                                    </td>
                                    <td></td>
                                    <td style="width: 22%">Timbangan Keluar</td>
                                    <td>:</td>
                                    <td style="font-weight: bold">
                                        <?php
                                       // $datasoldto = \common\models\RfcZZcsdShiptoSki::find()->where(['ID' => $datachildsoldto->id_soldto])->one();
                                        echo $model->nilai_keluar . ' KG';

                                        ?>
                                    </td>

                                </tr>
                                 <tr>
                                    <td style="width: 22%"> </td>
                                    <td></td>
                                    <td style="font-weight: bold">
                                       
                                    </td>
                                    <td></td>
                                    <td style="width: 22%">Subtotal</td>
                                    <td>:</td>
                                    <td style="font-weight: bold">
                                        <?php
                                       // $datasoldto = \common\models\RfcZZcsdShiptoSki::find()->where(['ID' => $datachildsoldto->id_soldto])->one();
                                        echo $model->nilai_akhir . ' KG';

                                        ?>
                                    </td>

                                </tr>
                            </tbody>
                        </table>



                    </div>

                </div>
                <div class="row" style="margin-top: 30px;">
                    <div class="col-xs-12">
                        <table width="100%" border="1">
                            <thead>
                                <tr style="text-transform: capitalize">
                                    <th style="padding: 5px; text-align: center; font-weight: bold">No</th>
                                    <th style="padding: 5px; text-align: center; font-weight: bold">Nama Shipto</th>
                                    <th style="padding: 5px; text-align: center; font-weight: bold">Alamat</th>
                                    <th style="padding: 5px; text-align: center; font-weight: bold">Jumlah</th>
                                    <th style="padding: 5px; text-align: center; font-weight: bold">Dengan Huruf</th>
                                    <th style="padding: 5px; text-align: center; font-weight: bold">Jenis Semen</th>
                                    <th style="padding: 5px; text-align: center;font-weight: bold">Satuan</th>

                                    <th style="padding: 5px; text-align: center; font-weight: bold">Jenis Kantong</th>
                                 
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                function penyebut($nilai)
                                {
                                    $nilai = abs($nilai);
                                    $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
                                    $temp = "";
                                    if ($nilai < 12) {
                                        $temp = " " . $huruf[$nilai];
                                    } else if ($nilai < 20) {
                                        $temp = penyebut($nilai - 10) . " Belas";
                                    } else if ($nilai < 100) {
                                        $temp = penyebut($nilai / 10) . " Puluh" . penyebut($nilai % 10);
                                    } else if ($nilai < 200) {
                                        $temp = " seratus" . penyebut($nilai - 100);
                                    } else if ($nilai < 1000) {
                                        $temp = penyebut($nilai / 100) . " Ratus" . penyebut($nilai % 100);
                                    } else if ($nilai < 2000) {
                                        $temp = " seribu" . penyebut($nilai - 1000);
                                    } else if ($nilai < 1000000) {
                                        $temp = penyebut($nilai / 1000) . " Ribu" . penyebut($nilai % 1000);
                                    } else if ($nilai < 1000000000) {
                                        $temp = penyebut($nilai / 1000000) . " Juta" . penyebut($nilai % 1000000);
                                    } else if ($nilai < 1000000000000) {
                                        $temp = penyebut($nilai / 1000000000) . " Milyar" . penyebut(fmod($nilai, 1000000000));
                                    } else if ($nilai < 1000000000000000) {
                                        $temp = penyebut($nilai / 1000000000000) . " Trilyun" . penyebut(fmod($nilai, 1000000000000));
                                    }
                                    return $temp;
                                }

                                function terbilang($nilai)
                                {
                                    if ($nilai < 0) {
                                        $hasil = "Minus " . trim(penyebut($nilai));
                                    } else {
                                        $hasil = trim(penyebut($nilai));
                                    }
                                    return $hasil;
                                }


                                foreach ($datachild as $key => $val) {





                                    $datasoldto = \common\models\RfcZZcsdShiptoSki::find()->where(['ID' => $val->id_soldto])->one();
                                    $childsoldto = \common\models\ChildSholdto::find()->where(['ID' => $val->id_shipto])->one();
                                    $plant = \common\models\RfcZZappSelectSysplanSki::find()->where(['ID' => $val->id_plant])->one();
                                    $material = \common\models\RfcZZcsdListMatSalesSki::find()->where(['ID' => $val->id_material])->one();
                                    $tipeangkut = \common\models\MasterAngkut::find()->where(['id' => $val->id_angkut])->one();
                                    $top = \common\models\MasterTop::find()->where(['id' => $val->id_top])->one();
                                    $tax = \common\models\MasterTax::find()->where(['id' => $val->id_tax])->one();
                                    $datachildmatching = \common\models\ChildMatching::find()->where(['id_matching' => $model2->id])->andWhere(['id_child_so' => $val->ID])->one();
                                    if ($datachildmatching) {
                                        $datacekso = \common\models\OPpSki::find()->where(['ID' => $val->id_pp])->one();
                                        $noso = $datacekso->order_number;
                                        $idchild = $datachildmatching->id;
                                        $nodo = $datachildmatching->no_do;
                                        $quantitychild = $datachildmatching->quantity;
                                    } else {
                                        $noso = '-';
                                        $idchild = 0;
                                        $nodo = '-';
                                        $quantitychild = 0;
                                    }


                                    $angka = $quantitychild;
                                    $konversiNilai = terbilang($angka);


                                ?>

                                    <tr>
                                        <td style="padding: 5px; text-align: center;"><?= $no + $key ?></td>
                                        <td style="padding: 5px; text-align: center;"><?= $childsoldto->KODE_SHIPTO . '/ ' . $childsoldto->NAMA_SHIPTO ?></td>
                                        <td style="padding: 5px; text-align: center;"><?= $childsoldto->ALAMAT ?></td>

                                        <td style="padding: 5px; text-align: center;"><?= $quantitychild ?></td>
                                        <td style="padding: 5px; text-align: center;"><?= $konversiNilai; ?></td>
                                        <td style="padding: 5px; text-align: center;"><?= $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI ?></td>
                                        <td style="padding: 5px; text-align: center;"><?= $val->uom ?></td>
                                        <td style="padding: 5px; text-align: left;"></td>
                                       


                                    </tr>
                                <?php } ?>
                                <tr>
                                   

                                    <td style="padding: 5px; text-align: left;"></td>
                                    <td style="padding: 5px; text-align: left;">T Tangan Penyerahan</td>

                                    <td style="padding: 5px; text-align: left;">T T Supir</td>
                                    <td style="padding: 5px; text-align: left;">T T Stempel Penerima</td>
                                    <td style="padding: 5px; text-align: left;">Truk Datang</td>
                                    <td style="padding: 5px; text-align: left;">Selesai Bongkar</td>
                                    <td style="padding: 5px;text-align: left;">Terima Semen Baik:</td>
                                    <td style="padding: 5px;text-align: center;"></td>


                                </tr>
                                <tr>
                                 

                                    <td style="padding: 5px; text-align: left;"></td>
                                    <td style="padding: 5px; text-align: center;"></td>

                                    <td style="padding: 5px; text-align: left;"></td>
                                    <td style="padding: 5px; text-align: left;"></td>
                                    <td style="padding: 5px; text-align: left;">Tgl :</td>
                                    <td style="padding: 5px; text-align: left;">Tgl :</td>
                                    <td style="padding: 5px;text-align: left;">Klaim Jumlah Semen</td>
                                    <td style="padding: 5px;text-align: center;"></td>


                                </tr>
                                <tr>
                                  

                                    <td style="padding: 5px; text-align: left;"></td>
                                    <td style="padding: 5px; text-align: left;"></td>

                                    <td style="padding: 5px; text-align: left;"></td>
                                    <td style="padding: 5px; text-align: left;"></td>
                                    <td style="padding: 5px; text-align: left;">Jam :</td>
                                    <td style="padding: 5px; text-align: left;">Jam :</td>
                                    <td style="padding: 5px;text-align: left;">Klaim K Pecah</td>
                                    <td style="padding: 5px;text-align: center;"></td>


                                </tr>
                            </tbody>
                        </table>
                        <!-- <p>Note: Kapasitas Muatan Diluar Tanggung Jawab Perusahaan</p>
                        <p>Kiriman Dalam Radius 10 KM. Tanpa Biaya Tambahan</p> -->
                    </div>
                </div>
            </section>
        </div>


    </div>



    <script src="<?= Yii::getAlias('@web/js/jquery-3.1.1.js') ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            window.print();

            //  window.close();
        });
    </script>
</body>

</html>