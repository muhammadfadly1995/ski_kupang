<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ChildMatching;
use common\models\DataTruck;
use common\models\OPpSki;
use common\models\Supir;
use common\models\TableExpeditur;

use common\models\ChildOPpSki;
use common\models\ChildSholdto;
use common\models\Conveyor;
use common\models\MasterAngkut;
use common\models\MasterTax;
use common\models\MasterTop;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model2 common\models\ProsesTimbangan */

$this->title = $model2->no_matching;
$this->params['breadcrumbs'][] = ['label' => 'Proses Timbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
    .center {
  margin: 0;
  position: absolute;
  top: 50%;
  left: 50%;
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);}
</style>
<div class="proses-timbangan-view">
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="table-matching-view">

                    <h1><?= Html::encode($this->title) ?></h1>


                    <div class="col-md-6">
                        <?= DetailView::widget([
                            'model' => $model2,
                            'attributes' => [
                                //  'id',
                                'no_matching',
                                'id_supir',
                                // [
                                //     'attribute' => 'id_supir',
                                //     'label' => 'Nama Supir',
                                //     'contentOptions' => ['class' => 'text-left'],
                                //     'value' => function ($model2) {
                                //         $dataso = Supir::find()->where(['id' => $model2->id_supir])->one();
                                //         return $dataso->nama_supir . '/' . $dataso->no_sim;
                                //     }
                                // ],
                                // 'no_so', [
                                    'id_truck',
                                // [
                                //     'attribute' => 'id_truck',
                                //     'label' => 'No Polisi',
                                //     'contentOptions' => ['class' => 'text-left'],
                                //     'value' => function ($model2) {
                                //         $datatruck = DataTruck::find()->where(['id' => $model2->id_truck])->one();
                                //         return $datatruck->no_pol;
                                //     }
                                // ],


                                // 'create_by',
                                // 'create_date',
                                // 'update_by',
                                // 'update_date',
                                // 'delete_mark',
                            ],
                        ]) ?>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'no_spj',
                                'tanggal_masuk',
                                [
                                    //'attribute' => 'id_truck',
                                    'label' => 'Conveyor',
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value' => function ($model) {
                                        $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                                        if ($datatruck) {
                                            return $datatruck->code_conveyor . '/' . $datatruck->deskripsi;
                                        }
                                    }
                                ],
                                //'id_conveyor',
                                'tanggal_keluar',
                                'jam_keluar',


                            ],
                        ]) ?>
                    </div>
                    <div class="col-md-6">
                        <?= DetailView::widget([
                            'model' => $model2,
                            'attributes' => [
                                // [
                                //     //'attribute' => 'id_truck',
                                //     'label' => 'Kapasitas Truck',
                                //     'contentOptions' => ['class' => 'text-left'],
                                //     'value' => function ($model2) {
                                //         $datatruck = DataTruck::find()->where(['id' => $model2->id_truck])->one();
                                //         return $datatruck->kapasitas_angkut . ' ' . $datatruck->satuan;
                                //     }
                                // ],
                                // [
                                //     //'attribute' => 'id_truck',
                                //     'label' => 'Supir/Sim',
                                //     'contentOptions' => ['class' => 'text-left'],
                                //     'value' => function ($model2) {
                                //         $datatruck = DataTruck::find()->where(['id' => $model2->id_truck])->one();
                                //         $datasupir = Supir::find()->where(['id' => $datatruck->id_supir])->one();
                                //         return $datasupir->nama_supir . '/' . $datasupir->no_sim;
                                //     }
                                // ],
                                [
                                    //'attribute' => 'id_truck',
                                    'label' => 'Expeditur',
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value' => function ($model2) {
                                        $dataexpeditur = TableExpeditur::find()->where(['id' => $model2->id_expeditur])->one();
                                        // $datasupir=Supir::find()->where(['id'=>$datatruck->id_supir])->one();
                                        return $dataexpeditur->code_expeditur . '/' . $dataexpeditur->deskripsi;
                                    }
                                ],
                            ],
                        ]) ?>
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'jam_masuk',
                                [
                                    //'attribute' => 'nilai_masuk',
                                    'label' => 'Nilai Timbangan Masuk',
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value' => function ($model) {
                                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                                        return number_format($model->nilai_masuk) . ' KG';
                                    }
                                ],
                                [
                                    //'attribute' => 'nilai_keluar',
                                    'label' => 'Nilai Timbangan Keluar',
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value' => function ($model) {
                                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                                        return number_format($model->nilai_keluar) . ' KG';
                                    }
                                ],
                                [
                                    //'attribute' => 'nilai_akhir',
                                    'label' => 'Berat Akhir',
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value' => function ($model) {
                                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                                        return number_format($model->nilai_akhir) . ' KG';
                                    }
                                ],




                            ],
                        ]) ?>
                    </div>
                </div>

            </div>
            <br>
            <?php if ($status == 'timbangankeluar') {
            ?>
             <div class="row">
                    <div class="box">
                        <div class="box-body">
                            <div class="proses-timbangan-form">

                                <?php $form = ActiveForm::begin(); ?>
                                <!-- <div class="col-md-12">
                                    <?= $form->field($model, 'nilai_keluar')->textInput(['placeholder' => 'Nilai Timbangan','style' => 'text-align:center; height: 70px ', 'readOnly' => FALSE,'autofocus' => 'autofocus'])->label(false) ?>
                                </div>  -->
                                



                                <div class="col-md-12">
                                    <div class="form-group">
                                    <?= Html::a('Next', ['input-number','id'=>$model->id,'idmat'=>$idmat,'status'=>$status], ['class' => 'btn btn-success btn-block']) ?> 
                                           </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                          <?= Html::a('Kembali', ['create'], ['class' => 'btn btn-default btn-block']) ?>
                                    </div>
                                </div>
                                <?php ActiveForm::end(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php if ($id == null) { ?>
                <div class="row">
                    <div class="box">
                        <div class="box-body">
                            <div class="proses-timbangan-form">

                                <?php $form = ActiveForm::begin(); ?>
                                <!-- <div class="col-md-12">
                                    <?= $form->field($model, 'nilai_masuk')->textInput(['placeholder' => 'Nilai Timbangan','style' => 'text-align:center; height: 70px ', 'readOnly' => false,'autofocus' => 'autofocus'])->label(false) ?>
                                </div> -->
                                <div class="col-md-12">
                                    <?= $form->field($model, 'id_conveyor')->widget(\kartik\select2\Select2::classname(), [
                                        'data' => ArrayHelper::map(common\models\Conveyor::find()->where(['delete_mark' => 0])->asArray()->all(), 'id', 'code_conveyor', 'deskripsi'),
                                        'language' => 'eng',
                                        'options' => ['placeholder' => 'Pilih Conveyor...'],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ])->label(false);
                                    ?>
                                </div>



                                <div class="col-md-12">
                                    <div class="form-group">
                                        <?= Html::submitButton('NEXT', ['class' => 'btn btn-success btn-block']) ?>
                                           </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                          <?= Html::a('Kembali', ['create'], ['class' => 'btn btn-default btn-block']) ?>
                                    </div>
                                </div>
                                <?php ActiveForm::end(); ?>

                            </div>
                        </div>
                    </div>
                </div>
            <?php } else if($id) { ?>
                <?= Html::a('Entri Baru', ['create'], ['class' => 'btn btn-default']) ?>||
                <?= Html::a('Cetak SPJ', ['cetak-spj', 'id' => $id, 'idmat' => $idmat, 'status' => $status], ['class' => 'btn btn-success', 'target' => '_blank']) ?>
                <br>
            <?php } ?>
            <div class="row">
                <div class="box-header">
                    <h1 class="box-title"><?= 'Detail Matching' ?> </h1>

                    <br>


                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table>
                            <thead style="background-color: #7fc6b6;">
                                <tr>
                                    <th>No</th>
                                    <th>No SO</th>
                                    <th>No DO</th>
                                    <th>Soldto</th>
                                    <th>Shipto</th>
                                    <th>Plant</th>
                                    <th>Material</th>
                                    <th>Tipe Angkut</th>

                                    <th>Quantity Matching</th>
                                    <th>UOM</th>
                                    <th>Tanggal Order</th>





                                </tr>
                            </thead>
                            <?php if ($datachild) {
                                $no = 1;
                                foreach ($datachild as $key => $val) {
                                    $datasoldto = RfcZZcsdShiptoSki::find()->where(['ID' => $val->id_soldto])->one();
                                    $childsoldto = ChildSholdto::find()->where(['ID' => $val->id_shipto])->one();
                                    $plant = RfcZZappSelectSysplanSki::find()->where(['ID' => $val->id_plant])->one();
                                    $material = RfcZZcsdListMatSalesSki::find()->where(['ID' => $val->id_material])->one();
                                    $tipeangkut = MasterAngkut::find()->where(['id' => $val->id_angkut])->one();
                                    $top = MasterTop::find()->where(['id' => $val->id_top])->one();
                                    $tax = MasterTax::find()->where(['id' => $val->id_tax])->one();
                                    $datachildmatching = ChildMatching::find()->where(['id_matching' => $model2->id])->andWhere(['id_child_so' => $val->ID])->one();
                                    if ($datachildmatching) {
                                        $datacekso = OPpSki::find()->where(['ID' => $val->id_pp])->one();
                                        $noso = $datacekso->order_number;
                                        $idchild = $datachildmatching->id;
                                        $nodo = $datachildmatching->no_do;
                                        $quantitychild = $datachildmatching->quantity;
                                    } else {
                                        $noso = '-';
                                        $idchild = 0;
                                        $nodo = '-';
                                        $quantitychild = 0;
                                    }
                            ?>

                                    <tr>
                                        <td><?= $no + $key ?></td>
                                        <td><?= $noso; ?></td>
                                        <td><?= $nodo; ?></td>
                                        <td><?= $datasoldto->KODE_SOLDTO . '/ ' . $datasoldto->NAMA_SOLDTO ?></td>
                                        <td><?= $childsoldto->KODE_SHIPTO . '/ ' . $childsoldto->NAMA_SHIPTO ?></td>
                                        <td><?= $plant->WERKS . '/ ' . $plant->NAME1 ?></td>
                                        <td><?= $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI ?></td>
                                        <td><?= $tipeangkut->code_angkut ?></td>

                                        <td> <?= $quantitychild ?></td>
                                        <td><?= $val->uom ?></td>
                                        <td><?= $val->date_order ?></td>




                                    </tr>

                            <?php }
                            } ?>


                        </table>
                        <br>

                    </div>
                </div>
            </div>

        </div>
    </div>

</div>