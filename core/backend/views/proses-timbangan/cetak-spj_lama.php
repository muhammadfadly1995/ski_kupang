<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ChildMatching;
use common\models\DataTruck;
use common\models\OPpSki;
use common\models\Supir;
use common\models\TableExpeditur;

use common\models\ChildOPpSki;
use common\models\ChildSholdto;
use common\models\Conveyor;
use common\models\MasterAngkut;
use common\models\MasterTax;
use common\models\MasterTop;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model2 common\models\ProsesTimbangan */

$this->title = $model->no_spj;
$this->params['breadcrumbs'][] = ['label' => 'Proses Timbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<html>

<head>
    <link href="<?= Yii::getAlias('@web/css/bootstrap.min.css') ?>" rel="stylesheet" media="print">

</head>

<body onload="window.print()" onfocus="window.close()">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {

            window.print();

            //  window.close();
        });
    </script>

    <div class="box">
        <div class="box-body">
            <div class="row">


                <h1><?= Html::encode($this->title) ?></h1>

                <div class="table-responsive">
                    <table>

                        <tr>
                            <th>No Matching</th>
                            <th style="text-align:center ;">:</th>
                            <th> <?= $model2->no_matching ?></th>
                            <th> </th>
                            <th>Nama Supir</th>
                            <th style="text-align:center ;">:</th>
                            <th><?php
                                $dataso = Supir::find()->where(['id' => $model2->id_supir])->one();
                                echo $dataso->nama_supir . '/' . $dataso->no_sim;  ?> </th>
                        </tr>
                        <tr>
                            <th>No Polisi</th>
                            <th style="text-align:center ;">:</th>
                            <th> <?php
                            
                            $datatruck = DataTruck::find()->where(['id' => $model2->id_truck])->one();
                            echo $datatruck->no_pol; ?></th>
                            <th> </th>
                            <th>No Spj</th>
                            <th style="text-align:center ;">:</th>
                            <th><?php
                                echo $model->no_spj  ?> </th>
                        </tr>
                        <tr>
                            <th>Tanggal Masuk </th>
                            <th style="text-align:center ;">:</th>
                            <th> <?= $model->tanggal_masuk ?></th>
                            <th> </th>
                            <th>Conveyor</th>
                            <th style="text-align:center ;">:</th>
                            <th><?php
                               $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                               if ($datatruck) {
                                   echo $datatruck->code_conveyor . '/' . $datatruck->deskripsi;
                               }  ?> </th>
                        </tr>
                        <tr>
                            <th>Tanggal Keluar</th>
                            <th style="text-align:center ;">:</th>
                            <th> <?= $model->tanggal_keluar ?></th>
                            <th> </th>
                            <th>Jam Keluar</th>
                            <th style="text-align:center ;">:</th>
                            <th><?php
                               echo $model->jam_keluar  ?> </th>
                        </tr>
                        <tr>
                            <th>Kapasitas Truck</th>
                            <th style="text-align:center ;">:</th>
                            <th> <?php
                             $datatruck = DataTruck::find()->where(['id' => $model2->id_truck])->one();
                             echo $datatruck->kapasitas_angkut . ' ' . $datatruck->satuan;
                         
                            ?></th>
                            <th> </th>
                            <th>Supir/Sim</th>
                            <th style="text-align:center ;">:</th>
                            <th><?php
                                $datatruck = DataTruck::find()->where(['id' => $model2->id_truck])->one();
                                $datasupir = Supir::find()->where(['id' => $datatruck->id_supir])->one();
                                echo $datasupir->nama_supir . '/' . $datasupir->no_sim;  ?> </th>
                        </tr>
                        <tr>
                            <th>Expeditur</th>
                            <th style="text-align:center ;">:</th>
                            <th> <?php
                               $dataexpeditur = TableExpeditur::find()->where(['id' => $model2->id_expeditur])->one();
                               // $datasupir=Supir::find()->where(['id'=>$datatruck->id_supir])->one();
                               echo $dataexpeditur->code_expeditur . '/' . $dataexpeditur->deskripsi;
         
                            ?></th>
                            <th> </th>
                            <th>Jam Masuk</th>
                            <th style="text-align:center ;">:</th>
                            <th><?php
                               echo $model->jam_masuk  ?> </th>
                        </tr>
                        <tr>
                            <th>Nilai Timbangan Masuk</th>
                            <th style="text-align:center ;">:</th>
                            <th> <?=  $model->nilai_masuk . ' KG' ?></th>
                            <th> </th>
                            <th>Nilai Timbangan Keluar</th>
                            <th style="text-align:center ;">:</th>
                            <th><?php
                               echo $model->nilai_keluar . ' KG';  ?> </th>
                        </tr>
                        <tr>
                            
                           
                            <th>Status</th>
                            <th style="text-align:center ;">:</th>
                            <th><?php 
                            if($model2->status_matching==30){
                                $status='Timbangan Masuk';

                            }else if($model2->status_matching==50){
                                $status='Timbangan Keluar';
                            }else{
                                $status='*****';
                            }
                            echo $status;
                            ?></th>
                            <th></th>
                            <th>Berat Akhir</th>
                            <th style="text-align:center ;">:</th>
                            <th> <?= $model->nilai_akhir . ' KG' ?></th>
                        </tr>

                    </table>
                </div>


              


            </div>
            <br>

            <div class="row">
                <div class="box-header">
                    <h1 class="box-title"><?= 'Detail Matching' ?> </h1>

                    <br>


                </div>
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table>
                            <thead style="background-color: #7fc6b6;">
                                <tr>
                                    <th>No</th>
                                    <th>No SO</th>
                                    <th>No DO</th>
                                    <th>Soldto</th>
                                    <th>Shipto</th>
                                    <th>Plant</th>
                                    <th>Material</th>
                                    <th>Tipe Angkut</th>

                                    <th>Quantity Matching</th>
                                    <th>UOM</th>
                                    <th>Tanggal Order</th>





                                </tr>
                            </thead>
                            <?php if ($datachild) {
                                $no = 1;
                                foreach ($datachild as $key => $val) {
                                    $datasoldto = RfcZZcsdShiptoSki::find()->where(['ID' => $val->id_soldto])->one();
                                    $childsoldto = ChildSholdto::find()->where(['ID' => $val->id_shipto])->one();
                                    $plant = RfcZZappSelectSysplanSki::find()->where(['ID' => $val->id_plant])->one();
                                    $material = RfcZZcsdListMatSalesSki::find()->where(['ID' => $val->id_material])->one();
                                    $tipeangkut = MasterAngkut::find()->where(['id' => $val->id_angkut])->one();
                                    $top = MasterTop::find()->where(['id' => $val->id_top])->one();
                                    $tax = MasterTax::find()->where(['id' => $val->id_tax])->one();
                                    $datachildmatching = ChildMatching::find()->where(['id_matching' => $model2->id])->andWhere(['id_child_so' => $val->ID])->one();
                                    if ($datachildmatching) {
                                        $datacekso = OPpSki::find()->where(['ID' => $val->id_pp])->one();
                                        $noso = $datacekso->order_number;
                                        $idchild = $datachildmatching->id;
                                        $nodo = $datachildmatching->no_do;
                                        $quantitychild = $datachildmatching->quantity;
                                    } else {
                                        $noso = '-';
                                        $idchild = 0;
                                        $nodo = '-';
                                        $quantitychild = 0;
                                    }
                            ?>

                                    <tr>
                                        <td><?= $no + $key ?></td>
                                        <td><?= $noso; ?></td>
                                        <td><?= $nodo; ?></td>
                                        <td><?= $datasoldto->KODE_SOLDTO . '/ ' . $datasoldto->NAMA_SOLDTO ?></td>
                                        <td><?= $childsoldto->KODE_SHIPTO . '/ ' . $childsoldto->NAMA_SHIPTO ?></td>
                                        <td><?= $plant->WERKS . '/ ' . $plant->NAME1 ?></td>
                                        <td><?= $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI ?></td>
                                        <td><?= $tipeangkut->code_angkut ?></td>

                                        <td> <?= $quantitychild ?></td>
                                        <td><?= $val->uom ?></td>
                                        <td><?= $val->date_order ?></td>




                                    </tr>

                            <?php }
                            } ?>


                        </table>
                        <br>

                    </div>
                </div>
            </div>

        </div>
    </div>


</body>

</html>