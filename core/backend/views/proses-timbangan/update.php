<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProsesTimbangan */

$this->title = 'Update Proses Timbangan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Proses Timbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="proses-timbangan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
