<?php

use common\models\Conveyor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\ProsesTimbangan */
/* @var $form yii\widgets\ActiveForm */
use common\models\PtMasterDistrikSki;
use common\models\TableMatching;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;
?>

<div class="row">
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="box-header">
                        <h1 class="box-title"><?= 'Input No Truck' ?></h1>
                        <br>


                    </div>
                    <div class="table-matching-form">

                        <?php $form = ActiveForm::begin(); ?>


                        
                            <div class="col-md-10">
                            <?= $form->field($modeltruck, 'kode_plat')->textInput(['id' => 'nama','placeholder'=>'KODE PLAT', 'style' => 'text-align:center;text-transform:uppercase', 'required' => true])->label(false);
                            ?>
                            </div
                       

                        <div class="col-md-2">
                            <div class="form-group">
                                <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>

                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <?php
    $this->title = 'Data Timbangan';
    $this->params['breadcrumbs'][] = $this->title;
    ?>
    <div class="box">
        <div class="box-body">
            <div class="rfc-zzapp-select-sysplan-ski-index">

                <h3><?= Html::encode($this->title) ?></h3>
                <ul class="nav nav-tabs pull-right">
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/proses-timbangan/create','keterangan'=>'timbanganclose']) ?>"> Timbangan Close</a>
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/proses-timbangan/create','keterangan'=>'timbanganin']) ?>"> Timbangan Masuk</a>


            </ul>


                <?php // echo $this->render('_search', ['model' => $searchModel]); 
                ?>
                <?php
                $gridColumns = [
                    ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                  
                    [
                        'attribute' => 'id_matching',
                        'label' => 'No Matching',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            $dataso = TableMatching::find()->where(['id' => $model->id_matching])->one();
                            return $dataso->no_matching ;
                        }
                    ],
                    [
                        'attribute' => 'nama_supir',
                        'label' => 'Nama Supir',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            $dataso = TableMatching::find()->where(['id' => $model->id_matching])->one();
                            return $dataso->id_supir ;
                        }
                    ],
                    [
                        'attribute' => 'data_truck',
                        'label' => 'Plat Nomor',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            $dataso = TableMatching::find()->where(['id' => $model->id_matching])->one();
                            return $dataso->id_truck ;
                        }
                    ],
                  //  'id_matching',
                  [
                    'attribute' => 'id_conveyor',
                    'label' => 'Conveyor',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        if ($datatruck) {
                            return $datatruck->code_conveyor . '/' . $datatruck->deskripsi;
                        }
                    }
                ],
                   // 'total_nilai',
                   [
                    'attribute' => 'nilai_masuk',
                    'label' => 'Nilai Timbangan Masuk',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return number_format($model->nilai_masuk,2) . ' KG';
                    }
                ],
                [
                    'attribute' => 'nilai_keluar',
                    'label' => 'Nilai Timbangan Keluar',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return number_format($model->nilai_keluar,2) . ' KG';
                    }
                ],
                [
                    'attribute' => 'nilai_akhir',
                    'label' => 'Berat Akhir',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return number_format($model->nilai_akhir,2) . ' KG';
                    }
                ],
                    'tanggal_masuk',
                    'tanggal_keluar',
                    'jam_masuk',
                    'jam_keluar',
                    //'create_by',
                    //'create_date',
                    //'update_by',
                    //'update_date',
                    //'delete_mark',
                    [
                        'attribute' => 'delete_mark',
                        'label' => 'Status Transaksi',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            if($model->delete_mark==1){
                                $status='Transaksi Dicancel';
                            }else{
                                $status='Success';
                            }
                            return $status;
                        }
                    ],
                    'alasan_cancel',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => ' {myButton}',  // the default buttons + your custom button
                        'buttons' => [
                            'myButton' => function ($url, $model, $key) { 
                                $dataso = TableMatching::find()->where(['id' => $model->id_matching])->one();
                 
                                // render your custom button
                                return Html::a('View', ['view', 'idmat' => $dataso->no_matching,'id'=>$model->id,'status'=>'timbanganmasuk']);
                            }
                        ]
                    ]
                ];


                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'clearBuffers' => true, //optional
                ]);

                echo \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns
                ]);
                ?>



            </div>
        </div>
    </div>
</div>
<script>
    function myFunction() {
        var x = document.getElementById("nama");
        x.value = x.value.toUpperCase();
    }
</script>