<?php

use common\models\PtMasterDistrikSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RfcZZappSelectSysplanSkiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Plant';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzapp-select-sysplan-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
               
                'WERKS',
                'NAME1',
                'ALAMAT',
                [
                    'attribute' => 'ID_DISTRIK',
                    'value' => function ($model) {
                        $datakategori = PtMasterDistrikSki::find()->where(['ID' => $model->ID_DISTRIK])->one();
                       if($datakategori){
                        $status = $datakategori->KODE_DISTRIK.' / '.$datakategori->DISTRIK;
                       }else{
                        $status='Data Distrik Tidak Ditemukan';
                       }
                        
                       
                        return $status;
                    }
                ],
             //  'ID_DISTRIK',
                'LONGITUDE',
                'LATITUDE',
                'CREATE_BY',
                'CREATE_DATE',
                'LAST_UPDATE_DATE',
                'LAST_UPDATE_BY',
               
                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>



        </div>
    </div>
</div>