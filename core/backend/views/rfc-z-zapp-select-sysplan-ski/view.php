<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZappSelectSysplanSki */

$this->title = 'Detail Data ' . $model->NAME1;
$this->params['breadcrumbs'][] = ['label' => 'Rfc Z Zapp Select Sysplan Skis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzapp-select-sysplan-ski-view">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'ID',

                        'WERKS',
                        'NAME1',
                        'ALAMAT',
                        'ID_DISTRIK',
                        'LONGITUDE',
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'ID',
                       
                        'LATITUDE',
                        'CREATE_BY',
                        'CREATE_DATE',
                        'LAST_UPDATE_DATE',
                        'LAST_UPDATE_BY',

                    ],
                ]) ?>
            </div>

            <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
        </div>
    </div>
</div>