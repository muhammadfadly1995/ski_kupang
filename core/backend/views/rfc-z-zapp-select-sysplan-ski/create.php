<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZappSelectSysplanSki */

$this->title = 'Data Plant';
$this->params['breadcrumbs'][] = ['label' => 'Data Plant', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rfc-zzapp-select-sysplan-ski-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
