<?php

use common\models\ChildSholdto;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
use common\models\TableExpeditur;
use common\models\TypeTruck;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Table Sloc';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                //'id',
                'keterangan',
               // 'delete_mark',
                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
    </div>
</div>