<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TableSloc */

$this->title = 'Create Table Sloc';
$this->params['breadcrumbs'][] = ['label' => 'Table Slocs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-sloc-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
