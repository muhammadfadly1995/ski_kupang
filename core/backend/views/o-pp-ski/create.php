<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\OPpSki */

$this->title = 'Create SO';
$this->params['breadcrumbs'][] = ['label' => 'O Pp Skis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="opp-ski-create">
    <ul class="nav nav-tabs pull-right">
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/o-pp-ski/data-pp']) ?>"> Data PP</a>
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/o-pp-ski/create']) ?>"> Create PP</a>
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/o-pp-ski/']) ?>"> Approval PP</a>


    </ul>
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>