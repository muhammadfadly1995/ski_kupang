<?php

use common\models\PtMasterDistrikSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RfcZZcsdShiptoSkiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Approval Data';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzapp-select-sysplan-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>
            <ul class="nav nav-tabs pull-right">
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/o-pp-ski/data-pp']) ?>"> Data SO</a>
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/o-pp-ski/create']) ?>"> Create SO</a>
                <?php if (\yii::$app->user->identity->role == 'admin' ||\yii::$app->user->identity->role == 'admin') { ?>
                    <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/o-pp-ski/']) ?>"> Approval SO</a>
                <?php } ?>

            </ul>

 
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                'order_type',
                'nama_soldto',
                'order_number',
                // 'line_number',
                'create_by',
                [
                    'attribute' => 'create_date',
                   
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                       
                        return $model->create_date .'|'.$model->time_create;
                    }
                ],
                //'update_by',
                [
                    'attribute' => 'update_date',
                   
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                       
                        return $model->update_date .'|'.$model->time_update;
                    }
                ],
                //'delete_mark',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => ' {myButton}',  // the default buttons + your custom button
                    'buttons' => [
                        'myButton' => function ($url, $model, $key) {     // render your custom button
                            return Html::a('View', ['view', 'id' => $model->ID, 'status' => 'Approval']);
                        }
                    ]
                ]
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>



        </div>
    </div>
</div>