<?php

use common\models\Harga;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\money\MaskMoney;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
?>
<?php
$form = ActiveForm::begin();
?>

<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h3>Input Child Order</h3>

</div>
<?php if ($childcek == 1) { ?>
    <div class="modal-body">

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'id_plant')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZappSelectSysplanSki::find()->asArray()->all(), 'ID', 'WERKS', 'NAME1'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Plant...', 'id' => 'table-plant', 'onchange' => '$.post( "listregsoldto?id=' . '"+$(this).val(),
                function( data ) {
                    $( "#table-soldto").html( data );
                });
               
                ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'required' => true
                    ],
                ])->label('Pilih Plant');
                ?>
                <?= $form->field($model, 'id_soldto')->widget(\kartik\select2\Select2::classname(), [
                    // 'data' => ArrayHelper::map(common\models\RfcZZcsdShiptoSki::find()->asArray()->all(), 'ID', 'KODE_SOLDTO', 'NAMA_SOLDTO'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Sold To...', 'id' => 'table-soldto', 'onchange' => '$.post( "listregss?id=' . '"+$(this).val(),
            function( data ) {
                $( "#table-barang").html( data );
            });
           
            ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'required' => true
                    ],
                ])->label('Pilih Sold To');
                ?>

                <?= $form->field($model, 'id_shipto')->widget(\kartik\select2\Select2::classname(), [
                    //  'data' => ArrayHelper::map(common\models\ChildSholdto::find()->asArray()->all(), 'ID', 'KODE_SHIPTO', 'NAMA_SHIPTO'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Ship To...', 'id' => 'table-barang', 'onchange' => '$.post( "listregplant?id=' . '"+$(this).val(),
                function( data ) {
                    $( "#table-mat").html( data );
                });
               
                ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'required' => true
                    ],
                ])->label('Pilih Ship To');
                ?>



                <?= $form->field($model, 'id_material')->widget(\kartik\select2\Select2::classname(), [
                    //  'data' => ArrayHelper::map(common\models\RfcZZcsdListMatSalesSki::find()->asArray()->all(), 'ID', 'KODE_MATERIAL', 'DESKRIPSI'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Material...', 'id' => 'table-mat', 'onchange' => '$.post( "listregssmat?id=' . '"+$(this).val(),
            function( data ) {
                $( "#table-uom").html( data );
            });
           
            $.post( "getharga?id=' . '"+$(this).val(),
            function( data ) {
               
                $( "#id-harga").html( data );
            });
            ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Material');
                ?>

                <?= $form->field($model, 'id_angkut')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\MasterAngkut::find()->asArray()->all(), 'id', 'code_angkut', 'deskripsi'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Type Angkut   ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Tipe Angkut');
                ?>

                <?= $form->field($model, 'quantity')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'uom')->widget(\kartik\select2\Select2::classname(), [
                    //  'data' => ArrayHelper::map(common\models\ChildSholdto::find()->asArray()->all(), 'ID', 'KODE_SHIPTO', 'NAMA_SHIPTO'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Uom...', 'id' => 'table-uom'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true,
                    ],
                ]);
                ?>

                <?= $form->field($model, 'date_order')->textInput(['readOnly' => true]) ?>

                <?= $form->field($model, 'date_receipt')->label('Tanggal Terima')->widget(DatePicker::className(), [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-m-d', 'target' => '_Blank']
                ]);
                ?>

                <?= $form->field($model, 'id_top')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\MasterTop::find()->asArray()->all(), 'id', 'code_top', 'deskripsi'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih TOP...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('TOP');
                ?>



                <?= $form->field($model, 'id_tax')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\MasterTax::find()->asArray()->all(), 'id', 'deskripsi', 'tax_code'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Tax...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Tax');
                ?>



            </div>
        </div>
        <div class="modal-footer">
            <div class="col-md-12" style="text-align: left;">
                <?= Html::submitButton('Ubah', ['class' => 'btn btn-primary']) ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php } else { ?>
    <div class="modal-body">

        <div class="row">
            <div class="col-md-6">


                <?php
                $idmat = [];
                $cekharga = Harga::find()->where(['PLANT' => $cekdatachild1->id_plant])->andWhere(['SOLD_TO' => $cekdatachild1->id_soldto])->andWhere(['SHIP_TO' => $cekdatachild1->id_shipto])->all();
                // print_r($cekharga);die();
                foreach ($cekharga as $key => $val) {
                    $idmat[] = $val->KODE;
                }
                // print_r($idmat);die();
                ?>


                <?= $form->field($model, 'id_shipto')->widget(\kartik\select2\Select2::classname(), [
                      'data' => ArrayHelper::map(common\models\ChildSholdto::find()->where(['ID_SHOLDTO'=>$cekdatachild1->id_soldto])->asArray()->all(), 'ID', 'KODE_SHIPTO', 'NAMA_SHIPTO'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Ship To...', 'id' => 'table-barang', 'onchange' => '$.post( "listregplant?id=' . '"+$(this).val(),
                function( data ) {
                    $( "#table-mat").html( data );
                });
               
                ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'required' => true
                    ],
                ])->label('Pilih Ship To');
                ?>

                <?= $form->field($model, 'id_material')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZcsdListMatSalesSki::find()->where(['ID' => $idmat])->asArray()->all(), 'ID', 'KODE_MATERIAL', 'DESKRIPSI'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Material...', 'id' => 'table-mat', 'onchange' => '$.post( "listregssmat?id=' . '"+$(this).val(),
            function( data ) {
                $( "#table-uom").html( data );
            });
           
            $.post( "getharga?id=' . '"+$(this).val(),
            function( data ) {
               
                $( "#id-harga").html( data );
            });
            ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Material');
                ?>

                <?= $form->field($model, 'id_angkut')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\MasterAngkut::find()->asArray()->all(), 'id', 'code_angkut', 'deskripsi'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Type Angkut   ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Tipe Angkut');
                ?>

                <?= $form->field($model, 'quantity')->textInput() ?>
                <?= $form->field($model, 'uom')->widget(\kartik\select2\Select2::classname(), [
                    //  'data' => ArrayHelper::map(common\models\ChildSholdto::find()->asArray()->all(), 'ID', 'KODE_SHIPTO', 'NAMA_SHIPTO'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Uom...', 'id' => 'table-uom'],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'disabled' => true,
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-6">


                <?= $form->field($model, 'date_order')->textInput(['readOnly' => true]) ?>

                <?= $form->field($model, 'date_receipt')->label('Tanggal Terima')->widget(DatePicker::className(), [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-m-d', 'target' => '_Blank']
                ]);
                ?>

                <?= $form->field($model, 'id_top')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\MasterTop::find()->asArray()->all(), 'id', 'code_top', 'deskripsi'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih TOP...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('TOP');
                ?>



                <?= $form->field($model, 'id_tax')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\MasterTax::find()->asArray()->all(), 'id', 'deskripsi', 'tax_code'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Tax...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Tax');
                ?>



            </div>
        </div>
        <div class="modal-footer">
            <div class="col-md-12" style="text-align: left;">
                <?= Html::submitButton('Ubah', ['class' => 'btn btn-primary']) ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
<?php } ?>