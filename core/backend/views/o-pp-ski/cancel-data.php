<?php

use common\models\Harga;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\money\MaskMoney;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
?>
<?php
$form = ActiveForm::begin();
?>


<div class="modal-body">

    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'alasan_cancel')->textInput() ?>
        </div>
        <div class="modal-footer">
            <div class="col-md-12" style="text-align: left;">
                <?= Html::submitButton('Ubah', ['class' => 'btn btn-primary']) ?>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    </div>