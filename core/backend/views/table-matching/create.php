<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\TableMatching */

$this->title = 'Create Table Matching';
$this->params['breadcrumbs'][] = ['label' => 'Table Matchings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="table-matching-create">
    <ul class="nav nav-tabs pull-right">
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/table-matching/create']) ?>"> Create Matching</a>
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/table-matching']) ?>"> Waiting Matching</a>
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/table-matching/index-done']) ?>"> Data Matching</a>


    </ul>
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>