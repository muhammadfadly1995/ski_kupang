<?php

use common\models\ChildSholdto;
use common\models\DataTruck;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
use common\models\Supir;
use common\models\TableExpeditur;
use common\models\TypeTruck;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Done Matching';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>
           


            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => ' {myButton}',  // the default buttons + your custom button
                    'buttons' => [
                        'myButton' => function ($url, $model, $key) {     // render your custom button
                            return    Html::a('<i class="fa fa-book"></i>', ['view-cetak', 'id' => $model->id], ['class' => 'btn btn-black',  'title' => 'Views Data',]);
                        },
                       
                       
                    ]

                ],
                //  'id',
                'no_matching',
                'id_supir',
                // [
                //     'attribute' => 'id_supir',
                //     'label' => 'Nama Supir/SIM',
                //     'contentOptions' => ['class' => 'text-left'],
                //     'value' => function ($model) {
                //         $dataso = Supir::find()->where(['id' => $model->id_supir])->one();
                //         return $dataso->nama_supir . '/' . $dataso->no_sim;
                //     }
                // ],
                // 'no_so', [
                    'id_truck',
                // [
                //     'attribute' => 'id_truck',
                //     'label' => 'No Polisi',
                //     'contentOptions' => ['class' => 'text-left'],
                //     'value' => function ($model) {
                //         $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                //         return $datatruck->no_pol;
                //     }
                // ],
                // [
                //     //'attribute' => 'id_truck',
                //     'label' => 'Kapasitas Truck',
                //     'contentOptions' => ['class' => 'text-left'],
                //     'value' => function ($model) {
                //         $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                //         return $datatruck->kapasitas_angkut . ' ' . $datatruck->satuan;
                //     }
                // ],
                // [
                //     //'attribute' => 'id_truck',
                //     'label' => 'Supir/Sim',
                //     'contentOptions' => ['class' => 'text-left'],
                //     'value' => function ($model) {
                //         $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                //         $datasupir = Supir::find()->where(['id' => $datatruck->id_supir])->one();
                //         return $datasupir->nama_supir . '/' . $datasupir->no_sim;
                //     }
                // ],
                [
                    'attribute' => 'id_expeditur',
                    'label' => 'Expeditur',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $dataexpeditur = TableExpeditur::find()->where(['id' => $model->id_expeditur])->one();
                        // $datasupir=Supir::find()->where(['id'=>$datatruck->id_supir])->one();
                        return $dataexpeditur->code_expeditur . ' | ' . $dataexpeditur->deskripsi;
                    }
                ],
                [
                    //'attribute' => 'id_truck',
                    'label' => 'Status',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        if ($model->status_matching == 30) {
                            $status = 'Timbangan Masuk';
                        }
                        else if($model->delete_mark==1){
                            $status = 'Data Dicancel';
                        }
                        else if ($model->status_matching == 50) {
                            $status = 'Timbangan Keluar';
                        } else {
                            $status = 'Selesai Matching';
                        }
                        return $status;
                    }
                ],
                'alasan_cancel',

                // 'create_by',
                // 'create_date',
                // 'update_by',
                // 'update_date',
                // 'delete_mark',

                
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
    </div>
</div>