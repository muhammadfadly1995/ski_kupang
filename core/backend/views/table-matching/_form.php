<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\TableMatching */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="table-matching-form">

            <?php $form = ActiveForm::begin(); ?>



           
             <?= $form->field($model, 'id_supir')->textInput(['id' => 'nama', 'style' => 'text-transform:uppercase', 'required' => true]); ?>
            <?= $form->field($model, 'id_expeditur')->widget(\kartik\select2\Select2::classname(), [
                'data' => ArrayHelper::map(common\models\TableExpeditur::find()->where(['delete_mark' => 0])->asArray()->all(), 'id', 'code_expeditur','deskripsi'),
                'language' => 'eng',
                'options' => [
                    'placeholder' => 'Pilih Sold To...', 'onchange' => '$.post( "listregss?id=' . '"+$(this).val(),
        function( data ) {
            $( "#table-barang").html( data );
        });
       
        ',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Pilih Expeditur');
            ?>
            <?= $form->field($model, 'id_truck')->textInput(['id' => 'nama', 'style' => 'text-transform:uppercase', 'required' => true]); ?>


            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::a('Kembali', ['create'], ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>