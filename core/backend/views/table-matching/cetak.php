<?php

use common\models\ChildMatching;
use common\models\DataTruck;
use common\models\OPpSki;
use common\models\Supir;
use common\models\TableExpeditur;
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ChildOPpSki;
use common\models\ChildSholdto;
use common\models\MasterAngkut;
use common\models\MasterTax;
use common\models\MasterTop;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\TableMatching */

$this->title = $model->no_matching;
$this->params['breadcrumbs'][] = ['label' => 'Table Matchings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<html>

<head>
    <link href="<?= Yii::getAlias('@web/css/bootstrap.min.css') ?>" rel="stylesheet" media="print">
  
</head>

<body onload="window.print()" onfocus="window.close()">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
<script type="text/javascript">
        $(document).ready(function() {

            window.print();

          //  window.close();
        });
        
    </script>
    <div class="box">
        <div class="box-body">
            <div class="row">
                <div class="table-matching-view">

                  

        <div class="col-md-12">
                    <div class="col-md-12">
                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                //  'id',
                                'no_matching',
                                'id_supir',
                                // [
                                //     'attribute' => 'id_supir',
                                //     'label' => 'Nama Supir/SIM',
                                //     'contentOptions' => ['class' => 'text-left'],
                                //     'value' => function ($model) {
                                //         $dataso = Supir::find()->where(['id' => $model->id_supir])->one();
                                //         return $dataso->nama_supir . '/' . $dataso->no_sim;
                                //     }
                                // ],
                                // 'no_so', [
                                    'id_truck',
                                // [
                                //     'attribute' => 'id_truck',
                                //     'label' => 'No Polisi',
                                //     'contentOptions' => ['class' => 'text-left'],
                                //     'value' => function ($model) {
                                //         $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                                //         return $datatruck->no_pol;
                                //     }
                                // ],
                                // [
                                //     //'attribute' => 'id_truck',
                                //     'label' => 'Kapasitas Truck',
                                //     'contentOptions' => ['class' => 'text-left'],
                                //     'value' => function ($model) {
                                //         $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                                //         return $datatruck->kapasitas_angkut . ' ' . $datatruck->satuan;
                                //     }
                                // ],
                                // [
                                //     //'attribute' => 'id_truck',
                                //     'label' => 'Supir/Sim',
                                //     'contentOptions' => ['class' => 'text-left'],
                                //     'value' => function ($model) {
                                //         $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                                //         $datasupir = Supir::find()->where(['id' => $datatruck->id_supir])->one();
                                //         return $datasupir->nama_supir . '/' . $datasupir->no_sim;
                                //     }
                                // ],
                                [
                                    //'attribute' => 'id_truck',
                                    'label' => 'Expeditur',
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value' => function ($model) {
                                        $dataexpeditur = TableExpeditur::find()->where(['id' => $model->id_expeditur])->one();
                                        // $datasupir=Supir::find()->where(['id'=>$datatruck->id_supir])->one();
                                        return $dataexpeditur->code_expeditur . '/' . $dataexpeditur->deskripsi;
                                    }
                                ],
                                'create_by',
                                [
                                    'attribute' => 'create_date',
    
                                    'contentOptions' => ['class' => 'text-left'],
                                    'value' => function ($model) {
    
                                        return $model->create_date . '|' . $model->time_create;
                                    }
                                ],
                                // 'create_by',
                                // 'create_date',
                                // 'update_by',
                                // 'update_date',
                                // 'delete_mark',
                            ],
                        ]) ?>
                    </div>
                  
                </div>
                </div>
            </div>
            <br>



            <br>
            <div class="row">
                <div class="box-header">
                    <h1 class="box-title"><?= 'Detail Matching' ?> </h1>

                    <br>


                </div>
                <div class="col-md-12">
                <div class="table-responsive">
                    <table>
                        <thead style="background-color: #7fc6b6;">
                            <tr>
                                <th>No</th>
                                <th>No SO</th>
                                <th>No DO</th>
                                <th>Soldto</th>
                                <th>Shipto</th>
                                <th>Plant</th>
                                <th>Material</th>
                                <th>Tipe Angkut</th>
                               
                                <th>Quantity</th>
                                <th>UOM</th>
                               



                            </tr>
                        </thead>
                        <?php if ($datachild) {
                            $no = 1;
                            foreach ($datachild as $key => $val) {
                                $datasoldto = RfcZZcsdShiptoSki::find()->where(['ID' => $val->id_soldto])->one();
                                $childsoldto = ChildSholdto::find()->where(['ID' => $val->id_shipto])->one();
                                $plant = RfcZZappSelectSysplanSki::find()->where(['ID' => $val->id_plant])->one();
                                $material = RfcZZcsdListMatSalesSki::find()->where(['ID' => $val->id_material])->one();
                                $tipeangkut = MasterAngkut::find()->where(['id' => $val->id_angkut])->one();
                                $top = MasterTop::find()->where(['id' => $val->id_top])->one();
                                $tax = MasterTax::find()->where(['id' => $val->id_tax])->one();
                                $datachildmatching = ChildMatching::find()->where(['id_matching' => $model->id])->andWhere(['id_child_so' => $val->ID])->one();
                                if ($datachildmatching) {
                                    $datacekso = OPpSki::find()->where(['ID' => $val->id_pp])->one();
                                    $noso = $datacekso->order_number;
                                    $idchild = $datachildmatching->id;
                                    $nodo = $datachildmatching->no_do;
                                    $quantitychild = $datachildmatching->quantity;
                                } else {
                                    $noso = '-';
                                    $idchild = 0;
                                    $nodo = '-';
                                    $quantitychild = 0;
                                }
                        ?>

                                <tr>
                                    <td><?= $no + $key ?></td>
                                    <td><?= $noso; ?></td>
                                    <td><?= $nodo; ?></td>
                                    <td><?= $datasoldto->KODE_SOLDTO . '/ ' . $datasoldto->NAMA_SOLDTO ?></td>
                                    <td><?= $childsoldto->KODE_SHIPTO . '/ ' . $childsoldto->NAMA_SHIPTO ?></td>
                                    <td><?= $plant->WERKS . '/ ' . $plant->NAME1 ?></td>
                                    <td><?= $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI ?></td>
                                    <td><?= $tipeangkut->code_angkut ?></td>
                                    <td><?= $quantitychild ?></td>
                                    <td><?= $val->uom ?></td>
                                   




                                </tr>

                        <?php }
                        } ?>

                        
                    </table>
                    <br>
                   
                </div>
            </div>
            </div>

        </div>
    </div>

    

</body>

</html>