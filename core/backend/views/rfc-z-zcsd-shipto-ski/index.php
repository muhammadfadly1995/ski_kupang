<?php

use common\models\PtMasterDistrikSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RfcZZcsdShiptoSkiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Master Customer';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzapp-select-sysplan-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>
            <ul class="nav nav-tabs pull-right">
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/rfc-z-zcsd-shipto-ski']) ?>"> Data Soldto</a>
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/child-sholdto']) ?>"> Data Shipto</a>


            </ul>
            <p>
                <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                //   'ID',
                'NAMA_PEMILIK',
                'KODE_SOLDTO',
                'NAMA_SOLDTO',
                [
                    'attribute' => 'NILAI_JAMINAN',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        return 'Rp.' . number_format($model->NILAI_JAMINAN);
                    }
                ],
               // 'NILAI_JAMINAN',
                'ALAMAT:ntext',
                [
                    'attribute' => 'KODE_DSITRIK',
                    'value' => function ($model) {
                        $datakategori = PtMasterDistrikSki::find()->where(['ID' => $model->KODE_DSITRIK])->one();
                        if($datakategori){
                            $status = $datakategori->KODE_DISTRIK . ' / ' . $datakategori->DISTRIK;
                        }else{
                            $status='data Distrik Tidak DItemukan';
                        }
                        
                        return $status;
                    }
                ],
               // 'KODE_DSITRIK',
                'NAMA_DISTRIK',
                'email',
                'CREATE_BY',
                'CREATE_DATE',
                'LAST_UPDATE_BY',
                'LAST_UPDATE_DATE',
                'DELETE_MARK',

                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>



        </div>
    </div>
</div>