<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZcsdShiptoSki */

$this->title = 'Tambah Customer';
$this->params['breadcrumbs'][] = ['label' => 'Data Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rfc-zzcsd-shipto-ski-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
