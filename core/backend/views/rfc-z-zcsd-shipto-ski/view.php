<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\money\MaskMoney;
/* @var $this yii\web\View */
/* @var $model common\models\RfcZZcsdShiptoSki */

$this->title = $model->NAMA_SOLDTO;
$this->params['breadcrumbs'][] = ['label' => 'Data Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzcsd-shipto-ski-view">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'ID',
                        'NAMA_PEMILIK',
                        'KODE_SOLDTO',
                        'NAMA_SOLDTO',
                        [
                            'attribute' => 'NILAI_JAMINAN',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                return 'Rp.' . number_format($model->NILAI_JAMINAN);
                            }
                        ],

                        'ALAMAT:ntext',
                      //  'KODE_DSITRIK',

                        //   'DELETE_MARK',
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [

                        'NAMA_DISTRIK',
                        'email',
                        'CREATE_BY',
                        'CREATE_DATE',
                        'LAST_UPDATE_BY',
                        'LAST_UPDATE_DATE',
                        //   'DELETE_MARK',
                    ],
                ]) ?>
            </div>


        </div>
        <div class="col-md-12">
            <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
        </div>

    </div>
</div>
<div class="box">
    <div class="box-body">


        <h3><?= 'Data Shipto'; ?></h3>
        <?php $form = ActiveForm::begin([
            // 'layout' => 'horizontal',
        ]); ?>



        <div class="col-md-12">
            <div class="col-md-6">
            <?= $form->field($datasholdto, 'NAMA_PEMILIK')->textInput(['readOnly' => false, 'placeholder' => 'Nama Pemilik'])->label(false) ?>
           
                <?= $form->field($datasholdto, 'KODE_SHIPTO')->textInput(['readOnly' => false, 'placeholder' => 'Kode Shipto'])->label(false) ?>
           
                <?= $form->field($datasholdto, 'NAMA_SHIPTO')->textInput(['readOnly' => false, 'placeholder' => 'Nama Shipto'])->label(false) ?>
           
                <?= $form->field($datasholdto, 'ALAMAT')->textInput(['readOnly' => false, 'placeholder' => 'Alamat Shipto'])->label(false) ?>
            </div>
            <div class="col-md-6">
            <?= $form->field($datasholdto, 'DISTRIK_CODE')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\PtMasterDistrikSki::find()->asArray()->all(), 'ID', 'KODE_DISTRIK', 'DISTRIK'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Distrik...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label(false);
                ?>
           <?= $form->field($datasholdto, 'NILAI_JAMINAN')->widget(
                    MaskMoney::classname(),
                    [
                        'options' => [
                            //  'id' => 'harga-barang',
                            'onchange' => "myFunction()"
                        ],
                        'pluginOptions' => [
                            'prefix' => 'Rp. ',
                            'suffix' => '',
                            'placeholder'=>'Nilai Jaminan',
                            'affixesStay' => TRUE,
                            'precision' => 0,
                            'allowZero' => false,
                            'allowNegative' => false,

                        ],
                    ]
                )->label(FALSE);
                ?>
           <?= $form->field($datasholdto, 'PHONE')->textInput(['readOnly' => false, 'placeholder' => 'Phone'])->label(false) ?>
      
           <?= $form->field($datasholdto, 'EMAIL')->textInput(['readOnly' => false, 'placeholder' => 'Email'])->label(false) ?>
            </div>


        </div>
        <div class="col-md-12">
            <div class="form-group">
                <?= Html::submitButton($datasholdto->isNewRecord ? 'Create' : 'Update', ['class' => $datasholdto->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        </div>




        <?php ActiveForm::end(); ?>

        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered table-hover table-responsive table-striped" cellpadding="6" cellspacing="1" style="width:100%" border="1">
                    <thead style="background-color: #7fc6b6;">

                        <tr>
                            <th width="5%">No</th>
                            <th>Nama Pemilik</th>
                            <th>Kode Shipto</th>
                            <th>Nama Shipto</th>
                            <th>Kode Distrik</th>
                            <th>Phone</th>
                            <th>Email</th>
                            <th>Nilai Jaminan</th>

                            <th width="10%">Action</th>
                        </tr>
                    </thead>
                    <?php
                    $no = 1;
                    foreach ($dataSelect as $keydataSelect => $valdataSelect) {
                        //  $datasiswa = TableSiswa::find()->where(['nisn' => $valdatasiswakelas->nama_siswa])->one();
                    ?>
                        <tr>
                            <td><?= $no + $keydataSelect ?></td>
                            <td><?= $valdataSelect->NAMA_PEMILIK ?></td>
                            <td><?= $valdataSelect->KODE_SHIPTO ?></td>
                            <td><?= $valdataSelect->NAMA_SHIPTO ?></td>
                            <td><?= $valdataSelect->DISTRIK_CODE ?></td>
                            <td><?= $valdataSelect->PHONE ?></td>
                            <td><?= $valdataSelect->EMAIL ?></td>
                            <td><?= 'Rp.' . number_format($valdataSelect->NILAI_JAMINAN); ?></td>

                            <td><?= Html::a('<i class="fa fa-fw fa-trash"></i>', ['delete-soldto', 'id' => $valdataSelect->ID, 'ids' => $model->ID], ['class' => 'btn btn-black', 'title' => 'Hapus',  'data' => [
                                    'confirm' => 'Hapus?',
                                    'method' => 'post',
                                ],]) ?></td>


                        </tr>
                    <?php } ?>

                </table>
            </div>


        </div>


    </div>
</div>