<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Conveyor */

$this->title = 'Create Conveyor';
$this->params['breadcrumbs'][] = ['label' => 'Conveyors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conveyor-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
