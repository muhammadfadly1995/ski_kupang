<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Conveyor */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="conveyor-form">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'code_conveyor')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'deskripsi')->textInput(['maxlength' => true]) ?>



            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>