<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Conveyor */

$this->title = $model->code_conveyor;
$this->params['breadcrumbs'][] = ['label' => 'Conveyors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="conveyor-view">

            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //  'id',
                    'code_conveyor',
                    'deskripsi',
                    'create_by',
                    'create_date',
                    'update_by',
                    'update_date',
                    //  'delete_mark',
                ],
            ]) ?>

        </div>
        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>