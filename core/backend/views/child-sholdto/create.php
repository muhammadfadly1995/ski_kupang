<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ChildSholdto */

$this->title = 'Create Child Sholdto';
$this->params['breadcrumbs'][] = ['label' => 'Child Sholdtos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="child-sholdto-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
