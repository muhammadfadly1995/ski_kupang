<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ChildSholdto */

$this->title = 'Update Child Sholdto: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Child Sholdtos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="child-sholdto-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
