<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ChildSholdto */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Child Sholdtos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="child-sholdto-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'ID_SHOLDTO',
            'KODE_SHIPTO',
            'NAMA_SHIPTO',
            'ALAMAT:ntext',
            'DISTRIK_CODE',
            'PHONE',
            'EMAIL:email',
            'CREATE_BY',
            'UPDATE_BY',
            'UPDATE_DATE',
        ],
    ]) ?>

</div>
