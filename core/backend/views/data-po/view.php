<?php

use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\TableExpeditur;
use common\models\TableSloc;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DataPo */

$this->title ='Detail PO '. $model->id_com;
$this->params['breadcrumbs'][] = ['label' => 'Data Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?><div class="box">
    <div class="box-body">
        <div class="data-po-view">

            <h1><?= Html::encode($this->title) ?></h1>
            <?php if (\yii::$app->user->identity->role == 'admin'){?>
            <?php if ($model->status == 'Open') { ?>
                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
            <?php } ?>
<?php }?>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'id',
                        'id_com',
                        [
                            'attribute' => 'id_plant',
                            'label' => 'Plant',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                $datatruck = RfcZZappSelectSysplanSki::find()->where(['ID' => $model->id_plant])->one();
                                return $datatruck->WERKS.' '.$datatruck->NAME1;
                            }
                        ],
                       
                       // 'no_po',
                        [
                            'attribute' => 'vendor',
                            'label' => 'Vendor',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                $datatruck = TableExpeditur::find()->where(['id' => $model->vendor])->one();
                                return $datatruck->code_expeditur.' '.$datatruck->deskripsi;
                            }
                        ],
                        [
                            'attribute' => 'vendor_pbm',
                            'label' => 'Vendor PBM',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                $datatruck = TableExpeditur::find()->where(['id' => $model->vendor])->one();
                                return $datatruck->code_expeditur . ' ' . $datatruck->deskripsi;
                            }
                        ],
        
                       
                      //  'no_pol',
                        [
                            'attribute' => 'material',
                            'label' => 'Material',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                $datatruck = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->material])->one();
                                return $datatruck->KODE_MATERIAL.' '.$datatruck->DESKRIPSI;
                            }
                        ],
                      //  'material',
                      [
                        'attribute' => 'qty',
                        'label' => 'Qty',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                         
                            return $model->qty.' TO';
                        }
                    ],
                    [
                        'attribute' => 'sisa_qty',
                        'label' => 'Sisa Qty',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                         
                            return $model->sisa_qty.' TO';
                        }
                    ],
                     //   'qty',
                     [
                        'attribute' => 'sloc',
                        'label' => 'Sloc',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            $datatruck = TableSloc::find()->where(['id' => $model->sloc])->one();
                            return $datatruck->keterangan;
                        }
                    ],
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'id',
                       
                        'tanggal_datang',
                'nama_kapal',
                      //  'sloc',
                        'create_by',
                        'create_date',
                        'update_by',
                        'update_date',

                        'status',
                    ],
                ]) ?>
            </div>


        </div>
        <div class="col-md-12">
            <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
        </div>

    </div>
</div>