<?php

use common\models\ChildSholdto;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;


use common\models\TableExpeditur;
use common\models\TableSloc;
/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data PO';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
    <?php if (\yii::$app->user->identity->role == 'admin'){?>
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>
           
            <p>
                <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],

                //   'id',
                'id_com',
                [
                    'attribute' => 'id_plant',
                    'label' => 'Plant',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = RfcZZappSelectSysplanSki::find()->where(['ID' => $model->id_plant])->one();
                        return $datatruck->WERKS . ' ' . $datatruck->NAME1;
                    }
                ],
                'no_po',
                [
                    'attribute' => 'vendor',
                    'label' => 'Vendor',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = TableExpeditur::find()->where(['id' => $model->vendor])->one();
                        return $datatruck->code_expeditur . ' ' . $datatruck->deskripsi;
                    }
                ],
                [
                    'attribute' => 'vendor_pbm',
                    'label' => 'Vendor PBM',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = TableExpeditur::find()->where(['id' => $model->vendor])->one();
                        return $datatruck->code_expeditur . ' ' . $datatruck->deskripsi;
                    }
                ],

               // 'no_pol',
                [ 
                    'attribute' => 'material',
                    'label' => 'Material',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->material])->one();
                        return $datatruck->KODE_MATERIAL . ' ' . $datatruck->DESKRIPSI;
                    }
                ],
                [
                    'attribute' => 'qty',
                    //  'label' => 'Qty',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {

                        return number_format($model->qty,2) . ' TO';
                    }
                ],
                [
                    'attribute' => 'sisa_qty',
                    //  'label' => 'Qty',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {

                        return number_format($model->sisa_qty,2) . ' TO';
                    }
                ],
                [
                    'attribute' => 'sloc',
                    'label' => 'Sloc',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = TableSloc::find()->where(['id' => $model->sloc])->one();
                        return $datatruck->keterangan;
                    }
                ],
                'tanggal_datang',
                'nama_kapal',
                'create_by',
                'create_date',
                'update_by',
                'update_date',
                //'delete_mark',
                'status',

                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
        <?php }else if(\yii::$app->user->identity->role == 'Petugas Timbang'){?>
            <div class="pt-master-distrik-ski-index">

<h3><?= Html::encode($this->title) ?></h3>



<?php // echo $this->render('_search', ['model' => $searchModel]); 
?>
<?php
$gridColumns = [
    ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],

    //   'id',
    'id_com',
    [
        'attribute' => 'id_plant',
        'label' => 'Plant',
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($model) {
            $datatruck = RfcZZappSelectSysplanSki::find()->where(['ID' => $model->id_plant])->one();
            return $datatruck->WERKS . ' ' . $datatruck->NAME1;
        }
    ],
    'no_po',
    [
        'attribute' => 'vendor',
        'label' => 'Vendor',
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($model) {
            $datatruck = TableExpeditur::find()->where(['id' => $model->vendor])->one();
            return $datatruck->code_expeditur . ' ' . $datatruck->deskripsi;
        }
    ],

   // 'no_pol',
    [ 
        'attribute' => 'material',
        'label' => 'Material',
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($model) {
            $datatruck = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->material])->one();
            return $datatruck->KODE_MATERIAL . ' ' . $datatruck->DESKRIPSI;
        }
    ],
    [
        'attribute' => 'qty',
        //  'label' => 'Qty',
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($model) {

            return number_format($model->qty,2) . ' TO';
        }
    ],
    [
        'attribute' => 'sisa_qty',
        //  'label' => 'Qty',
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($model) {

            return number_format($model->sisa_qty,2) . ' TO';
        }
    ],
    [
        'attribute' => 'sloc',
        'label' => 'Sloc',
        'contentOptions' => ['class' => 'text-left'],
        'value' => function ($model) {
            $datatruck = TableSloc::find()->where(['id' => $model->sloc])->one();
            return $datatruck->keterangan;
        }
    ],
    'tanggal_datang',
    'nama_kapal',
    'create_by',
    'create_date',
    'update_by',
    'update_date',
    //'delete_mark',
    'status',

    [
        'class' => 'yii\grid\ActionColumn',
        'template' => ' {myButton}',  // the default buttons + your custom button
        'buttons' => [
            'myButton' => function ($url, $model, $key) {     // render your custom button
                return Html::a('View', ['view', 'id' => $model->id, 'status' => 'Approval']);
            }
        ]
    ]
];

echo ExportMenu::widget([
    'dataProvider' => $dataProvider,
    'columns' => $gridColumns,
    'clearBuffers' => true, //optional
]);

echo \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => $gridColumns
]);
?>


</div>
            <?php }?>
    </div>
</div>