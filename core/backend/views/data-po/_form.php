<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\DataPo */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="data-po-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'id_com')->textInput(['type' => 'number','readOnly'=>true]) ?>
                <?= $form->field($model, 'no_po')->textInput() ?>

                <?= $form->field($model, 'id_plant')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZappSelectSysplanSki::find()->asArray()->all(), 'ID', 'WERKS', 'NAME1'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Plant...', 'id' => 'table-plant',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'required' => true
                    ],
                ])->label('Pilih Plant');
                ?>



                <?= $form->field($model, 'vendor')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\TableExpeditur::find()->where(['delete_mark' => 0])->asArray()->all(), 'id', 'code_expeditur', 'deskripsi'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih ...', 'onchange' => '$.post( "listregss?id=' . '"+$(this).val(),
        function( data ) {
            $( "#table-barang").html( data );
        });
       
        ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Pilih Vendor'); ?>
                
                <?= $form->field($model, 'tanggal_datang')->widget(DatePicker::className(), [
                        'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-m-d', 'target' => '_Blank']
                    ]);
                    ?>
             

            </div>
            <div class="col-md-6">
                
            <?= $form->field($model, 'vendor_pbm')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\TableExpeditur::find()->where(['delete_mark' => 0])->asArray()->all(), 'id', 'code_expeditur', 'deskripsi'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih ...', 'onchange' => '$.post( "listregss?id=' . '"+$(this).val(),
        function( data ) {
            $( "#table-barang").html( data );
        });
       
        ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Pilih Vendor PBM'); ?>


            <?= $form->field($model, 'nama_kapal')->textInput(['style' => 'text-transform:uppercase']) ?>

                <?= $form->field($model, 'material')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZcsdListMatSalesSki::find()->asArray()->all(), 'ID', 'KODE_MATERIAL', 'DESKRIPSI'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Mat...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'required' => true
                    ],
                ])->label('Pilih Mat');
                ?>

                <?= $form->field($model, 'qty')->textInput() ?>

                <?= $form->field($model, 'sloc')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\TableSloc::find()->asArray()->all(), 'id', 'keterangan'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Sloc...',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true,
                        'required' => true
                    ],
                ])->label('Pilih Sloc');
                ?>
            </div>
            <div class="col-md-12">


                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>