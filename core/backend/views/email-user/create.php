<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\EmailUser */

$this->title = 'Tambah Email';
$this->params['breadcrumbs'][] = ['label' => 'Email Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="email-user-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
