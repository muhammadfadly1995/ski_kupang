<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ChildMatching;
use common\models\DataTruck;
use common\models\OPpSki;
use common\models\Supir;
use common\models\TableExpeditur;

use common\models\ChildOPpSki;
use common\models\ChildSholdto;
use common\models\Conveyor;
use common\models\MasterAngkut;
use common\models\MasterTax;
use common\models\MasterTop;
use common\models\ProsesTimbangan;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use common\models\TableMatching;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model2 common\models\ProsesTimbangan */

$this->title = $model->no_billing;
$this->params['breadcrumbs'][] = ['label' => 'Proses Timbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<html>

<head>
    <link href="<?= Yii::getAlias('@web/css/bootstrap.min.css') ?>" rel="stylesheet" media="print">

</head>

<body onload="window.print()" onfocus="window.close()">
    <style>
        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {

            window.print();

            //  window.close();
        });
    </script>

<div class="box">
    <div class="box-body">
        <div class="billing-view">

            <h1><?= Html::encode($this->title) ?></h1>
          


            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //  'id',
                    'no_billing',

                    [
                        //'attribute' => 'nilai_masuk',
                        'label' => 'Status Billing',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            if ($model->status_billing == 0) {
                                $status = 'Open';
                            } else {
                                $status = 'Closed';
                            }
                            return $status;
                        }
                    ],


                ],
            ]) ?>

        </div>
       
        <?php
        $nochildbilling = 1;
        foreach ($childbilling as $keychil => $valchild) {
            $dataspjtimbangan = ProsesTimbangan::find()->where(['id' => $valchild->no_spj])->one();
            $model2 = TableMatching::find()->where(['id' => $dataspjtimbangan->id_matching])->one();
            $datachildmatching = ChildMatching::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $model2->id])->all();
            $dataidchild = [];


            foreach ($datachildmatching as $key => $val) {
                $dataidchild[] = $val->id_so;
            }

            $datachild = ChildOPpSki::find()->where(['id_pp' => $dataidchild])->andWhere(['delete_mark' => 0])->andWhere(['status_konfirmasi' => 'Approval'])->all();
            $sumdatachild = ChildMatching::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $model2->id])->sum('quantity');

        ?>
            <div class="box-header">
                <h3 class="box-title"><?= $nochildbilling + $keychil . '.No SPJ : ' . $dataspjtimbangan->no_spj ?></h3>
                <br>

            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table>
                        <thead style="background-color: #7fc6b6;">
                            <tr>
                                <th>No</th>

                                <th>No SO</th>
                                <th>No DO</th>
                                <th>Soldto</th>
                                <th>Shipto</th>
                                <th>Plant</th>
                                <th>Material</th>
                                <th>Tipe Angkut</th>

                                <th>Quantity Matching</th>
                                <th>Harga</th>
                                <th>Tax</th>
                                <th>Subtotal</th>

                                <th>UOM</th>
                                <th>Tanggal Order</th>





                            </tr>
                        </thead>
                        <?php if ($datachild) {
                            $no = 1;
                            $totalquantity = 0;
                            $subtotalharga = 0;
                            foreach ($datachild as $key => $val) {
                                $datasoldto = RfcZZcsdShiptoSki::find()->where(['ID' => $val->id_soldto])->one();
                                $childsoldto = ChildSholdto::find()->where(['ID' => $val->id_shipto])->one();
                                $plant = RfcZZappSelectSysplanSki::find()->where(['ID' => $val->id_plant])->one();
                                $material = RfcZZcsdListMatSalesSki::find()->where(['ID' => $val->id_material])->one();
                                $tipeangkut = MasterAngkut::find()->where(['id' => $val->id_angkut])->one();
                                $top = MasterTop::find()->where(['id' => $val->id_top])->one();
                                $tax = MasterTax::find()->where(['id' => $val->id_tax])->one();
                                $datachildmatching = ChildMatching::find()->where(['id_matching' => $model2->id])->andWhere(['id_child_so' => $val->ID])->one();
                                if ($datachildmatching) {
                                    $datacekso = OPpSki::find()->where(['ID' => $val->id_pp])->one();
                                    $noso = $datacekso->order_number;
                                    $idchild = $datachildmatching->id;
                                    $nodo = $datachildmatching->no_do;
                                    $quantitychild = $datachildmatching->quantity;
                                } else {
                                    $noso = '-';
                                    $idchild = 0;
                                    $nodo = '-';
                                    $quantitychild = 0;
                                }
                                $totalquantity = $totalquantity + $quantitychild;
                                $subtotalharga = $subtotalharga + $val->subtotal;
                        ?>

                                <tr>
                                    <td><?= $no + $key ?></td>

                                    <td><?= $noso; ?></td>
                                    <td><?= $nodo; ?></td>
                                    <td><?= $datasoldto->KODE_SOLDTO . '/ ' . $datasoldto->NAMA_SOLDTO ?></td>
                                    <td><?= $childsoldto->KODE_SHIPTO . '/ ' . $childsoldto->NAMA_SHIPTO ?></td>
                                    <td><?= $plant->WERKS . '/ ' . $plant->NAME1 ?></td>
                                    <td><?= $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI ?></td>
                                    <td><?= $tipeangkut->code_angkut ?></td>

                                    <td> <?= $quantitychild ?></td>
                                    <td><?= 'Rp.' . number_format($val->harga); ?></td>
                                    <td><?= $tax->deskripsi . '(' . $tax->nominal_tax . '%) ' . ' ' . $tax->tax_code ?></td>
                                    <td><?= 'Rp.' . number_format($val->subtotal); ?></td>
                                    <td><?= $val->uom ?></td>
                                    <td><?= $val->date_order ?></td>




                                </tr>

                        <?php }
                        } ?>

                        <tr>
                            <th></th>
                            <th>Subtotal</th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th><?= $totalquantity . ' Item' ?></th>
                            <th></th>
                            <th></th>
                            <th><?= 'Rp.' . number_format($subtotalharga); ?></th>
                            <th></th>
                            <th></th>
                        </tr>

                    </table>
                    <br>

                </div>
            </div>

        <?php } ?>
    </div>
</div>


</body>

</html>