<?php

use common\models\ChildBilling;
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ChildMatching;
use common\models\DataTruck;
use common\models\OPpSki;
use common\models\Supir;
use common\models\TableExpeditur;

use common\models\ChildOPpSki;
use common\models\ChildSholdto;
use common\models\Conveyor;
use common\models\MasterAngkut;
use common\models\MasterTax;
use common\models\MasterTop;
use common\models\ProsesTimbangan;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use common\models\TableMatching;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Billing */

$this->title = $model->no_billing;
$this->params['breadcrumbs'][] = ['label' => 'Billings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<div class="box">
    <div class="box-body">
        <div class="billing-view">

            <h3><?= Html::encode($this->title) ?></h3>
            <?php
            if ($model->status_billing == 0) {
            ?>
                <!-- <p>
                    <?= Html::a('Simpan data Billing', ['simpan-billing', 'id' => $model->id], ['class' => 'btn btn-primary', 'data' => [
                        'confirm' => 'Simpan Data ?',
                        'method' => 'post',
                    ],]) ?> -->

                </p>
            <?php } else { ?>
                <?= Html::a('Cetak Billing', ['cetak-data', 'id' => $model->id], ['class' => 'btn btn-primary', 'target' => '_blank', 'data' => [
                    'confirm' => 'Cetak Billing?',
                    'method' => 'post',
                ],]) ?>
                <?= Html::a('Selesai', ['index'], ['class' => 'btn btn-default']) ?>
            <?php   } ?>


            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //  'id',
                    'no_billing',
                    [
                        'attribute' => 'subtotal',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            return 'Rp.' . number_format($model->subtotal);
                        }
                    ],
                    //  'subtotal',
                    [
                        //'attribute' => 'nilai_masuk',
                        'label' => 'Status Billing',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            if ($model->status_billing == 0) {
                                $status = 'Open';
                            } else {
                                $status = 'Closed';
                            }
                            return $status;
                        }
                    ],
                    'create_date',
                    'tanggal_billing',
                    [
                        //'attribute' => 'nilai_masuk',
                        'label' => 'TOP',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            $datatop=MasterTop::find()->where(['id'=>$model->id_top])->one();
                            if($datatop){
                                $status=$datatop->code_top.' '.$datatop->deskripsi;
                            }else{
                                $status='-';
                            }
                            return $status;
                        }
                    ],
                   // 'id_top',


                ],
            ]) ?>

        </div>
        <?php
            if ($model->status_billing == 0) {
            ?>
        <?php $form = ActiveForm::begin(); ?>


        <div class="col-md-6">

            <?= $form->field($modelbilling2, 'tanggal_billing')->widget(DatePicker::className(), [
                'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-m-d', 'target' => '_Blank']
            ]);
            ?>
        </div>
        <div class="col-md-6">

            <?= $form->field($modelbilling2, 'id_top')->widget(\kartik\select2\Select2::classname(), [
                'data' => ArrayHelper::map(common\models\MasterTop::find()->where(['delete_mark' => 0])->asArray()->all(), 'id', 'code_top', 'deskripsi'),
                'language' => 'eng',
                'options' => ['placeholder' => 'Pilih...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('TOP');
            ?>



        </div>

        <div class="col-md-12">
            <div class="form-group">
            <?= Html::submitButton('Simpan/Kirim Data', ['class' => 'btn-xs btn-primary', 'name' => 'simpan', 'value' => 'true']) ?>

            </div>
        </div>
        <?php ActiveForm::end(); ?>
<?php }?>

        <?php
        if ($model->status_billing == 0) {
        ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="box-header">
                        <h1 class="box-title"><?= 'Search Distributor' ?></h1>
                        <br>


                    </div>
                    <div class="table-matching-form">

                        <?php $form = ActiveForm::begin(); ?>


                        <div class="col-md-10">
                            <?php if ($datasoldto == 0) { ?>
                                <?= $form->field($modelbilling, 'cekbilling')->widget(\kartik\select2\Select2::classname(), [
                                    'data' => ArrayHelper::map(common\models\RfcZZcsdShiptoSki::find()->where(['DELETE_MARK' => 0])->asArray()->all(), 'ID', 'NAMA_SOLDTO', 'KODE_SOLDTO'),
                                    'language' => 'eng',
                                    'options' => ['placeholder' => 'Pilih...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label(false);
                                ?>
                            <?php } else { ?>
                                <?= $form->field($modelbilling, 'cekbilling')->widget(\kartik\select2\Select2::classname(), [
                                    'data' => ArrayHelper::map(common\models\RfcZZcsdShiptoSki::find()->where(['DELETE_MARK' => 0])->andWhere(['ID' => $datasoldto])->asArray()->all(), 'ID', 'NAMA_SOLDTO', 'KODE_SOLDTO'),
                                    'language' => 'eng',
                                    'options' => ['placeholder' => 'Pilih...'],
                                    'pluginOptions' => [
                                        'allowClear' => true
                                    ],
                                ])->label(false);
                                ?>
                            <?php } ?>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                            <?= Html::submitButton('Search', ['class' => 'btn btn-primary', 'name' => 'search', 'value' => 'true']) ?>

                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>

                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="box-header">
                <h1 class="box-title"><?= 'Data SPJ' ?></h1>
                <br>


            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table>
                        <thead style="background-color: #7fc6b6;">
                            <tr>
                                <th>No</th>

                                <th>No SPJ</th>
                                <th>TAX</th>
                                <th>Qty</th>
                                <th>Subtotal</th>
                                <th>Nilai Masuk</th>
                                <th>Nilai Keluar</th>
                                <th>Nilai Akhir</th>
                                <th>Tanggal Masuk</th>
                                <th>Tanggal Keluar</th>
                                <th>Distributor</th>


                                <th>Action</th>


                            </tr>
                        </thead>
                        <?php if ($dataspj != null) {
                            $no = 1;
                            foreach ($dataspj as $key => $val) {
                                $sumnominal = ChildMatching::find()->where(['id_matching' => $val->id_matching])->andWhere(['delete_mark' => 0])->sum('subtotal');
                                $datatax = ChildMatching::find()->where(['id_matching' => $val->id_matching])->andWhere(['delete_mark' => 0])->one();

                                $datadistributor = RfcZZcsdShiptoSki::find()->where(['ID' => $val->id_soldto])->one();
                                // $datamatching=TableMatching::find()->where(['id'=>$val->id_matching])->one();
                                // $cekdatachildmat=ChildMatching::find()->where(['id_matching'=>$datamatching->id])->one();
                                // $cekchilpp=ChildOPpSki::find()->where(['id'=>$cekdatachildmat->id_child_so])->one();
                                $tax = MasterTax::find()->where(['id' => $datatax->id_tax])->one();
                                if ($datadistributor) {
                                    $nama = $datadistributor->NAMA_SOLDTO;
                                } else {
                                    $nama = 'Data Distributor Tidak Ada';
                                }
                        ?>

                                <tr>
                                    <td><?= $no + $key ?></td>

                                    <td><?= $val->no_spj ?></td>
                                    <td><?= $tax->deskripsi . ' ' . $tax->nominal_tax; ?></td>
                                    <td><?= $val->total_nilai ?></td>
                                    <td><?= 'Rp.' . number_format($sumnominal); ?></td>
                                    <td><?= $val->nilai_masuk . ' KG' ?></td>
                                    <td><?= $val->nilai_keluar . ' KG' ?></td>
                                    <td><?= $val->nilai_akhir . ' KG' ?></td>
                                    <td><?= $val->tanggal_masuk ?></td>
                                    <td><?= $val->tanggal_keluar ?></td>
                                    <td><?= $nama ?></td>



                                    <?php
                                    if ($datadistributor) {
                                        if ($model->status_billing == 0) {
                                    ?>
                                            <td>
                                                <?php $cekdatachild = ChildBilling::find()->where(['id_billing' => $model->id])->andWhere(['no_spj' => $val->id])->one(); ?>
                                                <?php if (!$cekdatachild) { ?>
                                                    <?= Html::a('<i class="fa fa-check-square-o"></i>', ['pilih-data', 'id' => $val->id, 'idbill' => $model->id], ['class' => 'btn btn-black',  'title' => 'Pilih', 'data' => [
                                                        'confirm' => 'Pilih Data ?',
                                                        'method' => 'post',
                                                    ],]) ?>
                                                <?php } else { ?>
                                                    <?= Html::a('<i class="fa fa-times-circle-o"></i>', ['cancel-data', 'id' => $val->id, 'idbill' => $model->id], ['class' => 'btn btn-black',  'title' => 'Cancel', 'data' => [
                                                        'confirm' => 'Cancel Data ?',
                                                        'method' => 'post',
                                                    ],]) ?>
                                                <?php } ?>

                                            </td>
                                        <?php } else { ?>
                                            <td>Status Billing Close</td>
                                        <?php } ?>

                                    <?php } ?>


                                </tr>

                        <?php }
                        } ?>


                    </table>
                </div>
            </div>
        </div>


    </div>
</div>