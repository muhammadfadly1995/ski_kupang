<?php

use common\models\ChildSholdto;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cancel Billing';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>

          
            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],

                //  'id',
                'no_billing',
                //  'subtotal',
                [
                    'attribute' => 'status_billing',
                    'label' => 'Status Billing',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        if ($model->status_billing == 0) {
                            $status = 'Open';
                        } else {
                            $status = 'Closed';
                        }
                        return $status;
                    }
                ],
                [
                    'attribute' => 'subtotal',
                    'label' => 'Berat Akhir',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return 'Rp '. number_format($model->subtotal) ;
                    }
                ],
                'create_by',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => ' {myButton}',  // the default buttons + your custom button
                    'buttons' => [
                        'myButton' => function ($url, $model, $key) {     // render your custom button
                            return Html::a('Cancel Transaksi', ['cancel-data-transaksi', 'id' => $model->id],['data-target'=>"#modalvote" ,'data-toggle'=>"modal"]);
                        }
                    ]
                ]
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
    </div>
</div>
<div class="modal remote fade" id="modalvote">
    <div class="modal-dialog">
        <div class="modal-content loader-lg"></div>
    </div>
</div>