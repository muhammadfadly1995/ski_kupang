<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TimbanganIncoming */

$this->title = 'Update Timbangan Incoming: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Timbangan Incomings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="timbangan-incoming-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
