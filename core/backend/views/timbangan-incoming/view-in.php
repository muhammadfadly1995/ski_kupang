<?php

use common\models\DataPo;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use common\models\TableExpeditur;
use common\models\TableSloc;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model common\models\TimbanganIncoming */
/* @var $form yii\widgets\ActiveForm */

$this->title = $model->no_spj;
$this->params['breadcrumbs'][] = ['label' => 'Timbangan Incomings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<div class="box">
    <div class="box-body">
        <div class="timbangan-incoming-view">

            <h3><?= Html::encode($this->title) ?></h3>


            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'id',
                        'no_spj',
                        'no_pol',
                        'nama_supir',
                        [
                            'attribute' => 'id_po',
                            'label' => 'No PO',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                $datatruck = DataPo::find()->where(['id' => $model->id_po])->one();
                                if ($datatruck) {
                                    return $datatruck->no_po;
                                } else {
                                    return 0;
                                }
                            }
                        ],
                        'tanggal_masuk',
                        'tanggal_keluar',
                        


                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'id',
                        'jam_masuk',
                        // [
                        //     'attribute' => 'qty_po',
                        //     // 'label' => 'No PO',
                        //     'contentOptions' => ['class' => 'text-left'],
                        //     'value' => function ($model) {

                        //         return  number_format($model->qty_po) . ' TO';
                        //     }
                        // ],
                        'jam_keluar',

                        [
                            'attribute' => 'nilai_masuk',
                             'label' => 'Timbangan Masuk',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {

                                return  number_format($model->nilai_masuk) . ' KG';
                            }
                        ],
                        // 'nilai_masuk',
                        // [
                        //     'attribute' => 'nilai_keluar',
                        //     // 'label' => 'No PO',
                        //     'contentOptions' => ['class' => 'text-left'],
                        //     'value' => function ($model) {

                        //         return  number_format($model->nilai_keluar) . ' KG';
                        //     }
                        // ],
                        [
                            'attribute' => 'nilai_keluar',
                             'label' => 'Timbangan Keluar',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {

                                return  number_format($model->nilai_keluar) . ' KG';
                            }
                        ],
                       //  'nilai_keluar',
                        [
                            'attribute' => 'subtotal',
                             'label' => 'NET',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {

                                return number_format($model->subtotal) . ' KG';
                            }
                        ],

                    ], 
                ]) ?>
            </div>

        </div>
        <?php $form = ActiveForm::begin(); ?>
        <?php if ($model->nilai_masuk < 1) { ?>
            <?php if ($model->id_po == 0) { ?>
                <?= $form->field($model, 'nilai_masuk')->textInput(['type' => 'float', 'placeholder' => 'Nilai Timbangan', 'style' => 'text-align:center; height: 70px ', 'readOnly' => true])->label(false) ?>
              
                
                <div class="col-md-12">
                    <div class="form-group">
                        <?= Html::submitButton('Simpan Data', ['class' => 'btn btn-default btn-block disabled']) ?>
                    </div>
                </div>
            <?php } else {
            ?>
               
               <div id ="content"> </div>
                <div class="col-md-12">
                    <div class="form-group">
                   
                    <?= Html::a('Next', ['input-number','data'=>$model->id], ['class' => 'btn btn-default btn-block']) ?> 
                       
                    </div>
                </div>
            <?php } ?>
        <?php } else { ?>
            <!-- <?= $form->field($model, 'nilai_keluar')->textInput(['type' => 'float', 'placeholder' => 'Nilai Timbangan', 'style' => 'text-align:center; height: 70px ', 'readOnly' => false])->label(false) ?> -->
            <div class="col-md-12">
                <div class="form-group">
                <?= Html::a('Next', ['input-number','data'=>$model->id], ['class' => 'btn btn-default btn-block']) ?> 
                       
                 </div>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>
        <br>
        <div class="row">
            <?php if ($model->nilai_masuk < 1) { ?>
                <div class="col-md-12">
                    <div class="box-header">
                        <h1 class="box-title"><?= 'Data PO' ?></h1>
                        <br>


                    </div>
                </div>
                <?php if ($model->id_po == 0) { ?>
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="col-md-10">
                        <?= $form->field($modelpo, 'no_po1')->textInput(['placeholder' => 'No PO', 'style' => 'text-transform:uppercase'])->label(false) ?>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <?= Html::submitButton('Search', ['class' => 'btn btn-default']) ?>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                <?php } ?>

                <div class="col-md-12">
                    <div class="table-responsive">
                        <table>
                            <thead style="background-color: #7fc6b6;">
                                <tr>
                                <th>Action</th>
                                    <th>No</th>
                                    <th>Tanggal Kedatangan</th>
                                    <th>Nama Angkutan</th>
                                  

                                    
                                    <th>No PO</th>
                                    <th>Vendor</th>
                                    <th>Material</th>
                                    <th>Sloc</th>
                                    <th>Qty</th>
                                    <th>Sisa Qty</th>
                                   


                                </tr>
                            </thead>
                            <?php if ($datapo) {
                                $no = 1;
                                foreach ($datapo as $key => $val) {
                                    $plant = RfcZZappSelectSysplanSki::find()->where(['ID' => $val->id_plant])->one();
                                    $vendor = TableExpeditur::find()->where(['id' => $val->vendor])->one();
                                    $material = RfcZZcsdListMatSalesSki::find()->where(['ID' => $val->material])->one();
                                    $sloc = TableSloc::find()->where(['id' => $val->sloc])->one();
                            ?>

                                    <tr>
                                    <?php

if ($val->status == 'Open' || $val->status== 'Timbangan Masuk') {
?>
    <td>

        <?php if ($model->id_po == 0) { ?>
            <?= Html::a('<i class="fa fa-check-square-o"></i>', ['pilih-data', 'id' => $val->id, 'idbill' => $model->id], ['class' => 'btn btn-black',  'title' => 'Pilih']) ?>
        <?php } else { ?>
            <?= Html::a('<i class="fa fa-times-circle-o"></i>', ['cancel-data', 'id' => $val->id, 'idbill' => $model->id], ['class' => 'btn btn-black',  'title' => 'Cancel', 'data' => [
                'confirm' => 'Cancel Data ?',
                'method' => 'post',
            ],]) ?>
        <?php } ?>

    </td>
<?php } else { ?>
    <td>Status Billing Close</td>
<?php } ?>
                                        <td><?= $no + $key ?></td>

                                        <td><?= $val->tanggal_datang ?></td>
                                        <td><?= $val->nama_kapal ?></td>

                                        <!-- <td><?= $plant->WERKS . ' ' . $plant->NAME1 ?></td> -->


                                        <td><?= $val->no_po ?></td>
                                        <td><?= $vendor->code_expeditur . ' ' . $vendor->deskripsi ?></td>
                                        <td><?= $material->KODE_MATERIAL . ' ' . $material->DESKRIPSI ?></td>
                                        <td><?= $sloc->keterangan ?></td>
                                        <td><?= $val->qty .' TO'?></td>
                                        <td><?= $val->sisa_qty .' TO'?></td>


                                       




                                    </tr>

                            <?php }
                            } ?>


                        </table>
                    </div>
                </div>
            <?php } ?>
        </div>

        <br>


    </div>
</div>
<div class="modal remote fade" id="modalvote">
    <div class="modal-dialog">
        <div class="modal-content loader-lg"></div>
    </div>
</div>
<script>
      function load_home(){
            document.getElementById("content").innerHTML='<object type="type/html" data="home.html" ></object>';
  }
</script>
