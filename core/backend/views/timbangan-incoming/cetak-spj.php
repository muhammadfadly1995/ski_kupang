<!DOCTYPE html>
<html>

<head>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <link href="<?= Yii::getAlias('@web/css/bootstrap.min.css') ?>" rel="stylesheet" media="print">

    <!-- Google Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        body {
            font-family: "Times New Roman", Times, serif;
            margin-left: 0cm;
            margin-right: 0cm;
            margin-top: 0cm;
            margin-bottom: 0cm;

        }

        .mar-papper {
            /* margin: 1cm 3cm 2cm; */
        }

        h4 {
            font-family: "Times New Roman", Times, serif;
            font-size: 16pt;
            margin: 0px auto;
            font-weight: normal;

        }

        h2 {
            font-family: "Times New Roman", Times, serif;
            font-size: 14pt;
            font-weight: bold;
            margin: 0px auto;
        }

        p {
            font-family: "Times New Roman", Times, serif;
            font-size: 18px;
            margin: 0px auto;
            font-weight: normal;

        }

        th {
            font-family: "Times New Roman", Times, serif;
            font-size: 18px !important;
            font-weight: normal;
            line-height: 14px;
        }

        td {
            font-family: "Times New Roman", Times, serif;
            font-size: 17px !important;
            font-weight: normal;
            line-height: 14px;
        }

        table {
            font-family: "Times New Roman", Times, serif;
        }

        .table-padd .table tbody tr td {
            border: none;
            padding: 1px !important;
        }

        hr {
            border: 0;
            height: 2px;
            border-top: 2px solid rgb(0 0 0 / 80%);
            border-bottom: 1px solid rgba(255, 255, 255, 0.3);
            margin: 0px 0px 0px 0px;
        }

        .tembusan tr th {
            font-size: 10px !important;
            line-height: 10px;
        }

        @media print {
            html,body{
                height: 100%;
                overflow: hidden;
                page-break-after: always;
            }

            /* page-break-after works, as well */
        }

        .brand-box {
            background-color: #c2c2c2 !important;
        }

        @media print {
            .brand-box {
                box-shadow: inset 0 0 0 100em rgba(2, 132, 130, 0.2) !important;
            }

            body {
                width: auto;
                height: auto;
            }
        }

        .wrapper-p p {
            font-size: 18px;
        }

        @media print {
            @page {
                size: 21.59cm 16.02cm portrait;
                /* margin: 30px 80px 10px 120px; */
            }

        }
    </style>
</head>

<body onload="window.print()">

    <div class="wrapper wrapper-p">
        <div class="pagebreak">
            <section class="invoice">
                <div class="row">
                    <div class="col-xs-12">

                        <table style="width:100%;">
                            <tr>
                                <td><img src="<?= Yii::getAlias('@web/background/ski.jpg') ?>" class="img-responsive" width="120px" alt="" style="padding-left: 30px;"></td>

                                <td>
                                        <!-- <h4 style="text-transform: uppercase; text-align: center;">
                                        <?= 'PT Semen Indonesia (Persero) Tbk' ?></h4> -->
                                    <!-- <br> -->
                                    <h2 class="text-center" style="text-transform: uppercase;text-align: center;">
                                        <?= 'Surat Perintah Jalan IN' ?></h2>
                                    <hr>
                                    <p style="text-align: center;">Nomor : <?= $datatimbangan->no_spj ?></p>

                                </td>
                                <td style="text-align:right;"><img src="<?= Yii::getAlias('@web/background/Semen_Kupang.webp') ?>" class="img-responsive" width="75px" alt="" style="padding-right: 30px;"></td>
                                
                                <td>

                            </tr>
                        </table>
                    </div>
                </div>
                <br>
                <div class="row invoice-info">
                    <div class="col-sm-12 table-padd">

                        <table class="table">
                            <tbody style="text-transform: capitalize">
                                <tr>
                                    <td style="width: 22%">Truck/Gerbong/Supir </td>
                                    <td>:</td>
                                    <td style="font-weight: bold"> <?php

                                                                    // $datatruck = \common\models\DataTruck::find()->where(['id' => $model2->id_truck])->one();

                                                                  //  use common\models\RfcZZappSelectSysplanSki;

                                                                    echo $datatimbangan->no_pol.'/'.$datatimbangan->nama_supir; ?>
                                    </td>
                                    <td></td>
                                    <td style="width: 22%">Tanggal Timbang Keluar </td>
                                    <td>:</td>
                                    <td style="font-weight: bold"><?= $datatimbangan->tanggal_keluar ?>
                                    </td>
                                </tr>

                                <tr>
                                    <td style="width: 22%">Distributor </td>
                                    <td>:</td>
                                    <td style="font-weight: bold">Semen Kupang IND
                                    </td>
                                    <td></td>
                                    <td style="width: 22%">Jam </td>
                                    <td>:</td>
                                    <td style="font-weight: bold">
                                        <?php

                                        echo $datatimbangan->jam_keluar;
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 22%">Nilai Timbangan Masuk </td>
                                    <td>:</td>
                                    <td style="font-weight: bold"><?= number_format($datatimbangan->nilai_masuk) . ' KG' ?>
                                    </td>
                                    <td></td>
                                    <td style="width: 22%">Nilai Timbangan Keluar </td>
                                    <td>:</td>
                                    <td style="font-weight: bold">
                                        <?=  number_format($datatimbangan->nilai_keluar) . ' KG' ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 22%">NET </td>
                                    <td>:</td>
                                    <td style="font-weight: bold"><?=  number_format($datatimbangan->subtotal) . ' KG' ?>
                                    </td>
                                    <td></td>
                                   
                                </tr>


                            </tbody>
                        </table>



                    </div>

                </div>
                <div class="row" style="margin-top: 10px;">
                    <div class="col-xs-12">
                        <table width="100%" border="1">
                            <thead>
                                <tr style="text-transform: capitalize">
                                    <th style="padding: 5px; text-align: center; font-weight: bold">No</th>
                                    <th style="padding: 5px; text-align: center; font-weight: bold">Company</th>
                                    <th style="padding: 5px; text-align: center; font-weight: bold">Plant</th>
                                    <th style="padding: 5px; text-align: center; font-weight: bold">No PO</th>
                                    <th style="padding: 5px; text-align: center;font-weight: bold">Vendor</th>

                                    <th style="padding: 5px; text-align: center; font-weight: bold">Material</th>
                                    <th style="padding: 5px; text-align: center; font-weight: bold">Sloc</th>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;



                                if ($datapo) {
                                    $dataplant = \common\models\RfcZZappSelectSysplanSki::find()->where(['ID' => $datapo->id_plant])->one();
                                    $expeditur = \common\models\TableExpeditur::find()->where(['id' => $datapo->vendor])->one();
                                    $material = \common\models\RfcZZcsdListMatSalesSki::find()->where(['ID' => $datapo->material])->one();
                                    $sloc = \common\models\TableSloc::find()->where(['id' => $datapo->sloc])->one();
                                ?>

                                    <tr>
                                        <td style="padding: 5px; text-align: center;"><?= $no  ?></td>

                                        <td style="padding: 5px; text-align: center;"><?= $datapo->id_com ?></td>
                                        <td style="padding: 5px; text-align: center;"><?= $dataplant->WERKS . ' ' . $dataplant->NAME1; ?></td>
                                        <td style="padding: 5px; text-align: center;"><?= $datapo->no_po ?></td>
                                        <td style="padding: 5px; text-align: center;"><?= $expeditur->code_expeditur . ' ' . $expeditur->deskripsi ?></td>
                                        <td style="padding: 5px; text-align: center;"><?= $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI ?></td>

                                        <td style="padding: 5px;text-align: center;"><?= $sloc->keterangan ?></td>
                                      


                                    </tr>
                                <?php }  ?>
                                <tr>
                                    <td></td>
                                    <td style="padding: 5px; text-align: left;">T Tangan Penyerahan</td>

                                   
                                    <td style="padding: 5px; text-align: left;">____________</td>
                                    <td style="padding: 5px; text-align: left;"></td>
                                    <td style="padding: 5px; text-align: left;">T T Supir</td>
                                    <td style="padding: 5px;text-align: left;">____________</td>
                                    <td></td>
                                  


                                </tr>
                              
                            </tbody>
                        </table>
                       <!-- <td> Note: Kapasitas Muatan Diluar Tanggung Jawab PT. Semen Indonesia (Persero) Tbk.</td> -->

                    </div>
                </div>
            </section>
        </div>


    </div>



    <script src="<?= Yii::getAlias('@web/js/jquery-3.1.1.js') ?>"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            window.print();

            //  window.close();
        });
    </script>
</body>

</html>