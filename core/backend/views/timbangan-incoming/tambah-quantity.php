<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;
use kartik\money\MaskMoney;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
?>
<?php
$form = ActiveForm::begin();
?>

<div class="modal-header" style="background-color:#7fc6b6 ">
    <button type="button" class="close" data-dismiss="modal">&times;</button>


</div>
<div class="modal-body">
 
        <?= $form->field($model, 'sisa_qty')->textInput(['type'=>'number'])->label('Input QTY') ?>

   





</div>
<div class="modal-footer" style="background-color:#7fc6b6 ">
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
<?php ActiveForm::end(); ?>