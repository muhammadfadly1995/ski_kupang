<?php

use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\TimbanganIncoming */

$this->title = 'Timbangan Incoming';
$this->params['breadcrumbs'][] = ['label' => 'Timbangan Incomings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timbangan-incoming-create">
    <ul class="nav nav-tabs pull-right">
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/timbangan-incoming/create']) ?>"> Create Timbangan</a>
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/timbangan-incoming/in']) ?>"> Timbangan Masuk</a>
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/timbangan-incoming/']) ?>"> Timbangan Close</a>


    </ul>
    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>