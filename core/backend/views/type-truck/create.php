<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TypeTruck */

$this->title = 'Create Type Truck';
$this->params['breadcrumbs'][] = ['label' => 'Type Trucks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-truck-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
