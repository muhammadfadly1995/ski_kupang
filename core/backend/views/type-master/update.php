<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TypeMaster */

$this->title = 'Update Type Master: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Type Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="type-master-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
