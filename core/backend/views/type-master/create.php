<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TypeMaster */

$this->title = 'Create Type Master';
$this->params['breadcrumbs'][] = ['label' => 'Type Masters', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="type-master-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
