<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZcsdListMatSalesSki */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzcsd-list-mat-sales-ski-form">
 
            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'KODE_MATERIAL')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'DESKRIPSI')->textarea(['rows' => 6]) ?>
                <?= $form->field($model, 'TYPE_MATERIAL')->widget(\kartik\select2\Select2::classname(), [
                        'data' => ['ZAK' => 'ZAK', 'CURAH' => 'CURAH','KANTONG'=>'KANTONG'],
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>
                <?= $form->field($model, 'UOM')->widget(\kartik\select2\Select2::classname(), [
                        'data' => ['TO' => 'TO', 'ZAK' => 'ZAK','BAG'=>'BAG'],
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>




            </div>
            <div class="col-md-6">
               
                <?= $form->field($model, 'BERAT_KONFERSI')->textInput() ?>
                <?= $form->field($model, 'UOM_KONFERSI')->widget(\kartik\select2\Select2::classname(), [
                        'data' => [ 'ZAK' => 'ZAK','KG'=>'KG'],
                        'language' => 'de',
                        'options' => ['placeholder' => 'Pilih ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);
                    ?>

                <?= $form->field($model, 'CREATE_BY')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <?= $form->field($model, 'CREATE_DATE')->textInput(['readOnly' => true]) ?>

                <?= $form->field($model, 'LAST_UPDATE_BY')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>