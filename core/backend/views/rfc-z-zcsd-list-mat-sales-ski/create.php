<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZcsdListMatSalesSki */

$this->title = 'Create Material';
$this->params['breadcrumbs'][] = ['label' => 'Material', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rfc-zzcsd-list-mat-sales-ski-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
