<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZcsdListMatSalesSki */

$this->title = 'Update Data Material: ' . $model->DESKRIPSI;
$this->params['breadcrumbs'][] = ['label' => 'Material', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rfc-zzcsd-list-mat-sales-ski-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
