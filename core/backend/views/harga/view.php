<?php
 
use common\models\ChildSholdto;
use common\models\MasterTax;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdListMatSalesSkiSearch;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Harga */

$this->title = 'Detail Harga';
$this->params['breadcrumbs'][] = ['label' => 'Hargas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="harga-view">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'ID',
                        [
                            'attribute' => 'PLANT',
                            'value' => function ($model) {
                                $datakategori = RfcZZappSelectSysplanSki::find()->where(['ID' => $model->PLANT])->one();
                                $status = $datakategori->WERKS.' / '.$datakategori->NAME1;
                                return $status;
                            }
                        ],
                        [
                            'attribute' => 'SOLD_TO',
                            'value' => function ($model) {
                                $datakategori = RfcZZcsdShiptoSki::find()->where(['ID' => $model->SOLD_TO])->one();
                                $status = $datakategori->KODE_SOLDTO.' / '.$datakategori->NAMA_SOLDTO;
                                return $status;
                            }
                        ], 
                        [
                            'attribute' => 'SHIP_TO',
                            'value' => function ($model) {
                                $datakategori = ChildSholdto::find()->where(['ID' => $model->SHIP_TO])->one();
                                $status = $datakategori->KODE_SHIPTO.' / '.$datakategori->NAMA_SHIPTO;
                                return $status;
                            }
                        ],
                       // 'SHIP_TO',
                       [
                        'attribute' => 'DISTRIK',
                        'value' => function ($model) {
                            $datakategori = PtMasterDistrikSki::find()->where(['ID' => $model->DISTRIK])->one();
                            $status = $datakategori->KODE_DISTRIK.' / '.$datakategori->DISTRIK;
                            return $status;
                        }
                    ],
                      //  'DISTRIK',
                      [
                        'attribute' => 'KODE',
                        'value' => function ($model) {
                            $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->KODE])->one();
                            $status = $datakategori->KODE_MATERIAL.' / '.$datakategori->DESKRIPSI;
                            return $status;
                        }
                    ],
                       
                       
                        'DESKRIPSI:ntext',
                        [
                            'attribute' => 'HARGA',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                return 'Rp.' . number_format($model->HARGA);
                            }
                        ],
                        [
                            'attribute' => 'TAX_PPN',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                $datappn=MasterTax::find()->where(['id'=>$model->TAX_PPN])->one();
                                return $datappn->nominal_tax .'%';
                            }
                        ],
                       

                        //  'DELETE_MARK',
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'ID',
                        [
                            'attribute' => 'TAX2',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                return $model->TAX2 .'%';
                            }
                        ],
                        [
                            'attribute' => 'TOTAL',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                return 'Rp.' . number_format($model->TOTAL);
                            }
                        ],
                        'VALID_FROM',
                        'VALID_TO',
                        'CREATE_BY',
                        'CREATE_DATE',
                        'LAST_UPDATE_BY',
                        'LAST_UPDATE_DATE',
                        //  'DELETE_MARK',
                    ],
                ]) ?>
            </div>


        </div>
        <div class="col-md-12">
            <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
        </div>
    </div>
</div>