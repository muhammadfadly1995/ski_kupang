<?php

use common\models\ChildSholdto;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Table Harga';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                //'ID',
                [
                    'attribute' => 'PLANT',
                    'value' => function ($model) {
                        $datakategori = RfcZZappSelectSysplanSki::find()->where(['ID' => $model->PLANT])->one();
                        if($datakategori){
$status = $datakategori->WERKS . ' / ' . $datakategori->NAME1;
                        }else{
                            $status='Data  Plant Tidak Ditemukan';
                        }
                        
                        return $status;
                    }
                ],
                [
                    'attribute' => 'SOLD_TO',
                    'value' => function ($model) {
                        $datakategori = RfcZZcsdShiptoSki::find()->where(['ID' => $model->SOLD_TO])->one();
                        if($datakategori){
                            $status = $datakategori->KODE_SOLDTO . ' / ' . $datakategori->NAMA_SOLDTO;
                        }else{
                            $status='Data SoldTo Tidak Ditemukan';
                        }
                        
                        return $status;
                    }
                ],
                [
                    'attribute' => 'SHIP_TO',
                    'value' => function ($model) {
                        $datakategori = ChildSholdto::find()->where(['ID' => $model->SHIP_TO])->one();
                        if($datakategori){
                            $status = $datakategori->KODE_SHIPTO . ' / ' . $datakategori->NAMA_SHIPTO;
                        }else{
                            $status='Data SoldTo Tidak Ditemukan';
                        }
                        
                        return $status;
                    }
                ],
                // 'SHIP_TO',
                [
                    'attribute' => 'DISTRIK',
                    'value' => function ($model) {
                        $datakategori = PtMasterDistrikSki::find()->where(['ID' => $model->DISTRIK])->one();
                        if($datakategori){
                            $status = $datakategori->KODE_DISTRIK . ' / ' . $datakategori->DISTRIK;
                        }else{
                            $status='Data SoldTo Tidak Ditemukan';
                        }
                        
                        return $status;
                    }
                ],
                [
                    'attribute' => 'KODE',
                    'value' => function ($model) {
                        $datakategori = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->KODE])->one();
                        if($datakategori){
                             $status = $datakategori->KODE_MATERIAL . ' / ' . $datakategori->DESKRIPSI;
                        }else{
                            $status='Data SoldTo Tidak Ditemukan';
                        }
                       
                        return $status;
                    }
                ],
                'DESKRIPSI:ntext',
                [
                    'attribute' => 'HARGA',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        return 'Rp.' . number_format($model->HARGA);
                    }
                ],
                [
                    'attribute' => 'TAX_PPN',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        return $model->TAX_PPN .'%';
                    }
                ],
                [
                    'attribute' => 'TAX_PPN',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datappn=MasterTax::find()->where(['id'=>$model->TAX_PPN])->one();
                        return $datappn->nominal_tax .'%';
                    }
                ],
                [
                    'attribute' => 'TOTAL',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        return 'Rp.' . number_format($model->TOTAL);
                    }
                ],
                //   'HARGA',
                'VALID_FROM',
                'VALID_TO',
                'CREATE_BY',
                'CREATE_DATE',
                'LAST_UPDATE_BY',
                'LAST_UPDATE_DATE',
                //'DELETE_MARK',

                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
    </div>
</div>