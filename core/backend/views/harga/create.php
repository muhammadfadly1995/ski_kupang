<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Harga */

$this->title = 'Create Harga';
$this->params['breadcrumbs'][] = ['label' => 'Harga', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="harga-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
