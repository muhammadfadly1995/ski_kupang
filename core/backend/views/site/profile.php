<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */

$this->title = 'Profile';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box-body">
    <div class="row">
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    <img class="profile-user-img img-responsive" src="<?= Url::toRoute(['site/profile-photo', 'inline' => true]) ?>" alt="User profile picture" style="background-color: #333333;">

                    <h3 class="profile-username text-center"></h3>

                    <p class="text-muted text-center"></p>

                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item text-center">
                            <b><?= $model->name ?></b>
                        </li>
                        <li class="list-group-item text-center">
                            <b><?= $model->email ?></b>
                        </li>
                    </ul>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box-body -->
        </div>
        <div class="col-md-9">
            <div class="site-index">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#set" data-toggle="tab">Profile</a></li>
                        <li><?=
                                Html::a('Password', Url::toRoute(['site/change-password']), [
                                    'class' => 'btn btn-transparent'
                                ])
                            ?></li>
                    </ul>
                    <div class="tab-content">

                        <!--/.tab-pane -->
                        <div class="active tab-pane" id="set">
                            <div class="kasi-form">

                                <div class="box-body">
                                    <div class="col-lg-4 admin-menu-left-line">
                                        <?php
                                        $form = ActiveForm::begin([
                                            'fieldConfig' => [
                                                //                                'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
                                                'options' => [
                                                    'label' => 'col-sm-4',
                                                    'offset' => 'col-sm-offset-4',
                                                    'wrapper' => 'col-sm-8',
                                                    'error' => '',
                                                    'hint' => '',
                                                ],
                                            ],
                                        ]);
                                        ?>

                                        <div class="form-group">
                                            <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'class' => 'form-control']) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'username')->textInput(['class' => 'form-control']) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'email')->textInput(['class' => 'form-control']) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($model, 'input_photo')->fileInput(['accept' => 'image/*']) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                                        </div>



                                        <?php ActiveForm::end(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/.tab-content -->
                </div>
                <!--/.nav-tabs-custom -->
            </div>
        </div>
    </div>
</div>