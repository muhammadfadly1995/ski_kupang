<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ChildOPpSkiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="child-opp-ski-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'id_soldto') ?>

    <?= $form->field($model, 'id_shipto') ?>

    <?= $form->field($model, 'id_plant') ?>

    <?= $form->field($model, 'id_material') ?>

    <?php // echo $form->field($model, 'id_angkut') ?>

    <?php // echo $form->field($model, 'quantity') ?>

    <?php // echo $form->field($model, 'date_order') ?>

    <?php // echo $form->field($model, 'date_receipt') ?>

    <?php // echo $form->field($model, 'id_top') ?>

    <?php // echo $form->field($model, 'harga') ?>

    <?php // echo $form->field($model, 'id_tax') ?>

    <?php // echo $form->field($model, 'subtotal') ?>

    <?php // echo $form->field($model, 'create_by') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'approval_by') ?>

    <?php // echo $form->field($model, 'approval_date') ?>

    <?php // echo $form->field($model, 'approval_reject') ?>

    <?php // echo $form->field($model, 'approval_date_reject') ?>

    <?php // echo $form->field($model, 'delete_mark') ?>

    <?php // echo $form->field($model, 'status_konfirmasi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
