<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ChildOPpSki */

$this->title = $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'Child O Pp Skis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="child-opp-ski-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'ID',
            'id_soldto',
            'id_shipto',
            'id_plant',
            'id_material',
            'id_angkut',
            'quantity',
            'date_order',
            'date_receipt',
            'id_top',
            'harga',
            'id_tax',
            'subtotal',
            'create_by',
            'create_date',
            'approval_by',
            'approval_date',
            'approval_reject',
            'approval_date_reject',
            'delete_mark',
            'status_konfirmasi',
        ],
    ]) ?>

</div>
