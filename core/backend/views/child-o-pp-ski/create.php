<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ChildOPpSki */

$this->title = 'Create Child O Pp Ski';
$this->params['breadcrumbs'][] = ['label' => 'Child O Pp Skis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="child-opp-ski-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
