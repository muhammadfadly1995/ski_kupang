<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StagingStock */

$this->title = 'Create Staging Stock';
$this->params['breadcrumbs'][] = ['label' => 'Staging Stocks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="staging-stock-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
