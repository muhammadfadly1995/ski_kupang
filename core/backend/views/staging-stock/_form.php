<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\StagingStock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="staging-stock-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_plant')->textInput() ?>

    <?= $form->field($model, 'id_mat')->textInput() ?>

    <?= $form->field($model, 'qty')->textInput() ?>

    <?= $form->field($model, 'last_update')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
