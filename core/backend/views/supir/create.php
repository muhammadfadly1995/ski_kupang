<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Supir */

$this->title = 'Create Supir';
$this->params['breadcrumbs'][] = ['label' => 'Supirs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supir-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
