<?php

namespace backend\controllers;

use common\models\ChildSholdto;
use common\models\PtMasterDistrikSki;
use Yii;
use common\models\RfcZZcsdShiptoSki;
use common\models\RfcZZcsdShiptoSkiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RfcZZcsdShiptoSkiController implements the CRUD actions for RfcZZcsdShiptoSki model.
 */
class RfcZZcsdShiptoSkiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RfcZZcsdShiptoSki models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RfcZZcsdShiptoSkiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RfcZZcsdShiptoSki model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

     public function actionDeleteSoldto($id,$ids){
        $data=ChildSholdto::find()->where(['ID'=>$id])->one();
        if($data){
            $data->delete();
            \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Didelete');
            return $this->redirect(['view', 'id' => $ids]);
        }else{
            \Yii::$app->getSession()->setFlash('error', 'Data Tidak Ditemukan');
            return $this->redirect(['view', 'id' => $ids]);
        }
     }

    public function actionView($id)
    {
        $datasholdto=new ChildSholdto();
        $dataSelect=ChildSholdto::find()->where(['ID_SHOLDTO'=>$id])->all();
        if ($datasholdto->load(Yii::$app->request->post())) {
            $datasholdto->ID_SHOLDTO=$id;
            $datasholdto->CREATE_BY=\yii::$app->user->identity->username;
            $datasholdto->UPDATE_BY=\yii::$app->user->identity->username;
            $datasholdto->UPDATE_DATE=date('Y-m-d');
            $datasholdto->save(false);
            \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Disimpan');
            return $this->redirect(['view', 'id' => $id]);
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
            'datasholdto'=>$datasholdto,
            'dataSelect'=>$dataSelect,
        ]);
    }

    /**
     * Creates a new RfcZZcsdShiptoSki model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 
        $model = new RfcZZcsdShiptoSki();
        $model->CREATE_BY=\yii::$app->user->identity->username;
        $model->CREATE_DATE=date('Y-m-d');
        $model->LAST_UPDATE_BY=\yii::$app->user->identity->username;
        $model->LAST_UPDATE_DATE=date('Y-m-d');
        $model->DELETE_MARK=0;
        if ($model->load(Yii::$app->request->post()) ) {
            //$username=$model->NAMA_PEMILIK.rand(1,100);
            
            $user = new \common\models\SignupForm();
            $user->name = $model->NAMA_PEMILIK;
            $user->username = $model->NAMA_PEMILIK.rand(1,10000).date('md');
            $user->password = '123456789';
            $user->email = $user->username.'@gmail.com';
           
            $user->role = 'Distributor';
            $user = $user->signup();

            $datadistrik=PtMasterDistrikSki::find()->where(['ID'=>$model->KODE_DSITRIK])->one();
            $model->NAMA_DISTRIK=$datadistrik->DISTRIK;
            $model->id_user=$user->id;
            $model->save(false);
            \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Disimpan');
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RfcZZcsdShiptoSki model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RfcZZcsdShiptoSki model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RfcZZcsdShiptoSki model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RfcZZcsdShiptoSki the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RfcZZcsdShiptoSki::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
