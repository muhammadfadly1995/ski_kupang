<?php

namespace backend\controllers;

use Yii;
use common\models\OPpSki;
use common\models\OPpSkiSearch;
use common\models\ChildOPpSki;
use common\models\ChildSholdto;
use common\models\Harga;
use common\models\MasterTax;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OPpSkiController implements the CRUD actions for OPpSki model.
 */
class OPpSkiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OPpSki models.
     * @return mixed
     */
    public function actionApproval($id, $ids, $status = null)
    {
        $datachild = ChildOPpSki::find()->where(['ID' => $id])->one();
        $datachild->approval_by = \yii::$app->user->identity->username;
        $datachild->approval_date = date('Y-m-d');
        $datachild->status_konfirmasi = 'Approval';
        $datachild->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Data Berhasil DiApproval');
        return $this->redirect(['view', 'id' => $ids, 'status' => $status]);
    }
    public function actionReject($id, $ids, $status = null)
    {
        $datachild = ChildOPpSki::find()->where(['ID' => $id])->one();
        $datachild->approval_reject = \yii::$app->user->identity->username;
        $datachild->approval_date_reject = date('Y-m-d');
        $datachild->status_konfirmasi = 'Reject';
        $datachild->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Direject');
        return $this->redirect(['view', 'id' => $ids, 'status' => $status]);
    }
    public function actionDeleteChild($id, $ids)
    {
        $datachild = ChildOPpSki::find()->where(['ID' => $id])->one();

        $datachild->delete_mark = 1;
        $datachild->status_konfirmasi = 'Delete';
        $datachild->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Didelete');
        return $this->redirect(['view', 'id' => $ids]);
    }

    public function actionCancelDataTransaksi($id){
        $model=OPpSki::find()->where(['ID'=>$id])->one();
        if ($model->load(Yii::$app->request->post())) {
            $model->delete_mark=1;
            $model->update_by=\yii::$app->user->identity->username;
            $model->update_date=date('Y-m-d');
            $model->time_update=date('H:i:s');
            $model->save(false);
            \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Dicancel');
            return $this->redirect(['cancel-transaksi']);
        }
        return $this->renderAjax('cancel-data', [
           'model'=>$model,
        ]);
    }
    public function actionIndex()
    {
        $searchModel = new OPpSkiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCancelTransaksi()
    {
        $searchModel = new OPpSkiSearch();
        $dataProvider = $searchModel->searchcancel(Yii::$app->request->queryParams);

        return $this->render('index-cancel', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionDataPp()
    {
        $searchModel = new OPpSkiSearch();
        $dataProvider = $searchModel->searchpp(Yii::$app->request->queryParams);

        return $this->render('index-pp', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OPpSki model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $status = null)
    {
        $datachild = ChildOPpSki::find()->where(['id_pp' => $id])->andWhere(['delete_mark' => 0])->all();
        return $this->render('view', [
            'status' => $status,
            'datachild' => $datachild,
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OPpSki model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionListregss($id = null)
    {
       
        if ($id) {
            $hitungdata = \common\models\Harga::find()
                ->where(['SOLD_TO' => $id])


                ->count();
               
            $regs = \common\models\Harga::find()
                ->where(['SOLD_TO' => $id])

                
                ->all();

               
            if ($hitungdata > 0) {
                // $idsatuan=[];
                $idchild=[];
                echo
                "<option value=" . 'Pilih . . .' . "'>" . 'Pilih . . .' . "</option>  <br/>";
                foreach ($regs as $reg) {
                    $idchild[]=$reg->SHIP_TO;
                }
                $datachild = ChildSholdto::find()->where(['ID' => $idchild])->all();
              //  print_r($datachild);die();
                    if ($datachild) {
                        foreach($datachild as $keyc =>$valchil){
                            echo


                            "<option value='" . $valchil->ID . "'>" . $valchil->NAMA_SHIPTO . "</option>";
                  
                        }
                            } else {
                        
                        echo  "<option value=" . '' . "'>" . 'Shipto Belum Terdaftar Ditable Harga' . "</option>  <br/>";
                    }
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionListregplant($id = null)
    {
        if ($id) {
             $tanggalsekarang=date('Y-m-d');
             $tanggalkecil=date('Y-m-d', strtotime('-1 days', strtotime($tanggalsekarang)));
             $tanggalbesar=date('Y-m-d', strtotime('+1 days', strtotime($tanggalsekarang)));
            $hitungdata = \common\models\Harga::find()
                ->where(['SHIP_TO' => $id])
                ->andWhere(['<','VALID_FROM',$tanggalkecil])->andWhere(['>','VALID_TO',$tanggalbesar])

                ->count();

            $regs = \common\models\Harga::find()
                ->where(['SHIP_TO' => $id])
                ->andWhere(['<','VALID_FROM',$tanggalkecil])->andWhere(['>','VALID_TO',$tanggalbesar])
                ->orderBy([
                    'ID' => SORT_ASC,
                ])
                ->all();


            if ($hitungdata > 0) {
                // $idsatuan=[];

                echo
                "<option value=" . 'Pilih . . .' . "'>" . 'Pilih . . .' . "</option>  <br/>"; 
                foreach ($regs as $reg) {
                   
                    $datachild = RfcZZcsdListMatSalesSki::find()->where(['ID' => $reg->KODE])->one();
                    if ($datachild) {
                        echo


                        "<option value='" . $datachild->ID . "'>" . $datachild->KODE_MATERIAL . '-' . $datachild->DESKRIPSI . "</option>";
                    } else {
                        echo  "<option value=" . '' . "'>" . 'Material Belum Terdaftar Ditable Harga' . "</option>  <br/>";
                    }
                }
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionGetharga($id = null)
    {
        if ($id) {
            $hitungdata = \common\models\Harga::find()
                ->where(['KODE' => $id])


                ->count();

            $regs = \common\models\Harga::find()
                ->where(['KODE' => $id])

                ->orderBy([
                    'ID' => SORT_ASC,
                ])
                ->all();


            if ($hitungdata > 0) {
                // $idsatuan=[];


                foreach ($regs as $reg) {
                    // $datachild = RfcZZcsdListMatSalesSki::find()->where(['ID' => $reg->KODE])->one();

                    echo


                    "<option value='" . $reg->HARGA . "'>" . 'Rp.' . $reg->HARGA . "</option>";
                }
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionListregsoldto($id = null)
    {
        if ($id) {
            $hitungdata = \common\models\Harga::find()
                ->where(['PLANT' => $id])


                ->count();

            $regs = \common\models\Harga::find()
                ->where(['PLANT' => $id])

                ->orderBy([
                    'ID' => SORT_ASC,
                ])
                ->all();


            if ($hitungdata > 0) {
                // $idsatuan=[];
                $idplant=[];
                echo
                "<option value=" . 'Pilih . . .' . "'>" . 'Pilih . . .' . "</option>  <br/>";
                foreach ($regs as $reg) {
                   $idplant[]=$reg->SOLD_TO;
                }
                $datachild = RfcZZcsdShiptoSki::find()->where(['ID' => $idplant])->all();
                if ($datachild) {
                    foreach ($datachild as $keys =>$vals){
                        echo


                        "<option value='" . $vals->ID . "'>" . $vals->KODE_SOLDTO . '-' . $vals->NAMA_SOLDTO . "</option>";
               
                    }
                        } else {
                    echo  "<option value=" . '' . "'>" . 'Plant Belum Terdaftar Ditable Harga' . "</option>  <br/>";
                }
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionListregssmat($id = null)
    {
        if ($id) {
            $hitungdata = \common\models\RfcZZcsdListMatSalesSki::find()
                ->where(['ID' => $id])


                ->count();

            $regs = \common\models\RfcZZcsdListMatSalesSki::find()
                ->where(['ID' => $id])

                ->orderBy([
                    'ID' => SORT_ASC,
                ])
                ->all();


            if ($hitungdata > 0) {
                // $idsatuan=[];


                foreach ($regs as $reg) {
                    echo


                    "<option value='" . $reg->UOM . "'>" . $reg->UOM . "</option>";
                }
            } 
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionTambahChild($id, $idchild = null)
    {

        if ($idchild != null) {
            $model = ChildOPpSki::find()->where(['ID' => $idchild])->andWhere(['delete_mark' => 0])->orderBy('ID DESC')->one();
        } else {
            $model = new ChildOPpSki();
        }
        $cekdatachild1 = ChildOPpSki::find()->where(['id_pp' => $id])->one();
        if ($cekdatachild1) {
            $childcek = 0;
        } else {
            $childcek = 1;
            $cekdatachild1 = null;
        }
        $model->create_by = \yii::$app->user->identity->username;
        $model->create_date = date('Y-m-d');
        $model->time_create = date("h:i:sa");
        $model->delete_mark = 0;
        $model->id_pp = $id;
        $model->date_order = date('Y-m-d');
        $model->time_order = date("h:i:sa");
        if ($model->load(Yii::$app->request->post())) {
            //  print_r($model);die();
            if($cekdatachild1!=null){
            if (!$model->id_shipto || !$model->id_material || !$model->id_angkut || !$model->quantity || !$model->date_receipt || !$model->id_top || !$model->id_tax) {
                \Yii::$app->getSession()->setFlash('error', 'Maaf Silahkan Lengkapi Data Isian Anda');
                return $this->redirect(['create']);
            }
            }else{
                if (!$model->id_plant || !$model->id_soldto || !$model->id_shipto || !$model->id_material || !$model->id_angkut || !$model->quantity || !$model->date_receipt || !$model->id_top || !$model->id_tax) {
                    \Yii::$app->getSession()->setFlash('error', 'Maaf Silahkan Lengkapi Data Isian Anda');
                    return $this->redirect(['create']);
                }
            }
           
            if($cekdatachild1!=null){
                $model->id_plant=$cekdatachild1->id_plant;
                $model->id_soldto=$cekdatachild1->id_soldto;
               
                $datappski = OPpSki::find()->where(['ID' => $id])->one();
                // $material=RfcZZcsdListMatSalesSki::find()->where(['id'=>$model->id_material])->one();
                $datasoldto = RfcZZcsdShiptoSki::find()->where(['ID' => $model->id_soldto])->one();
                $datappski->id_soldto = $model->id_soldto;
                $datappski->nama_soldto = $datasoldto->NAMA_SOLDTO;
                $datamaterial = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->id_material])->orderBy('ID DESC')->one();
                $data = Harga::find(['PLANT' => $model->id_plant])->where(['SOLD_TO' => $model->id_soldto])->andWhere(['SHIP_TO' => $model->id_shipto])->andWhere(['KODE' => $model->id_material])->orderBy('ID DESC')->one();
                $model->uom = $datamaterial->UOM;
                $model->harga = $data->HARGA;
                $datatax = MasterTax::find()->where(['id' => $model->id_tax])->one();
                $model->subtotal = ($model->harga + ($model->harga * (($datatax->nominal_tax / 100)))) * $model->quantity;
                $model->status_konfirmasi = 'Open';
                $cekdata = ChildOPpSki::find()->orderBy('ID DESC')->one();
                $model->line_number =  0;
                $model->sisa_quantity = $model->quantity;
            }else{
                $datappski = OPpSki::find()->where(['ID' => $id])->one();
                // $material=RfcZZcsdListMatSalesSki::find()->where(['id'=>$model->id_material])->one();
                $datasoldto = RfcZZcsdShiptoSki::find()->where(['ID' => $model->id_soldto])->one();
                $datappski->id_soldto = $model->id_soldto;
                $datappski->nama_soldto = $datasoldto->NAMA_SOLDTO;
                $datamaterial = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->id_material])->orderBy('ID DESC')->one();
                $data = Harga::find(['PLANT' => $model->id_plant])->where(['SOLD_TO' => $model->id_soldto])->andWhere(['SHIP_TO' => $model->id_shipto])->andWhere(['KODE' => $model->id_material])->orderBy('ID DESC')->one();
                $model->uom = $datamaterial->UOM;
                $model->harga = $data->HARGA;
                $datatax = MasterTax::find()->where(['id' => $model->id_tax])->one();
                $model->subtotal = ($model->harga + ($model->harga * (($datatax->nominal_tax / 100)))) * $model->quantity;
                $model->status_konfirmasi = 'Open';
                $cekdata = ChildOPpSki::find()->orderBy('ID DESC')->one();
                $model->line_number =  0;
                $model->sisa_quantity = $model->quantity;
            }
            $model->save(false);
            $datappski->save(false);

            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->renderAjax('tambah-child', [
            'cekdatachild1' => $cekdatachild1,
            'childcek' => $childcek,
            'model' => $model,
        ]);
    }
    public function actionDone($id, $status = null)
    {
        $dataorder = OPpSki::find()->where(['ID' => $id])->one();
        if ($status == null) {
            $dataorder->status_op = 1; //10=standart data, 1=data dikirim untuk approval, 2=data di approval,3=data di reject

        } else {
            $dataorder->status_op = 2; //10=standart data, 1=data dikirim untuk approval, 2=data di approval,3=data di reject
            $dataorder->save(false);
            $cekdata=ChildOPpSki::find()->where(['id_pp'=>$id])->all();
            foreach($cekdata as $key=>$val){
                if($val->status_konfirmasi=='Open'){
                    \Yii::$app->getSession()->setFlash('error', 'Data Line Order Masih Ada Berstatus Open');
                    return $this->redirect(['view','id'=>$id,'status'=>$status]);
                }
            }
            \Yii::$app->getSession()->setFlash('success', 'Data Approval Berhasil Dikirim');
            return $this->redirect(['index']);
        }
        $dataorder->save(false);
        $datachild = ChildOPpSki::find()->where(['>', 'line_number', 0])->andWhere(['id_pp' => $id])->orderBy('ID DESC')->one();
        if ($datachild) {
            $line_number = $datachild->line_number + 1;
        } else {
            $line_number = 1;
        }
        $cekdatachild = ChildOPpSki::find()->where(['id_pp' => $id])->all();
        foreach ($cekdatachild as $key => $val) {
            $val->line_number = $line_number + $key;
            $val->save(false);
        }
        \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Dikirim, Silahkan Menunggu Approval');
        return $this->redirect(['create']);
    }
    public function actionCreate()
    {
        $model = new OPpSki();
        $model->create_by = \yii::$app->user->identity->username;
        $model->create_date = date('Y-m-d');
        $model->time_create = date("h:i:sa");
        $model->update_by = \yii::$app->user->identity->username;
        $model->update_date = date('Y-m-d');
        $model->time_update = date("h:i:sa");
        $model->delete_mark = 0;
        $cekdata = OPpSki::find()->where(['status_op' => 0])->andWhere(['create_by' => $model->create_by])->orderBy('ID DESC')->one();
        if ($cekdata) {
            \Yii::$app->getSession()->setFlash('error', 'Ada Data Yang Masih Open');
            return $this->redirect(['view', 'id' => $cekdata->ID]);
        }
        if ($model->load(Yii::$app->request->post())) {
            $cekdata = OPpSki::find()->orderBy('ID DESC')->one();
            if ($cekdata) {
                $model->line_number = $cekdata->line_number + 1;
                $model->order_number = 'SO-0000000' . $model->line_number;
            } else {
                $number = $model->line_number = 1;

                $model->order_number = 'SO-0000000' . $number;
            }
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OPpSki model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OPpSki model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = OPpSki::find()->where(['ID' => $id])->one();
        $model->delete_mark = 1;
        $model->update_by = \yii::$app->user->identity->username;
        $model->update_date = date('Y-m-d');
        $model->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Dikirim, Silahkan Menunggu Approval');
        return $this->redirect(['data-pp']);
    }

    /**
     * Finds the OPpSki model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OPpSki the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OPpSki::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
