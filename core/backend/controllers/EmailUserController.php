<?php

namespace backend\controllers;

use Yii;
use common\models\EmailUser;
use common\models\EmailUserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EmailUserController implements the CRUD actions for EmailUser model.
 */
class EmailUserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmailUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmailUserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmailUser model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmailUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    
    public function actionSendEmail1()
    {
		// find all active subscribers
		$message = Yii::$app->mailer->compose();
	
		$message->setTo(['aafadly364@gmail.com']);
		$message->setFrom([\Yii::$app->params['supportEmail'] => "Bartosz Wójcik"]);
		$message->setSubject('Test');
		$message->setTextBody('Ok Test');
	
		// $headers = $message->getSwiftMessage()->getHeaders();
	
		// // message ID header (hide admin panel)
		// $msgId = $headers->get('Message-ID');
		// $msgId->setId(md5(time()) . '@pelock.com');
	 
		$result = $message->send();
	
		return $result;
    }
    public function actionSendEmail()
    {
        $user='muhammadfadly190219@gmail.com';
        return Yii::$app
        ->mailer
        ->compose(
            ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
            ['user' => $user]
        )
        ->setFrom([Yii::$app->params['supportEmail'] => 'Pesan Dari SKI Order PO' . ' NOT REPLAY'])
        ->setTo(['muhammadfadly190219@gmail.com'])
        ->setSubject('Password reset for ' . Yii::$app->name)
        ->send();
    }

    public function actionCreate()
    {
        $model = new EmailUser();
        $model->create_by = yii::$app->user->identity->username;
        $model->create_date = date('Y-m-d');
        $model->update_by = yii::$app->user->identity->username;
        $model->update_date = date('Y-m-d');
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing EmailUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing EmailUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmailUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return EmailUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmailUser::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
