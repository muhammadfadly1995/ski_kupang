<?php

namespace backend\controllers;

use Yii;
use common\models\DataTruck;
use common\models\DataTruckSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DataTruckController implements the CRUD actions for DataTruck model.
 */
class DataTruckController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DataTruck models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DataTruckSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DataTruck model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DataTruck model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionInputExcel(){
        $model = new DataTruck();
        $model->create_by=\yii::$app->user->identity->username;
        $model->create_date=date('Y-m-d');
        $model->update_by=\yii::$app->user->identity->username;
        $model->update_date=date('Y-m-d');
        $model->delete_mark=0;
        if ($model->load(Yii::$app->request->post()) ) {
            $file = UploadedFile::getInstance($model, 'fileexcel');
            $fileName = 'Data-Siswa' . date('dmYhis') . '.' . $file->extension;
            $upload = $file->saveAs(Yii::getAlias('excel/data-truck/') . $fileName);
            if ($upload) {
                // $fileName = $model->picture->tempName;
                $img = \Yii::getAlias('excel/data-guru/') . $fileName;

                $inpuFileType = \PHPExcel_IOFactory::identify($img);

                $phpExcel = \PHPExcel_IOFactory::createReader($inpuFileType);

                $phpexcel1 = $phpExcel->load($img)->getsheet(0);

                $phpExcel->setReadDataOnly(true);
                $sheet = $phpExcel->load($img);

                //       
                foreach ($sheet->getActiveSheet()->toArray() as $key => $value) {
                    if ($key == 0) {
                        continue;
                    }

                    // $value[5]->getError();
                    // print_r($cek);
                    // die();
                    if ($value[4] != null) {
                        $cekdatasiswa = \common\models\DataTruck::find()->where(['no_pol' => $value[4]])->one();
                        if (!$cekdatasiswa) {
                            if (is_numeric($value[12])) {
                                $base_timestamp = mktime(0, 0, 0, 1, $value[12] - 1, 1900);
                                $tanggal = date("Y-m-d", $base_timestamp);
                            } else {
                                //$base_timestamp = mktime(0, 0, 0, 1,  - 1, 1900);
                                $tanggal = $value[12];
                            }
                            $model = new DataTruck();
        $model->create_by=\yii::$app->user->identity->username;
        $model->create_date=date('Y-m-d');
        $model->update_by=\yii::$app->user->identity->username;
        $model->update_date=date('Y-m-d');
        $model->delete_mark=0;

                            $datapegawai->status_pegawai = $value[6];
                            $datapegawai->nip = $value[7];
                            $datapegawai->jenis_ptk = $value[8];
                            $datapegawai->agama = $value[9];
                            $datapegawai->alamat = $value[10];
                            $datapegawai->kode_pos = $value[11];
                            if ($value[12]) {
                                $datapegawai->no_hp = $value[12];
                            } else {
                                $datapegawai->no_hp = '0';
                            }
                            if(!$value[13] || $value[13]==0){
                                $datapegawai->email = rand(1,1000).'@gmail.com';
                            }else{
                                $datapegawai->email = $value[13];
                            }
                          //  $datapegawai->email = $value[13];
                            $datapegawai->tugas_tambahan = $value[14];
                            $datapegawai->sk_cpns = $value[15];
                            $datapegawai->tanggal_cpns = $tanggal1;
                            $datapegawai->sk_pengangkatan = $value[17];
                            $datapegawai->tmt_pengangkatan = $tanggal2;
                            $datapegawai->lembaga_pengangkatan = $value[19];
                            $datapegawai->pangkat_golongan = $value[20];
                            $datapegawai->sumber_gaji = $value[21];
                            $datapegawai->status_perkawinan = $value[22];
                            $datapegawai->bank_rekening = $value[23];
                            $datapegawai->no_rekening = $value[24];
                            $datapegawai->nama_rekening = $value[25];
                            $datapegawai->nik = $value[26];
                            $datapegawai->foto = $value[27];
                            $datapegawai->status_aktif = 1;
                            $user = new \common\models\SignupForm();
                            //  $user->name = $value[1];
                            if ($value[7]) {
                                $user->username = $value[7];
                            } else {
                                $user->username = $value[26];
                            }
                        
                          


                            $datapegawai->id_user = $iduser;

                          
                            $datapegawai->save(false);
                        }
                    }
                }
               
            }
         

            Yii::$app->session->setFlash('success', 'Import Berhasil . . !');
            return $this->redirect(['index']);
           
        }else{

       
        return $this->render('export-excel', [
            'model' => $model,
        ]);
    }
    }

    public function actionCreate()
    {
        $model = new DataTruck();
        $model->create_by=\yii::$app->user->identity->username;
        $model->create_date=date('Y-m-d');
        $model->update_by=\yii::$app->user->identity->username;
        $model->update_date=date('Y-m-d');
        $model->delete_mark=0;
        if ($model->load(Yii::$app->request->post()) ) {
            $model->no_pol = strtoupper($model->no_pol);
           
            $model->no_pol=preg_replace("/-/","",  $model->no_pol);
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DataTruck model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DataTruck model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DataTruck model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DataTruck the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DataTruck::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
