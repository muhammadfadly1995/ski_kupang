<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "saldo_kas".
 *
 * @property int $id
 * @property int $nominal_kas
 * @property string $tanggal_update
 */
class SaldoKas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'saldo_kas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nominal_kas', 'tanggal_update'], 'required'],
            [['nominal_kas'], 'integer'],
            [['tanggal_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nominal_kas' => 'Nominal Kas',
            'tanggal_update' => 'Tanggal Update',
        ];
    }
}
