<?php

$vendorDir = dirname(__DIR__);

return array (
  'yiidoc/yii2-redactor' => 
  array (
    'name' => 'yiidoc/yii2-redactor',
    'version' => '2.0.1.0',
    'alias' => 
    array (
      '@yii/redactor' => '/',
    ),
  ),
  'kartik-v/yii2-widget-timepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-timepicker',
    'version' => '1.0.4.0',
    'alias' => 
    array (
      '@kartik/time' => $vendorDir . '/kartik-v/yii2-widget-timepicker/src',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '1.4.9.0',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker/src',
    ),
  ),
  'borales/yii2-phone-input' => 
  array (
    'name' => 'borales/yii2-phone-input',
    'version' => '0.3.0.0',
    'alias' => 
    array (
      '@borales/extensions/phoneInput' => $vendorDir . '/borales/yii2-phone-input/src',
    ),
  ),
  'yiisoft/yii2-httpclient' => 
  array (
    'name' => 'yiisoft/yii2-httpclient',
    'version' => '2.0.12.0',
    'alias' => 
    array (
      '@yii/httpclient' => $vendorDir . '/yiisoft/yii2-httpclient/src',
    ),
  ),
  'yiisoft/yii2-authclient' => 
  array (
    'name' => 'yiisoft/yii2-authclient',
    'version' => '2.1.3.0',
    'alias' => 
    array (
      '@yii/authclient' => $vendorDir . '/yiisoft/yii2-authclient',
    ),
  ),
  'dektrium/yii2-user' => 
  array (
    'name' => 'dektrium/yii2-user',
    'version' => '0.9.12.0',
    'alias' => 
    array (
      '@dektrium/user' => $vendorDir . '/dektrium/yii2-user',
    ),
    'bootstrap' => 'dektrium\\user\\Bootstrap',
  ),
  'dektrium/yii2-rbac' => 
  array (
    'name' => 'dektrium/yii2-rbac',
    'version' => '1.0.0.0-alpha',
    'alias' => 
    array (
      '@dektrium/rbac' => $vendorDir . '/dektrium/yii2-rbac',
    ),
    'bootstrap' => 'dektrium\\rbac\\Bootstrap',
  ),
  'himiklab/yii2-recaptcha-widget' => 
  array (
    'name' => 'himiklab/yii2-recaptcha-widget',
    'version' => '1.2.2.0',
    'alias' => 
    array (
      '@himiklab/yii2/recaptcha' => $vendorDir . '/himiklab/yii2-recaptcha-widget/src',
      '@himiklab/yii2/recaptcha/tests' => $vendorDir . '/himiklab/yii2-recaptcha-widget/tests',
    ),
  ),
  'kartik-v/yii2-widget-fileinput' => 
  array (
    'name' => 'kartik-v/yii2-widget-fileinput',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@kartik/file' => $vendorDir . '/kartik-v/yii2-widget-fileinput/src',
    ),
  ),
  'cebe/yii2-gravatar' => 
  array (
    'name' => 'cebe/yii2-gravatar',
    'version' => '1.1.0.0',
    'alias' => 
    array (
      '@cebe/gravatar' => $vendorDir . '/cebe/yii2-gravatar/cebe/gravatar',
    ),
  ),
  'bizley/quill' => 
  array (
    'name' => 'bizley/quill',
    'version' => '2.6.1.0',
    'alias' => 
    array (
      '@bizley/quill' => $vendorDir . '/bizley/quill/src',
    ),
  ),
  'bizley/podium' => 
  array (
    'name' => 'bizley/podium',
    'version' => '0.7.0.0',
    'alias' => 
    array (
      '@bizley/podium' => $vendorDir . '/bizley/podium/src',
    ),
  ),
  'navatech/yii2-migration-generator' => 
  array (
    'name' => 'navatech/yii2-migration-generator',
    'version' => '1.0.0.2',
    'alias' => 
    array (
      '@navatech/migration' => $vendorDir . '/navatech/yii2-migration-generator',
    ),
    'bootstrap' => 'navatech\\migration\\Bootstrap',
  ),
  'deesoft/yii2-gii' => 
  array (
    'name' => 'deesoft/yii2-gii',
    'version' => '1.4.0.0',
    'alias' => 
    array (
      '@dee/gii' => $vendorDir . '/deesoft/yii2-gii',
    ),
  ),
  'dmstr/yii2-db' => 
  array (
    'name' => 'dmstr/yii2-db',
    'version' => '1.0.0.0',
    'alias' => 
    array (
      '@dmstr/db' => $vendorDir . '/dmstr/yii2-db/db',
      '@dmstr/db/tests' => $vendorDir . '/dmstr/yii2-db/db/tests',
      '@dmstr/console' => $vendorDir . '/dmstr/yii2-db/console',
      '@dmstr/widgets' => $vendorDir . '/dmstr/yii2-db/widgets',
    ),
  ),
  'bizley/migration' => 
  array (
    'name' => 'bizley/migration',
    'version' => '2.3.1.0',
    'alias' => 
    array (
      '@bizley/migration' => $vendorDir . '/bizley/migration',
    ),
  ),
  'insolita/yii2-migration-generator' => 
  array (
    'name' => 'insolita/yii2-migration-generator',
    'version' => '2.3.0.0',
    'alias' => 
    array (
      '@insolita/migrik' => $vendorDir . '/insolita/yii2-migration-generator',
    ),
    'bootstrap' => 'insolita\\migrik\\Bootstrap',
  ),
  'kartik-v/yii2-mpdf' => 
  array (
    'name' => 'kartik-v/yii2-mpdf',
    'version' => '1.0.6.0',
    'alias' => 
    array (
      '@kartik/mpdf' => $vendorDir . '/kartik-v/yii2-mpdf/src',
    ),
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base/src',
    ),
  ),
  'kartik-v/yii2-widget-select2' => 
  array (
    'name' => 'kartik-v/yii2-widget-select2',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/select2' => $vendorDir . '/kartik-v/yii2-widget-select2/src',
    ),
  ),
  'kartik-v/yii2-widget-activeform' => 
  array (
    'name' => 'kartik-v/yii2-widget-activeform',
    'version' => '1.5.8.0',
    'alias' => 
    array (
      '@kartik/form' => $vendorDir . '/kartik-v/yii2-widget-activeform/src',
    ),
  ),
  'kartik-v/yii2-sortable' => 
  array (
    'name' => 'kartik-v/yii2-sortable',
    'version' => '1.2.2.0',
    'alias' => 
    array (
      '@kartik/sortable' => $vendorDir . '/kartik-v/yii2-sortable/src',
    ),
  ),
  'kartik-v/yii2-dialog' => 
  array (
    'name' => 'kartik-v/yii2-dialog',
    'version' => '1.0.5.0',
    'alias' => 
    array (
      '@kartik/dialog' => $vendorDir . '/kartik-v/yii2-dialog/src',
    ),
  ),
  'kartik-v/yii2-bootstrap4-dropdown' => 
  array (
    'name' => 'kartik-v/yii2-bootstrap4-dropdown',
    'version' => '1.0.1.0',
    'alias' => 
    array (
      '@kartik/bs4dropdown' => $vendorDir . '/kartik-v/yii2-bootstrap4-dropdown/src',
    ),
  ),
  'kartik-v/yii2-grid' => 
  array (
    'name' => 'kartik-v/yii2-grid',
    'version' => '3.3.5.0',
    'alias' => 
    array (
      '@kartik/grid' => $vendorDir . '/kartik-v/yii2-grid/src',
    ),
  ),
  'kartik-v/yii2-dynagrid' => 
  array (
    'name' => 'kartik-v/yii2-dynagrid',
    'version' => '1.5.1.0',
    'alias' => 
    array (
      '@kartik/dynagrid' => $vendorDir . '/kartik-v/yii2-dynagrid/src',
    ),
  ),
  'kartik-v/yii2-export' => 
  array (
    'name' => 'kartik-v/yii2-export',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/export' => $vendorDir . '/kartik-v/yii2-export/src',
    ),
  ),
  'kartik-v/yii2-widget-datepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datepicker',
    'version' => 'dev-master',
    'alias' => 
    array (
      '@kartik/date' => $vendorDir . '/kartik-v/yii2-widget-datepicker/src',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '2.0.10.0',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap/src',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '2.1.15.0',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug/src',
    ),
  ),
  'yiisoft/yii2-faker' => 
  array (
    'name' => 'yiisoft/yii2-faker',
    'version' => '2.0.5.0',
    'alias' => 
    array (
      '@yii/faker' => $vendorDir . '/yiisoft/yii2-faker/src',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '2.1.4.0',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii/src',
    ),
  ),
  'yiisoft/yii2-imagine' => 
  array (
    'name' => 'yiisoft/yii2-imagine',
    'version' => '2.2.0.0',
    'alias' => 
    array (
      '@yii/imagine' => $vendorDir . '/yiisoft/yii2-imagine/src',
    ),
  ),
  'kartik-v/yii2-money' => 
  array (
    'name' => 'kartik-v/yii2-money',
    'version' => '1.2.3.0',
    'alias' => 
    array (
      '@kartik/money' => $vendorDir . '/kartik-v/yii2-money/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '2.0.7.0',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  '2amigos/qrcode-library' => 
  array (
    'name' => '2amigos/qrcode-library',
    'version' => '1.1.2.0',
    'alias' => 
    array (
      '@Da/QrCode' => $vendorDir . '/2amigos/qrcode-library/src',
    ),
  ),
);
