<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RfcZZcsdListMatSalesSkiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Material';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzapp-select-sysplan-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                'KODE_MATERIAL',
                'DESKRIPSI:ntext',
                'TYPE_MATERIAL',
                'BERAT',
                'UOM',
                'BERAT_KONFERSI',
                'UOM_KONFERSI',
                'CREATE_BY',
                'CREATE_DATE',
                'LAST_UPDATE_BY',
                'LAST_UPDATE_DATE',
                'DELETE_MARK',
               
                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>



        </div>
    </div>
</div>