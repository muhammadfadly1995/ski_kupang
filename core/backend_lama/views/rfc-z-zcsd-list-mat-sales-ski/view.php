<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZcsdListMatSalesSki */

$this->title = $model->KODE_MATERIAL;
$this->params['breadcrumbs'][] = ['label' => 'Material', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzcsd-list-mat-sales-ski-view">

            <h1><?= Html::encode($this->title) ?></h1>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'ID',
                        'KODE_MATERIAL',
                        'DESKRIPSI:ntext',
                        'BERAT',
                        'UOM',
                        'TYPE_MATERIAL',
                        'BERAT_KONFERSI',
                        //  'DELETE_MARK',
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'ID',
                        'UOM_KONFERSI',
                        'CREATE_BY',
                        'CREATE_DATE',
                        'LAST_UPDATE_BY',
                        'LAST_UPDATE_DATE',
                        //  'DELETE_MARK',
                    ],
                ]) ?>
            </div>
        </div>
        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>