<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OPpSki */

$this->title = 'Update O Pp Ski: ' . $model->ID;
$this->params['breadcrumbs'][] = ['label' => 'O Pp Skis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="opp-ski-update">
<ul class="nav nav-tabs pull-right">
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/o-pp-ski/data-pp']) ?>"> Data PP</a>
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/o-pp-ski/create']) ?>"> Create PP</a>
        <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/o-pp-ski/']) ?>"> Approval PP</a>


    </ul>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
