<?php

use common\models\ChildOPpSki;
use common\models\ChildSholdto;
use common\models\MasterAngkut;
use common\models\MasterTax;
use common\models\MasterTop;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\OPpSki */

$this->title = 'Number Order ' . $model->order_number;
$this->params['breadcrumbs'][] = ['label' => 'O Pp Skis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="opp-ski-view">

                <h3><?= Html::encode($this->title) ?></h3>

                <p>
                    <?php if ($model->delete_mark == 0) { ?>
                        <?php if ($status == null) { ?>
                            <?php
                            if ($model->status_op != 2) {
                            ?>
                                <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
                                <?= Html::a('Delete', ['delete', 'id' => $model->ID], [
                                    'class' => 'btn btn-danger',
                                    'data' => [
                                        'confirm' => 'Are you sure you want to delete this item?',
                                        'method' => 'post',
                                    ],
                                ]) ?>
                            <?php } ?>
                          
                        <?php } ?>
                    <?php } ?>
                    <?= Html::a('Kembali', ['create'], ['class' => 'btn btn-default']) ?>
                </p>
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //  'ID',
                            'order_type',
                            'nama_soldto',
                            'order_number',
                            //  'line_number',
                            'create_by',

                            //  'delete_mark',
                        ],
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //  'ID',
                            [
                                'attribute' => 'create_date',

                                'contentOptions' => ['class' => 'text-left'],
                                'value' => function ($model) {

                                    return $model->create_date . '|' . $model->time_create;
                                }
                            ],

                            'update_by',
                            [
                                'attribute' => 'update_date',

                                'contentOptions' => ['class' => 'text-left'],
                                'value' => function ($model) {

                                    return $model->update_date . '|' . $model->time_update;
                                }
                            ],

                            //  'delete_mark',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="box-header">
                <h1 class="box-title"><?= 'Detail Order' ?></h1>
                <br>
                <?php if ($model->delete_mark == 0) { ?>
                    <?php if ($status == null) { ?>
                        <?php if ($model->status_op != 2) { ?>
                            <p>
                                <a href="<?php echo \yii\helpers\Url::to(['tambah-child', 'id' => $model->ID]); ?>" class="btn btn-success" data-target="#modalvote" data-toggle="modal"><?php echo 'Tambah Data'; ?></a>
                                <?php $cekdatatrue = ChildOPpSki::find()->where(['id_pp' => $model->ID])->one();
                                if ($cekdatatrue) {
                                ?>
                                    <?= Html::a('Kirim Order', ['done', 'id' => $model->ID], ['class' => 'btn btn-default']) ?>
                                <?php } ?>
                            <?php } ?>
                            </p>
                        <?php } else { ?>
                            <?= Html::a('Kirim Approval Order', ['done', 'id' => $model->ID, 'status' => 'Approval'], ['class' => 'btn btn-default']) ?>
                            <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>

                        <?php } ?>
                    <?php } ?>

            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table>
                        <thead style="background-color: #7fc6b6;">
                            <tr>
                                <th>No</th>
                                <th>Soldto</th>
                                <th>Shipto</th>
                                <th>Plant</th>
                                <th>Material</th>
                                <th>Tipe Angkut</th>
                                <th>Quantity</th>
                                <th>UOM</th>
                                <th>Tanggal Order</th>
                                <th>Tanggal Terima</th>
                                <th>Term Of Payment</th>
                                <th>Harga</th>
                                <th>Jenis TAX</th>
                                <th>Subtotal</th>
                                <th>Status Konfirmasi</th>
                                <th>Action By</th>
                                <th>Update Date</th>

                                <th>Action</th>


                            </tr>
                        </thead>
                        <?php if ($datachild) {
                            $no = 1;
                            foreach ($datachild as $key => $val) {
                                $datasoldto = RfcZZcsdShiptoSki::find()->where(['ID' => $val->id_soldto])->one();
                                $childsoldto = ChildSholdto::find()->where(['ID' => $val->id_shipto])->one();
                                $plant = RfcZZappSelectSysplanSki::find()->where(['ID' => $val->id_plant])->one();
                                $material = RfcZZcsdListMatSalesSki::find()->where(['ID' => $val->id_material])->one();
                                $tipeangkut = MasterAngkut::find()->where(['id' => $val->id_angkut])->one();
                                $top = MasterTop::find()->where(['id' => $val->id_top])->one();
                                $tax = MasterTax::find()->where(['id' => $val->id_tax])->one();
                        ?>

                                <tr>
                                    <td><?= $no + $key ?></td>
                                    <td><?= $datasoldto->KODE_SOLDTO . '/ ' . $datasoldto->NAMA_SOLDTO ?></td>
                                    <td><?= $childsoldto->KODE_SHIPTO . '/ ' . $childsoldto->NAMA_SHIPTO ?></td>
                                    <td><?= $plant->WERKS . '/ ' . $plant->NAME1 ?></td>
                                    <td><?= $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI ?></td>
                                    <td><?= $tipeangkut->code_angkut ?></td>
                                    <td><?= $val->quantity ?></td>
                                    <td><?= $val->uom ?></td>
                                    <td><?= $val->date_order . '|' . $val->time_order ?></td>
                                    <td><?= $val->date_receipt ?></td>
                                    <td><?= $top->code_top . '/ ' . $top->deskripsi ?></td>
                                    <td><?= 'Rp.' . number_format($val->harga); ?></td>
                                    <td><?= $tax->deskripsi . '(' . $tax->nominal_tax . '%) ' . ' ' . $tax->tax_code ?></td>
                                    <td><?= 'Rp.' . number_format($val->subtotal); ?></td>
                                    <td><?= $val->status_konfirmasi ?></td>
                                    <td><?= $val->approval_by . $val->approval_reject ?></td>
                                    <td><?= $val->approval_date . $val->approval_date_reject ?></td>

                                    <?php if ($status == 'Approval') { ?>
                                        <td>
                                            <?php if ($val->status_konfirmasi == 'Open') { ?>
                                                <?= Html::a('<i class="fa fa-check-square-o"></i>', ['approval', 'id' => $val->ID, 'ids' => $model->ID, 'status' => $status], ['class' => 'btn btn-black',  'title' => 'Approval', 'data' => [
                                                    'confirm' => 'Approval Data ?',
                                                    'method' => 'post',
                                                ],]) ?>

                                                <br>
                                                <?= Html::a('<i class="fa fa-times-circle-o"></i>', ['reject', 'id' => $val->ID, 'ids' => $model->ID, 'status' => $status], ['class' => 'btn btn-black', 'title' => 'Hapus',  'data' => [
                                                    'confirm' => 'Reject Data ?',
                                                    'method' => 'post',
                                                ],]) ?>
                                            <?php } else { ?>
                                                <?= 'Status ' . $val->status_konfirmasi ?>
                                            <?php } ?>
                                        </td>
                                    <?php } else {
                                    ?>
                                        <td>
                                            <?php if ($val->status_konfirmasi == 'Open') { ?>
                                                <?= Html::a('<i class="fa fa-pencil-square-o"></i>', ['tambah-child', 'idchild' => $val->ID, 'id' => $model->ID], ['class' => 'btn btn-black', 'data-target' => '#modalvote', 'data-toggle' => 'modal', 'title' => 'Update',]) ?>

                                                <br>
                                                <?= Html::a('<i class="fa fa-times-circle-o"></i>', ['delete-child', 'id' => $val->ID, 'ids' => $model->ID], ['class' => 'btn btn-black', 'title' => 'Hapus',  'data' => [
                                                    'confirm' => 'Delete Data ?',
                                                    'method' => 'post',
                                                ],]) ?>
                                            <?php } else { ?>
                                                <?= 'Status ' . $val->status_konfirmasi ?>
                                            <?php } ?>
                                        </td>
                                    <?php } ?>
                                </tr>

                        <?php }
                        } ?>


                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal remote fade" id="modalvote">
    <div class="modal-dialog">
        <div class="modal-content loader-lg"></div>
    </div>
</div>