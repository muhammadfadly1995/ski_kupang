<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\OPpSki */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="opp-ski-form">
       
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'order_type')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\TypeMaster::find()->asArray()->all(), 'code_type', 'deskripsi'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Type...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

           

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>