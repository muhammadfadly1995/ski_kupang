<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TypeTruck */

$this->title = 'Update Type Truck: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Type Trucks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="type-truck-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
