<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ChildOPpSki */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="child-opp-ski-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_soldto')->textInput() ?>

    <?= $form->field($model, 'id_shipto')->textInput() ?>

    <?= $form->field($model, 'id_plant')->textInput() ?>

    <?= $form->field($model, 'id_material')->textInput() ?>

    <?= $form->field($model, 'id_angkut')->textInput() ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'date_order')->textInput() ?>

    <?= $form->field($model, 'date_receipt')->textInput() ?>

    <?= $form->field($model, 'id_top')->textInput() ?>

    <?= $form->field($model, 'harga')->textInput() ?>

    <?= $form->field($model, 'id_tax')->textInput() ?>

    <?= $form->field($model, 'subtotal')->textInput() ?>

    <?= $form->field($model, 'create_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'create_date')->textInput() ?>

    <?= $form->field($model, 'approval_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'approval_date')->textInput() ?>

    <?= $form->field($model, 'approval_reject')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'approval_date_reject')->textInput() ?>

    <?= $form->field($model, 'delete_mark')->textInput() ?>

    <?= $form->field($model, 'status_konfirmasi')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
