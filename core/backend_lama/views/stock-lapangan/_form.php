<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\StockLapangan */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="stock-lapangan-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'id_mat')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZcsdListMatSalesSki::find()->asArray()->all(), 'ID', 'KODE_MATERIAL', 'DESKRIPSI'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Material...', 'id' => 'table-mat', 'onchange' => '$.post( "listregssmat?id=' . '"+$(this).val(),
            function( data ) {
                $( "#table-uom").html( data );
            });
           
            $.post( "getharga?id=' . '"+$(this).val(),
            function( data ) {
               
                $( "#id-harga").html( data );
            });
            ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Material');
                ?>

             
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'jumlah')->textInput() ?>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>