<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StockLapangan */

$this->title = 'Update Stock Lapangan: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Stock Lapangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="stock-lapangan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
