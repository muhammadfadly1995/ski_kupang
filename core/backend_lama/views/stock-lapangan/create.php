<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\StockLapangan */

$this->title = 'Create Stock Lapangan';
$this->params['breadcrumbs'][] = ['label' => 'Stock Lapangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="stock-lapangan-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
