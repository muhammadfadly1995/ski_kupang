<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RfcZZcsdListMatSalesSkiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Stock Lapangan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzapp-select-sysplan-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Tambah Data', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                //'id',
                [
                    'attribute' => 'id_mat',
                    'value' => function ($model) {
                        $material = \common\models\RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->id_mat])->one();
                        $status = $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI;
                        return $status;
                    }
                ],
            'satuan',
            'jumlah',
            'create_by',
            'create_date',
            'update_by',
            'update_date',
            [
                'attribute' => 'delete_mark',
                'label'=>'Status',
                'value' => function ($model) {
                    if($model->delete_mark==1){
                        $status='Data Dicancel';
                    }else{
                        $status='Data Active';
                    }
                    return $status;
                }
            ],
          //  'delete_mark',
               
                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>



        </div>
    </div>
</div>