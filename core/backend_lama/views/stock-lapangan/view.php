<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\StockLapangan */

$this->title = 'View Stock Lapangan';
$this->params['breadcrumbs'][] = ['label' => 'Stock Lapangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="stock-lapangan-view">

            <h4><?= Html::encode($this->title) ?></h4>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    //   'id',
                    [
                        'attribute' => 'id_mat',
                        'value' => function ($model) {
                            $material = \common\models\RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->id_mat])->one();
                            $status = $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI;
                            return $status;
                        }
                    ],
                //    'id_mat',
                    'satuan',
                    'jumlah',
                    'create_by',
                    'create_date',
                    // 'update_by',
                    // 'update_date',
                    //  'delete_mark',
                ],
            ]) ?>

        </div>
        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>