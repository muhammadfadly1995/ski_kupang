<?php

use common\models\BarangKeluar;
use common\models\PengeluaranBarang;
use common\models\SaldoKas;
use common\models\TableBarang;
use common\models\User;
use kartik\date\DatePicker;
use yii\bootstrap\ActiveField;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\models\PengeluaranBarangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekap Laporan Keuangan';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<div class="box-header">
    <h3 class="box-title"><?= Html::encode($this->title) ?></h3>

</div>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>
                    <div class="row">
                        <div class="col-md-9">
                            <label class="col-md-2 control-label">

                                <strong>
                                    Search By Date
                                </strong>
                            </label>
                            <div class="col-md-5">
                                <?= $form->field($model, 'tanggal_awal')->label(false)->widget(DatePicker::className(), [
                                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-d', 'target' => '_Blank']
                                ]);
                                ?>
                            </div>
                            <div class="col-sm-1">
                                <strong>
                                    S/D
                                </strong>
                            </div>
                            <div class="col-md-4">
                                <?= $form->field($model, 'tanggal_akhir')->label(false)->widget(DatePicker::className(), [
                                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-d', 'target' => '_Blank']
                                ]);
                                ?>
                            </div>

                        </div>



                        <div class="col-md-3">

                            <?= Html::submitButton('Cari', ['class' => 'btn btn-primary', 'name' => 'simpan', 'value' => 'true']) ?>
                            <?php //echo Html::submitButton('Print Data', ['class' => 'btn btn-primary', 'name' => 'cetak', 'target' => '_Blank', 'value' => 'true']) 
                            ?>



                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="box-body">
                <table>
                    <thead style="background-color: #7fc6b6;">
                        <tr>
                            <th>No</th>
                            <th>Tanggal Transaksi</th>
                            <th>Administratror</th>
                            <th>Keterangan Transaksi</th>
                            <th>Jumlah</th>
                            <th>Nominal Transaksi</th>
                            <th>Pengurangan Transaksi</th>
                            <th>Debit</th>
                            <th>Kredit</th>
                        </tr>
                    </thead>
                    <?php
                    $totalnominal = 0;
                    $totalpengurangan = 0;
                    $debit = 0;
                    $kredit = 0;
                    if ($data != null) {
                        $no = 1;

                        foreach ($data as $key => $val) {
                            $totalnominal = $totalnominal + $val->totalnominal;
                            $totalpengurangan = $totalpengurangan + $val->totalpengurangan;
                            $debit = $debit + $val->totaldebit;
                            $kredit = $kredit + $val->totalkredit;

                    ?>
                            <tr>
                                <td><?= $no + $key ?></td>
                                <td><?php
                                    $formattanggal = strtotime($val->tanggal);
                                    $tanggal = date('d-m-Y', $formattanggal);
                                    echo $tanggal ?></td>
                                <td><?= $val->administrator ?></td>
                                <td><?= $val->keterangan ?></td>
                                <td><?= $val->jumlah ?></td>
                                <td><?= $val->nominal ?></td>
                                <td><?= $val->pengurangan ?></td>
                                <td><?= $val->debit ?></td>
                                <td><?= $val->kredit ?></td>
                            </tr>
                    <?php
                        }
                    } ?>
                    <tr>
                        <td colspan="5"></td>

                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>

                        <th colspan="5">Saldo Kas</th>

                        <th></th>
                        <th></th>
                        <th><?php
                        $saldokas=SaldoKas::find()->one();
                      echo  'Rp ' . number_format($saldokas->nominal_kas) ?></th>
                        <th></th>
                    </tr>
                    <tr>

                        <th colspan="5">Subtotal</th>

                        <th><?= 'Rp ' . number_format($totalnominal) ?></th>
                        <th><?= 'Rp ' . number_format($totalpengurangan) ?></th>
                        <th><?= 'Rp ' . number_format($debit) ?></th>
                        <th><?= 'Rp ' . number_format($kredit) ?></th>
                    </tr>

                </table>
            </div>
        </div>
    </div>
</div>