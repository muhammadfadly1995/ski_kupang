<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\MasterTop */

$this->title = $model->deskripsi;
$this->params['breadcrumbs'][] = ['label' => 'Master Tops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="master-top-view">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    // 'id',
                    'code_top',
                    'deskripsi',
                    'create_by',
                    'create_date',
                    'update_by',
                    'update_date',
                //    'delete_mark',
                ],
            ]) ?>

        </div>
        <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
    </div>
</div>