<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterTop */

$this->title = 'Create Master Top';
$this->params['breadcrumbs'][] = ['label' => 'Master Tops', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-top-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
