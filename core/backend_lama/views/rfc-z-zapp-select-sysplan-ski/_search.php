<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZappSelectSysplanSkiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rfc-zzapp-select-sysplan-ski-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'XPARAM') ?>

    <?= $form->field($model, 'WERKS') ?>

    <?= $form->field($model, 'NAME1') ?>

    <?= $form->field($model, 'CREATE_BY') ?>

    <?php // echo $form->field($model, 'CREATE_DATE') ?>

    <?php // echo $form->field($model, 'LAST_UPDATE_DATE') ?>

    <?php // echo $form->field($model, 'LAST_UPDATE_BY') ?>

    <?php // echo $form->field($model, 'DELETE_MARK') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
