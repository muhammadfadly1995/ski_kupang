<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\RfcZZappSelectSysplanSki */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzapp-select-sysplan-ski-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">


                <?= $form->field($model, 'WERKS')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'NAME1')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'ALAMAT')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'ID_DISTRIK')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\PtMasterDistrikSki::find()->asArray()->all(), 'ID', 'KODE_DISTRIK', 'DISTRIK'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Distrik...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
                <?= $form->field($model, 'LONGITUDE')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'LATITUDE')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'CREATE_BY')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <?= $form->field($model, 'CREATE_DATE')->textInput(['readOnly' => true]) ?>

                <?= $form->field($model, 'LAST_UPDATE_DATE')->textInput(['readOnly' => true]) ?>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            
        </div>
    </div>
</div>