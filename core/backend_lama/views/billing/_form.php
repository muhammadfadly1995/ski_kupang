<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Billing */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="billing-form">

                <?php $form = ActiveForm::begin(); ?>
                <div class="col-md-10">
                    <?= $form->field($model, 'no_billing')->textInput(['maxlength' => true, 'style' => 'text-transform:uppercase', 'placeholder' => 'Input No SPJ'])->label(false) ?>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>
        </div>
        <div class="row">
            <div class="box-header">
                <h1 class="box-title"><?= 'Data SPJ' ?></h1>
                <br>


            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table>
                        <thead style="background-color: #7fc6b6;">
                            <tr>
                                <th>No</th>

                                <th>No SPJ</th>
                                <th>Qty</th>
                                <th>Nilai Masuk</th>
                                <th>Nilai Keluar</th>
                                <th>Nilai Akhir</th>
                                <th>Tanggal Masuk</th>
                                <th>Tanggal Keluar</th>


                                <th>Action</th>


                            </tr>
                        </thead>
                        <?php if ($dataspj) {
                            $no = 1;
                            foreach ($dataspj as $key => $val) {

                        ?>

                                <tr>
                                    <td><?= $no + $key ?></td>

                                    <td><?= $val->no_spj ?></td>

                                    <td><?= $val->total_nilai ?></td>
                                    <td><?= $val->nilai_masuk . ' KG' ?></td>
                                    <td><?= $val->nilai_keluar . ' KG' ?></td>
                                    <td><?= $val->nilai_akhir . ' KG' ?></td>
                                    <td><?= $val->tanggal_masuk ?></td>
                                    <td><?= $val->tanggal_keluar ?></td>




                                    <td>

                                        <?= Html::a('<i class="fa fa-check-square-o"></i>', ['approval'], ['class' => 'btn btn-black',  'title' => 'Approval', 'data' => [
                                            'confirm' => 'Pilih Data ?',
                                            'method' => 'post',
                                        ],]) ?>

                                        <br>
                                        <?= Html::a('<i class="fa fa-times-circle-o"></i>', ['reject'], ['class' => 'btn btn-black', 'title' => 'Hapus',  'data' => [
                                            'confirm' => 'Reject Data ?',
                                            'method' => 'post',
                                        ],]) ?>

                                    </td>



                                </tr>

                        <?php }
                        } ?>


                    </table>
                </div>
            </div>
        </div>
    </div>
</div>