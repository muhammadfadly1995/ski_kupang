<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MasterTax */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="master-tax-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="col-md-6">
                <?= $form->field($model, 'deskripsi')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'tax_code')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'nominal_tax')->textInput()->label('Nilai Persentase (Tanpa Menggunakan %)') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'create_by')->textInput(['maxlength' => true,'readOnly'=>true]) ?>

                <?= $form->field($model, 'create_date')->textInput(['readOnly'=>true]) ?>
            </div>
            
            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>