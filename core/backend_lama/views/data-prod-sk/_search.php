<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DataProdSkSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-prod-sk-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'data_type') ?>

    <?= $form->field($model, 'data_version') ?>

    <?= $form->field($model, 'date_prod') ?>

    <?= $form->field($model, 'material_code') ?>

    <?php // echo $form->field($model, 'remark') ?>

    <?php // echo $form->field($model, 'plant') ?>

    <?php // echo $form->field($model, 'stock_awal') ?>

    <?php // echo $form->field($model, 'recive') ?>

    <?php // echo $form->field($model, 'issued') ?>

    <?php // echo $form->field($model, 'stock_akhir') ?>

    <?php // echo $form->field($model, 'create_by') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'update_by') ?>

    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'delete_mark') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
