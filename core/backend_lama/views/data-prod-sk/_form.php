<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\money\MaskMoney;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model common\models\DataProdSk */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="data-prod-sk-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'data_type')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ['Stock'=>'Stock','Prod'=>'Prod'],
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'data_version')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ['SK'=>'SK','SKI'=>'SKI'],
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih ...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'date_prod')->widget(DatePicker::className(), [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd', 'target' => '_Blank']
                ]);
                ?>

                <?= $form->field($model, 'material_code')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZcsdListMatSalesSki::find()->asArray()->all(), 'ID', 'KODE_MATERIAL', 'DESKRIPSI'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Material...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'remark')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'plant')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZappSelectSysplanSki::find()->asArray()->all(), 'ID', 'WERKS', 'NAME1'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Plant...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'stock_awal')->textInput() ?>

                <?= $form->field($model, 'recive')->textInput() ?>

                <?= $form->field($model, 'issued')->textInput() ?>
            </div>
            <div class="col-md-12">


                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>