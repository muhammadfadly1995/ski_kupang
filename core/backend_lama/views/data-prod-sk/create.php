<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DataProdSk */

$this->title = 'Create Data Prod Sk';
$this->params['breadcrumbs'][] = ['label' => 'Data Prod Sks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-prod-sk-create">

    <h4><?= Html::encode($this->title) ?></h4>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
