<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MasterAngkut */

$this->title = 'Create Master Angkut';
$this->params['breadcrumbs'][] = ['label' => 'Master Angkuts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="master-angkut-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
