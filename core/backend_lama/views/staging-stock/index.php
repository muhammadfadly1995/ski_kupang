<?php

use common\models\ChildSholdto;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
use common\models\TableExpeditur;
use common\models\TypeTruck;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Table Staging Stock';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>
            <ul class="nav nav-tabs pull-right">
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/staging-stock']) ?>"> Staging Stock</a>
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/timbangan-incoming/index-in']) ?>">Timbangan Incomming</a>
               


            </ul>
           

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                [
                    'attribute' => 'id_plant',
                    'label' => 'Plant',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = RfcZZappSelectSysplanSki::find()->where(['ID' => $model->id_plant])->one();
                        return $datatruck->WERKS . ' ' . $datatruck->NAME1;
                    }
                ],
            //    'id_plant',
                [
                    'attribute' => 'id_mat',
                    'label' => 'Material',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = RfcZZcsdListMatSalesSki::find()->where(['ID' => $model->id_mat])->one();
                        return $datatruck->KODE_MATERIAL . ' ' . $datatruck->DESKRIPSI;
                    }
                ],
               // 'id_mat',
               [
                'attribute' => 'qty',
                //  'label' => 'Qty',
                'contentOptions' => ['class' => 'text-left'],
                'value' => function ($model) {

                    return number_format($model->qty,2) . ' TO';
                }
            ],
                'last_update',
                //'delete_mark',
               
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
    </div>
</div>