<?php
/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use common\models\StockBarang;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
use yii\helpers\StringHelper;
use yii\bootstrap\Modal;

AppAsset::register($this);

$user = Yii::$app->user->identity;
$user = Yii::$app->user->identity;
if (!$user) {
    header("location: ../");
    exit();
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= 'SCaning' ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link rel="shortcut icon" type="image/x-icon" href="<?= yii\helpers\Url::toRoute(['/favicon.ico']) ?>" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>

<body class="hold-transition skin-yellow sidebar-mini fixed">
    <?php $this->beginBody() ?>
    <div class="wrapper">
        <header class="main-header">
            <!-- Logo -->
            <a href="<?= Url::toRoute(['/']) ?>" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini"> <i class="fa fa-home"></i></span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-sm" style="font-size:16px;"><?= 'SKI' ?></span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= Url::toRoute(['/site/profile-photo', 'inline' => true]) ?>" class="user-image" alt="User Image" style="background-color: #d8f1ec;">
                                <span class="hidden-xs"><?= ucwords($user->name) ?></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= Url::toRoute(['/site/profile-photo', 'inline' => true]) ?>" class="img-circle" alt="User Image" style="background-color: #333333;">

                                    <p>
                                        <small>Member since <?= date("Y M d", $user->created_at) ?></small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="<?= Url::toRoute(['/site/profile']) ?>" class="btn btn-default btn-flat">Profile</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?= Url::to(['/site/logout']) ?>" data-method="post" class="btn btn-default btn-flat">Log Out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">
                <!-- Sidebar user panel -->
                <div class="user-panel">
                    <div class="pull-left image">
                        <img src="<?= Url::toRoute(['/site/profile-photo', 'inline' => true]) ?>" class="img-circle" alt="User Image" style="background-color: #d8f1ec;">
                    </div>
                    <div class="pull-left info">
                        <p><?= ucwords($user->name) ?></p>
                        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
                    </div>
                </div>
                <br>
                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu">
                    <li class="header">MAIN NAVIGATION</li>
                    <li <?= Yii::$app->controller->id == 'site' ? 'class=active' : '' ?>>
                        <a href="<?= Url::toRoute(['/']) ?>">
                            <i class="fa fa-home text-yellow"></i> <span>Home</span>
                        </a>
                    </li>
                    <?php if (\yii::$app->user->identity->role == 'admin' || \yii::$app->user->identity->role == 'Penjualan') { ?>

                        <li class="header">MASTER DATA</li>
                        <li class="treeview">
                            <a href="/#">
                                <i class="fa fa-envelope-square"></i>
                                <span>Mater Data</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href=<?php echo \yii\helpers\Url::to(['/type-master']); ?>><i class="fa fa-briefcase"></i>Master Type</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/master-angkut']); ?>><i class="fa fa-briefcase"></i>Master Angkut</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/master-top']); ?>><i class="fa fa-briefcase"></i>Master TOP</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/master-tax']); ?>><i class="fa fa-briefcase"></i>Master TAX</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/table-expeditur']); ?>><i class="fa fa-briefcase"></i>Data Expeditur</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/supir']); ?>><i class="fa fa-briefcase"></i>Data Supir</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/conveyor']); ?>><i class="fa fa-briefcase"></i>Data Conveyor</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/table-sloc']); ?>><i class="fa fa-briefcase"></i>Data Sloc</a></li>

                                <li><a href=<?php echo \yii\helpers\Url::to(['/type-truck']); ?>><i class="fa fa-briefcase"></i>Tipe Truck</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/data-truck']); ?>><i class="fa fa-briefcase"></i>Data Truck</a></li>

                                <li><a href=<?php echo \yii\helpers\Url::to(['/pt-master-distrik-ski']); ?>><i class="fa fa-briefcase"></i>Distrik</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/rfc-z-zapp-select-sysplan-ski']); ?>><i class="fa fa-briefcase"></i>Plant</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/rfc-z-zcsd-shipto-ski']); ?>><i class="fa fa-briefcase"></i>Customer</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/rfc-z-zcsd-list-mat-sales-ski']); ?>><i class="fa fa-briefcase"></i>Material</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/harga']); ?>><i class="fa fa-briefcase"></i>Harga</a></li>

                            </ul>
                        <li class="header">Cancel Transaksi</li>
                        <li class="treeview">
                            <a href="/#">
                                <i class="fa fa-envelope-square"></i>
                                <span>Cancel Transaksi</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href=<?php echo \yii\helpers\Url::to(['/o-pp-ski/cancel-transaksi']); ?>><i class="fa fa-briefcase"></i>Cancel SO</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/table-matching/cancel-transaksi']); ?>><i class="fa fa-briefcase"></i>Cancel Matching</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/proses-timbangan/cancel-transaksi']); ?>><i class="fa fa-briefcase"></i>Cancel Timbangan</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/billing/cancel-transaksi']); ?>><i class="fa fa-briefcase"></i>Cancel Billing</a></li>


                            </ul>
                        </li>

                    <?php } ?>
                    </li>
                    <?php if (\yii::$app->user->identity->role == 'admin' || \yii::$app->user->identity->role == 'Penjualan' || \yii::$app->user->identity->role == 'Distributor') { ?>
                        <li class="header">TRANSAKSI SO</li>
                        <li class="treeview">
                            <a href=<?php echo \yii\helpers\Url::to(['/o-pp-ski/data-pp']); ?>>
                                <i class="fa fa-envelope-square"></i>
                                <span>Transaksi SO</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>

                        </li>

                    <?php } ?>
                    <?php if (\yii::$app->user->identity->role == 'admin') { ?>
                        <li class="header">STOCK LAPANGAN</li>
                        <li class="treeview">
                            <a href=<?php echo \yii\helpers\Url::to(['/stock-lapangan']); ?>>
                                <i class="fa fa-envelope-square"></i>
                                <span>Stock Lapangan</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>

                        </li>

                    <?php } ?>
                    <?php if (\yii::$app->user->identity->role == 'admin') { ?>

                        <li class="header">INPUT PO</li>
                        <li class="treeview">
                            <a href=<?php echo \yii\helpers\Url::to(['/data-po/']); ?>>
                                <i class="fa fa-envelope-square"></i>
                                <span>Transaksi PO</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>

                        </li>
                        <li class="header">DATA PROD SK</li>
                        <li class="treeview">
                            <a href=<?php echo \yii\helpers\Url::to(['/data-prod-sk/']); ?>>
                                <i class="fa fa-envelope-square"></i>
                                <span>Data Prod SK</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>

                        </li>
                    <?php } ?>
                    <?php if (\yii::$app->user->identity->role == 'admin' || \yii::$app->user->identity->role == 'Petugas Timbang') { ?>
                        <li class="header">MATCHING DATA</li>
                        <li class="treeview">
                            <a href="<?php echo \yii\helpers\Url::to(['/table-matching/create']); ?>">
                                <i class="fa fa-envelope-square"></i>
                                <span>Matching Data</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>

                        </li>
                        <li class="header">TIMBANGAN</li>
                        <li class="treeview">
                            <a href="/#">
                                <i class="fa fa-envelope-square"></i>
                                <span>Timbangan</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a href=<?php echo \yii\helpers\Url::to(['/proses-timbangan/create']); ?>><i class="fa fa-briefcase"></i>Timbangan OutComming</a></li>
                                <li><a href=<?php echo \yii\helpers\Url::to(['/timbangan-incoming/create']); ?>><i class="fa fa-briefcase"></i>Timbangan Incoming</a></li>


                            </ul>
                        </li>
                    <?php } ?>
                    <?php if (\yii::$app->user->identity->role == 'admin') { ?>
                        <li class="header">Billing</li>
                        <li class="treeview">
                            <a href="<?php echo \yii\helpers\Url::to(['/billing/']); ?>">
                                <i class="fa fa-envelope-square"></i>
                                <span>Billing</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>

                        </li>
                        <li class="header">Staging Stock</li>
                        <li class="treeview">
                            <a href="<?php echo \yii\helpers\Url::to(['/staging-stock']); ?>">
                                <i class="fa fa-envelope-square"></i>
                                <span>Staging Stock</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>

                        </li>
                        <li class="header">MODUL DATA</li>
                        <li class="header">USER NAVIGATION</li>
                        <li <?= Yii::$app->controller->id == 'user' ? 'class=active' : '' ?>>
                            <a href="<?= Url::toRoute(['/user']) ?>">
                                <i class="fa fa-users text-yellow"></i> <span>User</span>
                            </a>
                        </li>
                    <?php } ?>
                </ul>
            </section>
            <!-- /.sidebar -->
        </aside>

        <!-- =============================================== -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Main content -->
            <section class="content">

                <div class="row">
                    <div class="col-lg-12">
                        <?php if (Yii::$app->session->getFlash('success')) : ?>
                            <div id="w0-success-0" class="alert alert-success" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?= Yii::$app->session->getFlash('success') ?>
                            </div>
                        <?php elseif (Yii::$app->session->getFlash('error')) : ?>
                            <div id="w0-error-0" class="alert alert-danger" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?= Yii::$app->session->getFlash('error') ?>
                            </div>
                        <?php elseif (Yii::$app->session->getFlash('warning')) : ?>
                            <div id="w0-error-0" class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <?= Yii::$app->session->getFlash('warning') ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>

                <?= $content ?>

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.1.5
            </div>
            <strong>Copyright &copy; <?= date('Y') ?> <a href="<?= Url::toRoute(['/']) ?>"><?= 'SKI' ?></a> </strong>
        </footer>

    </div>

    <script>
        $('th:contains("No.")').css('width', '10px');
        $('th.action-column').css('width', '30px');
    </script>

    <!--jangan diganggu-->
    <?php
    Modal::begin([
        'header' => 'janganDiganggu',
        'id' => 'modalJanganDiganggu',
        'size' => 'modal-lg',
    ]);

    echo "<div id='janganDiganggu'></div>";

    Modal::end();
    ?>
    <!--jangan diganggu-->

    <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>