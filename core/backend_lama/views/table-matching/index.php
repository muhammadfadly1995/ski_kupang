<?php

use common\models\ChildSholdto;
use common\models\DataTruck;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
use common\models\Supir;
use common\models\TableExpeditur;
use common\models\TypeTruck;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Witing Matching';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>
            <ul class="nav nav-tabs pull-right">
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/table-matching/create']) ?>"> Create Matching</a>
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/table-matching']) ?>"> Waiting Matching</a>
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/table-matching/index-done']) ?>"> Data Matching</a>


            </ul>


            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                //  'id',
                'no_matching',
                [
                    'attribute' => 'id_supir',
                    'label' => 'Nama Supir/SIM',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $dataso = Supir::find()->where(['id' => $model->id_supir])->one();
                        return $dataso->nama_supir . '/' . $dataso->no_sim;
                    }
                ],
                // 'no_so', [
                [
                    'attribute' => 'id_truck',
                    'label' => 'No Polisi',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                        return $datatruck->no_pol;
                    }
                ],
                [
                    //'attribute' => 'id_truck',
                    'label' => 'Kapasitas Truck',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                        return $datatruck->kapasitas_angkut . ' ' . $datatruck->satuan;
                    }
                ],
                [
                    //'attribute' => 'id_truck',
                    'label' => 'Supir/Sim',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                        $datasupir = Supir::find()->where(['id' => $datatruck->id_supir])->one();
                        return $datasupir->nama_supir . '/' . $datasupir->no_sim;
                    }
                ],
                [
                    //'attribute' => 'id_truck',
                    'label' => 'Expeditur',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $dataexpeditur = TableExpeditur::find()->where(['id' => $model->id_expeditur])->one();
                        // $datasupir=Supir::find()->where(['id'=>$datatruck->id_supir])->one();
                        return $dataexpeditur->code_expeditur . '/' . $dataexpeditur->deskripsi;
                    }
                ],

                // 'create_by',
                // 'create_date',
                // 'update_by',
                // 'update_date',
                // 'delete_mark',

                ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
    </div>
</div>