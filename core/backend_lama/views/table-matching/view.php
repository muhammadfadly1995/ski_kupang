<?php

use common\models\ChildMatching;
use common\models\DataTruck;
use common\models\OPpSki;
use common\models\Supir;
use common\models\TableExpeditur;
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ChildOPpSki;
use common\models\ChildSholdto;
use common\models\MasterAngkut;
use common\models\MasterTax;
use common\models\MasterTop;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model common\models\TableMatching */

$this->title = $model->no_matching;
$this->params['breadcrumbs'][] = ['label' => 'Table Matchings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);

?>
<style>
    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td,
    th {
        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }

    tr:nth-child(even) {
        background-color: #dddddd;
    }
</style>
<script>
    function pejabatan() {
        //  print_r($id);die();
        var cara = document.getElementById("dataso").value;

        var pokjas = document.getElementById("detailso");

        // console.log(cara);
        if (cara != null) {

            // pokjas.style.display = 'visible';
            //  console.log(cara);


            pokjas.style.display = 'visible';

        } else {
            pokjas.style.display = 'none';
        }
    }

    window.onload = function() {
        pejabatan();
    };
</script>
<div class="box">
    <div class="box-body">
        <div class="row">
            <div class="table-matching-view">

                <h1><?= Html::encode($this->title) ?></h1>

                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            //  'id',
                            'no_matching',
                            [
                                'attribute' => 'id_supir',
                                'label' => 'Nama Supir',
                                'contentOptions' => ['class' => 'text-left'],
                                'value' => function ($model) {
                                    $dataso = Supir::find()->where(['id' => $model->id_supir])->one();
                                    return $dataso->nama_supir . '/' . $dataso->no_sim;
                                }
                            ],
                            // 'no_so', [
                            [
                                'attribute' => 'id_truck',
                                'label' => 'No Polisi',
                                'contentOptions' => ['class' => 'text-left'],
                                'value' => function ($model) {
                                    $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                                    return $datatruck->no_pol;
                                }
                            ],


                            'create_by',
                            [
                                'attribute' => 'create_date',

                                'contentOptions' => ['class' => 'text-left'],
                                'value' => function ($model) {

                                    return $model->create_date . '|' . $model->time_create;
                                }
                            ],
                            // 'update_by',
                            // 'update_date',   
                            // 'delete_mark',
                        ],
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            [
                                //'attribute' => 'id_truck',
                                'label' => 'Kapasitas Truck',
                                'contentOptions' => ['class' => 'text-left'],
                                'value' => function ($model) {
                                    $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                                    return $datatruck->kapasitas_angkut . ' ' . $datatruck->satuan;
                                }
                            ],
                            [
                                //'attribute' => 'id_truck',
                                'label' => 'Supir/Sim',
                                'contentOptions' => ['class' => 'text-left'],
                                'value' => function ($model) {
                                    $datatruck = DataTruck::find()->where(['id' => $model->id_truck])->one();
                                    $datasupir = Supir::find()->where(['id' => $datatruck->id_supir])->one();
                                    return $datasupir->nama_supir . '/' . $datasupir->no_sim;
                                }
                            ],
                            [
                                //'attribute' => 'id_truck',
                                'label' => 'Expeditur',
                                'contentOptions' => ['class' => 'text-left'],
                                'value' => function ($model) {
                                    $dataexpeditur = TableExpeditur::find()->where(['id' => $model->id_expeditur])->one();
                                    // $datasupir=Supir::find()->where(['id'=>$datatruck->id_supir])->one();
                                    return $dataexpeditur->code_expeditur . '/' . $dataexpeditur->deskripsi;
                                }
                            ],
                        ],
                    ]) ?>
                </div>
            </div>
            <div class="col-md-12">
                <?= Html::a('Kembali', ['create'], ['class' => 'btn btn-default']) ?>
            </div>
        </div>
        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="box-header">
                    <h1 class="box-title"><?= 'Pilih SO' ?></h1>
                    <br>


                </div>
                <div class="table-matching-form">

                    <?php $form = ActiveForm::begin(); ?>


                    <div class="col-md-10">
                        <?php if ($statusdis == 0) { ?>
                            <?= $form->field($modelmat, 'no_so')->widget(\kartik\select2\Select2::classname(), [
                                'data' => ArrayHelper::map(common\models\OPpSki::find()->where(['delete_mark' => 0])->andWhere(['status_op' => 2])->asArray()->all(), 'ID', 'order_number', 'order_type'),
                                'language' => 'eng',
                                'options' => ['placeholder' => 'Pilih SO...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false);
                            ?>
                        <?php } else { ?>
                           
                            <?= $form->field($modelmat, 'no_so')->widget(\kartik\select2\Select2::classname(), [
                                'data' => ArrayHelper::map(common\models\OPpSki::find()->where(['delete_mark' => 0])->andWhere(['status_op' => 2])->andWhere(['ID'=>$statusdis])->asArray()->all(), 'ID', 'order_number', 'order_type'),
                                'language' => 'eng',
                                'options' => ['placeholder' => 'Pilih SO...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ])->label(false);
                            ?>
                        <?php } ?>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <?= Html::submitButton('Search', ['class' => 'btn btn-success']) ?>

                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>
            </div>
        </div>
        <?php if ($datasearchso != null) { ?>
            <div class="row">
                <div id="detailso">
                    <div class="box-header">
                        <h1 class="box-title"><?= 'Detail SO' ?></h1>
                        <br>


                    </div>

                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table>
                                <thead style="background-color: #7fc6b6;">
                                    <tr>
                                        <th>No</th>

                                        <th>Soldto</th>
                                        <th>Shipto</th>
                                        <th>Plant</th>
                                        <th>Material</th>
                                        <th>Tipe Angkut</th>
                                        <th>Sisa Quantity</th>
                                        <th>Matching Quantity</th>
                                        <th>UOM</th>

                                        <th>Harga</th>





                                    </tr>
                                </thead>
                                <?php if ($datasearchso) {
                                    $no = 1;
                                    foreach ($datasearchso as $key => $val) {
                                        $datasoldto = RfcZZcsdShiptoSki::find()->where(['ID' => $val->id_soldto])->one();
                                        $childsoldto = ChildSholdto::find()->where(['ID' => $val->id_shipto])->one();
                                        $plant = RfcZZappSelectSysplanSki::find()->where(['ID' => $val->id_plant])->one();
                                        $material = RfcZZcsdListMatSalesSki::find()->where(['ID' => $val->id_material])->one();
                                        $tipeangkut = MasterAngkut::find()->where(['id' => $val->id_angkut])->one();
                                        $top = MasterTop::find()->where(['id' => $val->id_top])->one();
                                        $tax = MasterTax::find()->where(['id' => $val->id_tax])->one();
                                        $datachildmatching = ChildMatching::find()->where(['id_matching' => $model->id])->andWhere(['id_so' => $model->no_so])->andWhere(['id_child_so' => $val->ID])->one();
                                        if ($datachildmatching) {
                                            $idchild = $datachildmatching->id;
                                            $nodo = $datachildmatching->no_do;
                                            $quantitychild = $datachildmatching->quantity;
                                        } else {
                                            $idchild = 0;
                                            $nodo = '-';
                                            $quantitychild = 0;
                                        }
                                ?>

                                        <tr> 
                                            <td><?= $no + $key ?></td>

                                            <td><?= $datasoldto->KODE_SOLDTO . '/ ' . $datasoldto->NAMA_SOLDTO ?></td>
                                            <td><?= $childsoldto->KODE_SHIPTO . '/ ' . $childsoldto->NAMA_SHIPTO ?></td>
                                            <td><?= $plant->WERKS . '/ ' . $plant->NAME1 ?></td>
                                            <td><?= $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI ?></td>
                                            <td><?= $tipeangkut->code_angkut ?></td>
                                            <td><?= $val->sisa_quantity ?></td>
                                            <td> <a href="<?php echo \yii\helpers\Url::to(['input-quantity', 'id' => $val->ID, 'idmat' => $model->id, 'idso' => $val->id_pp, 'idchilmat' => $idchild,]); ?>" data-target='#modalvote' , data-toggle='modal'><?= $quantitychild ?></a></td>
                                            <td><?= $val->uom ?></td>

                                            <td><?= 'Rp.' . number_format($val->harga); ?></td>





                                        </tr>

                                    <?php }
                                } else { ?>
                                    Data Tidak Ada
                                <?php } ?>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <br>
        <div class="row">
            <div class="box-header">
                <h1 class="box-title"><?= 'Detail Matching' ?> || <?= Html::a('Cetak DO/Selesai', ['selesai-do', 'id' => $model->id], ['class' => 'btn btn-success']) ?></h1>

                <br>


            </div>
            <div class="col-md-12">
                <div class="table-responsive">
                    <table>
                        <thead style="background-color: #7fc6b6;">
                            <tr>
                                <th>No</th>
                                <th>No SO</th>
                                <th>No DO</th>
                                <th>Soldto</th>
                                <th>Shipto</th>
                                <th>Plant</th>
                                <th>Material</th>
                                <th>Tipe Angkut</th>
                                <th>Sisa Quantity</th>
                                <th>Quantity Matching</th>
                                <th>UOM</th>
                                <th>Tanggal Order</th>
                                <th>Tanggal Terima</th>
                                <th>Term Of Payment</th>
                                <th>Harga</th>
                                <th>Jenis TAX</th>
                                <th>Subtotal</th>
                                <th>Status Konfirmasi</th>
                                <th>Approval By</th>




                            </tr>
                        </thead>
                        <?php if ($datachild) {
                            $no = 1;
                            foreach ($datachild as $key => $val) {
                                $datasoldto = RfcZZcsdShiptoSki::find()->where(['ID' => $val->id_soldto])->one();
                                $childsoldto = ChildSholdto::find()->where(['ID' => $val->id_shipto])->one();
                                $plant = RfcZZappSelectSysplanSki::find()->where(['ID' => $val->id_plant])->one();
                                $material = RfcZZcsdListMatSalesSki::find()->where(['ID' => $val->id_material])->one();
                                $tipeangkut = MasterAngkut::find()->where(['id' => $val->id_angkut])->one();
                                $top = MasterTop::find()->where(['id' => $val->id_top])->one();
                                $tax = MasterTax::find()->where(['id' => $val->id_tax])->one();
                                $datachildmatching = ChildMatching::find()->where(['id_matching' => $model->id])->andWhere(['id_child_so' => $val->ID])->one();
                                if ($datachildmatching) {
                                    $datacekso = OPpSki::find()->where(['ID' => $val->id_pp])->one();
                                    $noso = $datacekso->order_number;
                                    $idchild = $datachildmatching->id;
                                    $nodo = $datachildmatching->no_do;
                                    $quantitychild = $datachildmatching->quantity;
                                } else {
                                    $noso = '-';
                                    $idchild = 0;
                                    $nodo = '-';
                                    $quantitychild = 0;
                                }
                        ?>

                                <tr>
                                    <td><?= $no + $key ?></td>
                                    <td><?= $noso; ?></td>
                                    <td><?= $nodo; ?></td>
                                    <td><?= $datasoldto->KODE_SOLDTO . '/ ' . $datasoldto->NAMA_SOLDTO ?></td>
                                    <td><?= $childsoldto->KODE_SHIPTO . '/ ' . $childsoldto->NAMA_SHIPTO ?></td>
                                    <td><?= $plant->WERKS . '/ ' . $plant->NAME1 ?></td>
                                    <td><?= $material->KODE_MATERIAL . '/ ' . $material->DESKRIPSI ?></td>
                                    <td><?= $tipeangkut->code_angkut ?></td>
                                    <td><?= $val->sisa_quantity ?></td>
                                    <td> <a href="<?php echo \yii\helpers\Url::to(['input-quantity', 'id' => $val->ID, 'idmat' => $model->id, 'idso' => $val->id_pp, 'idchilmat' => $idchild,]); ?>" data-target='#modalvote' , data-toggle='modal'><?= $quantitychild ?></a></td>
                                    <td><?= $val->uom ?></td>
                                    <td><?= $val->date_order ?></td>
                                    <td><?= $val->date_receipt ?></td>
                                    <td><?= $top->code_top . '/ ' . $top->deskripsi ?></td>
                                    <td><?= 'Rp.' . number_format($val->harga); ?></td>
                                    <td><?= $tax->deskripsi . '(' . $tax->nominal_tax . '%) ' . ' ' . $tax->tax_code ?></td>
                                    <td><?= 'Rp.' . number_format(($val->subtotal / $val->quantity) * $quantitychild); ?></td>
                                    <td><?= $val->status_konfirmasi ?></td>
                                    <td><?= $val->approval_by ?></td>




                                </tr>

                        <?php }
                        } ?>


                    </table>
                    <br>

                </div>
            </div>
        </div>

    </div>
</div>
<div class="modal remote fade" id="modalvote">
    <div class="modal-dialog">
        <div class="modal-content loader-lg"></div>
    </div>
</div>