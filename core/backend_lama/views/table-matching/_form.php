<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model common\models\TableMatching */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="table-matching-form">

            <?php $form = ActiveForm::begin(); ?>



           
             <?= $form->field($model, 'id_supir')->widget(\kartik\select2\Select2::classname(), [
                'data' => ArrayHelper::map(common\models\Supir::find()->where(['delete_mark' => 0])->asArray()->all(), 'id', 'nama_supir'),
                'language' => 'eng',
                'options' => ['placeholder' => 'Pilih...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Pilih Supir');
            ?>
            <?= $form->field($model, 'id_expeditur')->widget(\kartik\select2\Select2::classname(), [
                'data' => ArrayHelper::map(common\models\TableExpeditur::find()->where(['delete_mark' => 0])->asArray()->all(), 'id', 'code_expeditur','deskripsi'),
                'language' => 'eng',
                'options' => [
                    'placeholder' => 'Pilih Sold To...', 'onchange' => '$.post( "listregss?id=' . '"+$(this).val(),
        function( data ) {
            $( "#table-barang").html( data );
        });
       
        ',
                ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Pilih Expeditur');
            ?>
            <?= $form->field($model, 'id_truck')->widget(\kartik\select2\Select2::classname(), [
               // 'data' => ArrayHelper::map(common\models\DataTruck::find()->where(['delete_mark' => 0])->asArray()->all(), 'id', 'no_pol'),
                'language' => 'eng',
                'options' => ['placeholder' => 'Pilih...','id'=>'table-barang'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ])->label('Pilih Truck');
            ?>


            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                <?= Html::a('Kembali', ['create'], ['class' => 'btn btn-default']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>