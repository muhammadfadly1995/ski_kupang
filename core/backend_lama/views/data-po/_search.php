<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\DataPoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="data-po-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_com') ?>

    <?= $form->field($model, 'id_plant') ?>

    <?= $form->field($model, 'no_po') ?>

    <?= $form->field($model, 'vendor') ?>

    <?php // echo $form->field($model, 'no_pol') ?>

    <?php // echo $form->field($model, 'material') ?>

    <?php // echo $form->field($model, 'qty') ?>

    <?php // echo $form->field($model, 'sloc') ?>

    <?php // echo $form->field($model, 'create_by') ?>

    <?php // echo $form->field($model, 'create_date') ?>

    <?php // echo $form->field($model, 'update_by') ?>

    <?php // echo $form->field($model, 'update_date') ?>

    <?php // echo $form->field($model, 'delete_mark') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
