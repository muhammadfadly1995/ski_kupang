<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DataPo */

$this->title = 'Create Data Po';
$this->params['breadcrumbs'][] = ['label' => 'Data Pos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-po-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
