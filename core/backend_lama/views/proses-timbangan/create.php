<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ProsesTimbangan */

$this->title = 'Proses Timbangan';
$this->params['breadcrumbs'][] = ['label' => 'Proses Timbangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="proses-timbangan-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'modelmat' => $modelmat,
        'model' => $model,
    ]) ?>

</div>