<?php

use common\models\Conveyor;
use common\models\PtMasterDistrikSki;
use common\models\TableMatching;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RfcZZcsdShiptoSkiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cancel Timbangan';
$this->params['breadcrumbs'][] = $this->title;
?>
 <div class="box">
        <div class="box-body">
            <div class="rfc-zzapp-select-sysplan-ski-index">

                <h3><?= Html::encode($this->title) ?></h3>



                <?php // echo $this->render('_search', ['model' => $searchModel]); 
                ?>
                <?php
                $gridColumns = [
                    ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                  
                    [
                        'attribute' => 'id_matching',
                        'label' => 'No Matching',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            $dataso = TableMatching::find()->where(['id' => $model->id_matching])->one();
                            return $dataso->no_matching ;
                        }
                    ],
                  //  'id_matching',
                  [
                    'attribute' => 'id_conveyor',
                    'label' => 'Conveyor',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        if ($datatruck) {
                            return $datatruck->code_conveyor . '/' . $datatruck->deskripsi;
                        }
                    }
                ],
                   // 'total_nilai',
                   [
                    'attribute' => 'nilai_masuk',
                    'label' => 'Nilai Timbangan Masuk',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return number_format($model->nilai_masuk,2) . ' KG';
                    }
                ],
                [
                    'attribute' => 'nilai_keluar',
                    'label' => 'Nilai Timbangan Keluar',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return number_format($model->nilai_keluar,2) . ' KG';
                    }
                ],
                [
                    'attribute' => 'nilai_akhir',
                    'label' => 'Berat Akhir',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return number_format($model->nilai_akhir,2) . ' KG';
                    }
                ],
                    'tanggal_masuk',
                    'tanggal_keluar',
                    'jam_masuk',
                    'jam_keluar',
                    //'create_by',
                    //'create_date',
                    //'update_by',
                    //'update_date',
                    //'delete_mark',
                    [
                        'attribute' => 'delete_mark',
                        'label' => 'Status Transaksi',
                        'contentOptions' => ['class' => 'text-left'],
                        'value' => function ($model) {
                            if($model->delete_mark==1){
                                $status='Transaksi Dicancel';
                            }else{
                                $status='Success';
                            }
                            return $status;
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => ' {myButton}',  // the default buttons + your custom button
                        'buttons' => [
                            'myButton' => function ($url, $model, $key) {     // render your custom button
                                return Html::a('Cancel Transaksi', ['cancel-data-transaksi', 'id' => $model->id],['data-target'=>"#modalvote" ,'data-toggle'=>"modal"]);
                            }
                        ]
                    ]
                   
                ];


                echo ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'clearBuffers' => true, //optional
                ]);

                echo \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns
                ]);
                ?>



            </div>
        </div>
    </div>
    <div class="modal remote fade" id="modalvote">
    <div class="modal-dialog">
        <div class="modal-content loader-lg"></div>
    </div>
</div>