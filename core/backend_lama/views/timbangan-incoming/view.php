<?php

use common\models\DataPo;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TimbanganIncoming */

$this->title = $model->no_spj;
$this->params['breadcrumbs'][] = ['label' => 'Timbangan Incomings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="timbangan-incoming-view">

            <h1><?= Html::encode($this->title) ?></h1>


            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'id',
                        'no_spj',
                        'no_pol',
                        [
                            'attribute' => 'id_po',
                            'label' => 'No PO',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                $datatruck = DataPo::find()->where(['id' => $model->id_po])->one();
                                return $datatruck->no_po;
                            }
                        ],
                        //  'id_po',
                        'tanggal_masuk',
                        'tanggal_keluar',
                        'jam_masuk',


                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'id',
                     //   'tanggal',
                        [
                            'attribute' => 'qty_po',
                            // 'label' => 'No PO',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {

                                return  number_format($model->qty_po) . ' TO';
                            }
                        ],
                        'jam_keluar',
                        [
                            'attribute' => 'nilai_masuk',
                            // 'label' => 'No PO',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {

                                return  number_format($model->nilai_masuk,2) . ' KG';
                            }
                        ],
                        // 'nilai_masuk',
                        [
                            'attribute' => 'nilai_keluar',
                            // 'label' => 'No PO',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {

                                return  number_format($model->nilai_keluar,2) . ' KG';
                            }
                        ],
                       //  'nilai_keluar',
                        [
                            'attribute' => 'subtotal',
                            // 'label' => 'No PO',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {

                                return number_format($model->subtotal,2) . ' KG';
                            }
                        ],
                        //  'subtotal',

                    ],
                ]) ?>
            </div>

        </div>
        <div class="col-md-12">
            <?= Html::a('Selesai', ['create'], ['class' => 'btn btn-default']) ?>
            <?= Html::a('Cetak SPJ', ['cetak-spj','id'=>$model->id],['class' => 'btn btn-success', 'target' => '_blank']) ?>
        </div>
    </div>
</div>