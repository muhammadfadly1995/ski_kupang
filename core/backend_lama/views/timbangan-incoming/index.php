<?php

use common\models\ChildSholdto;
use common\models\DataPo;
use common\models\DataTruck;
use common\models\PtMasterDistrikSki;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdShiptoSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use common\models\MasterTax;
use common\models\Supir;
use common\models\TableExpeditur;
use common\models\TypeTruck;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\HargaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Timbangan Incomming';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="pt-master-distrik-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>
            <ul class="nav nav-tabs pull-right">
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/timbangan-incoming/create']) ?>"> Create Timbangan</a>
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/timbangan-incoming/in']) ?>"> Timbangan Masuk</a>
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/timbangan-incoming/']) ?>"> Timbangan Close</a>


            </ul>


            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
                //  'id',
                // 'id',
                'no_spj',
                [
                    'attribute' => 'id_po',
                    //'label' => 'Expeditur',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        $dataexpeditur = DataPo::find()->where(['id' => $model->id_po])->one();
                        // $datasupir=Supir::find()->where(['id'=>$datatruck->id_supir])->one();
                        return $dataexpeditur->no_po;
                    }
                ],
               // 'id_po',
                'tanggal_masuk',
                'tanggal_keluar',
                'jam_masuk',
                'jam_keluar',

                [
                    'attribute' => 'nilai_masuk',
                    //'label' => 'Nilai Timbangan Masuk',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return  number_format($model->nilai_masuk,2) . ' KG'; 
                    }
                ],
                [
                    'attribute' => 'nilai_keluar',
                    //'label' => 'Nilai Timbangan Keluar',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return number_format($model->nilai_keluar,2) . ' KG';
                    }
                ],
                [
                    'attribute' => 'subtotal',
                    //'label' => 'Berat Akhir',
                    'contentOptions' => ['class' => 'text-left'],
                    'value' => function ($model) {
                        //  $datatruck = Conveyor::find()->where(['id' => $model->id_conveyor])->one();
                        return  number_format($model->subtotal,2) . ' KG';
                    }
                ],

              

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => ' {myButton}',  // the default buttons + your custom button
                    'buttons' => [
                        'myButton' => function ($url, $model, $key) {     // render your custom button
                            return Html::a('View', ['view', 'id' => $model->id]);
                        }
                    ]
                ]
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>


        </div>
    </div>
</div>