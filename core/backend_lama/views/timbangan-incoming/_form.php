<?php 

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\DetailView;
/* @var $this yii\web\View */
/* @var $model common\models\TimbanganIncoming */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="timbangan-incoming-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-10">
                <?= $form->field($model, 'no_pol')->textInput(['placeholder' => 'No Pol','style' => 'text-transform:uppercase'])->label(false) ?>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-default']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<div class="box">
    <div class="box-body">
        <div class="timbangan-incoming-view">




            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'id',
                        'no_spj',

                        'id_po',
                        'tanggal_masuk',
                        'tanggal_keluar',
                        'jam_masuk',


                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //  'id',

                        'jam_keluar',

                        'nilai_masuk',

                        'nilai_keluar',

                        'subtotal',

                    ],
                ]) ?>
            </div>

        </div>
        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'nilai_masuk')->textInput(['type' => 'number','placeholder'=>'Nilai Timbangan','style'=>'text-align:center; height: 70px ','readOnly'=>true])->label(false) ?>
        <?php ActiveForm::end(); ?>
    </div>
</div>