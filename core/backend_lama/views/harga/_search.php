<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\HargaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="harga-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'PLANT') ?>

    <?= $form->field($model, 'SOLD_TO') ?>

    <?= $form->field($model, 'SHIP_TO') ?>

    <?= $form->field($model, 'DISTRIK') ?>

    <?php // echo $form->field($model, 'KODE') ?>

    <?php // echo $form->field($model, 'DESKRIPSI') ?>

    <?php // echo $form->field($model, 'HARGA') ?>

    <?php // echo $form->field($model, 'VALID_FROM') ?>

    <?php // echo $form->field($model, 'VALID_TO') ?>

    <?php // echo $form->field($model, 'CREATE_BY') ?>

    <?php // echo $form->field($model, 'CREATE_DATE') ?>

    <?php // echo $form->field($model, 'LAST_UPDATE_BY') ?>

    <?php // echo $form->field($model, 'LAST_UPDATE_DATE') ?>

    <?php // echo $form->field($model, 'DELETE_MARK') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
