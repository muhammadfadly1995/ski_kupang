<?php
 
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use kartik\money\MaskMoney;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model common\models\Harga */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="harga-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'PLANT')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZappSelectSysplanSki::find()->asArray()->all(), 'ID', 'WERKS', 'NAME1'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Plant...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'SOLD_TO')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZcsdShiptoSki::find()->asArray()->all(), 'ID', 'KODE_SOLDTO', 'NAMA_SOLDTO'),
                    'language' => 'eng',
                    'options' => [
                        'placeholder' => 'Pilih Sold To...', 'onchange' => '$.post( "listregss?id=' . '"+$(this).val(),
            function( data ) {
                $( "#table-barang").html( data );
            });
           
            ',
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ])->label('Pilih Sold To');
                ?>

                <?= $form->field($model, 'SHIP_TO')->widget(\kartik\select2\Select2::classname(), [
                    //  'data' => ArrayHelper::map(common\models\ChildSholdto::find()->asArray()->all(), 'ID', 'KODE_SHIPTO', 'NAMA_SHIPTO'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Ship To...', 'id' => 'table-barang'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'DISTRIK')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\PtMasterDistrikSki::find()->asArray()->all(), 'ID', 'KODE_DISTRIK', 'DISTRIK'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Distrik...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'KODE')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\RfcZZcsdListMatSalesSki::find()->asArray()->all(), 'ID', 'KODE_MATERIAL', 'DESKRIPSI'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Material...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>

                <?= $form->field($model, 'DESKRIPSI')->textarea(['rows' => 6]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'HARGA')->widget(
                    MaskMoney::classname(),
                    [
                        'options' => [
                            //  'id' => 'harga-barang',
                            'onchange' => "myFunction()"
                        ],
                        'pluginOptions' => [
                            'prefix' => 'Rp. ',
                            'suffix' => '',
                            'affixesStay' => TRUE,
                            'precision' => 0,
                            'allowZero' => false,
                            'allowNegative' => false,

                        ],
                    ]
                )->label('Harga ');
                ?>
                <?= $form->field($model, 'TAX_PPN')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\MasterTax::find()->asArray()->all(), 'id',  'deskripsi'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih TAX...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
                <?= $form->field($model, 'VALID_FROM')->widget(DatePicker::className(), [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd', 'target' => '_Blank']
                ]);
                ?>

                <?= $form->field($model, 'VALID_TO')->widget(DatePicker::className(), [
                    'pluginOptions' => ['autoclose' => true, 'format' => 'yyyy-mm-dd', 'target' => '_Blank']
                ]);
                ?>

                <?= $form->field($model, 'CREATE_BY')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <?= $form->field($model, 'CREATE_DATE')->textInput(['readOnly' => true]) ?>

                <?= $form->field($model, 'LAST_UPDATE_BY')->textInput(['maxlength' => true, 'readOnly' => true]) ?>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>