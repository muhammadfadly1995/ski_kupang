<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\DataTruck */

$this->title = 'Create Data Truck';
$this->params['breadcrumbs'][] = ['label' => 'Data Trucks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="data-truck-create">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
