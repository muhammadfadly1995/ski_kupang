<?php

use common\models\Supir;
use common\models\TableExpeditur;
use common\models\TypeTruck;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\DataTruck */

$this->title = $model->no_pol;
$this->params['breadcrumbs'][] = ['label' => 'Data Trucks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="box">
    <div class="box-body">
        <div class="data-truck-view">

            <h3><?= Html::encode($this->title) ?></h3>

            <p>
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        //'id',
                        [
                            'attribute' => 'id_expeditur',
                            'label'=>'Expeditur Truck',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                               $dataexpeditur=TableExpeditur::find()->where(['id'=>$model->id_expeditur])->one();
                               if($dataexpeditur){
                                $expeditur=$dataexpeditur->code_expeditur.'/'.$dataexpeditur->deskripsi;
                               }else{
                                $expeditur='Data Tidak Ditemukan';
                               }
                                return $expeditur;
                            }
                        ],
                        [
                            'attribute' => 'id_supir',
                            'label'=>'Supir Truck',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                               $datasupir=Supir::find()->where(['id'=>$model->id_supir])->one();
                               if($datasupir){
                                $supir=$datasupir->nama_supir;
                               }else{
                                $supir='Data Tidak Ditemukan';
                               }
                                return $supir;
                            }
                        ],
                        'no_pol',
                        [
                            'attribute' => 'Tipe Truck',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                                $datatipe=TypeTruck::find()->where(['id'=>$model->id_type_truck])->one();
                                return $datatipe->code .' '.$datatipe->deskripsi;
                            }
                        ],
                      //  'id_type_truck',
                        'merek_truck',
                        'no_rfid',
                        'tahun_pembuatan',
                        'jumlah_ban',
                        'no_stnk',
                       
                        
                        // 'delete_mark',
                    ],
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'kapasitas_angkut',
                            //'label'=>'Tipe Truck',
                            'contentOptions' => ['class' => 'text-left'],
                            'value' => function ($model) {
                               
                                return $model->kapasitas_angkut .' '.$model->satuan;
                            }
                        ],
                        'tanggal_berlaku_stnk',
                        'no_mesin',
                        'no_rangka',
                        'create_by',
                        'create_date',
                        'update_by',
                        'update_date',
                        // 'delete_mark',
                    ],
                ]) ?>
            </div>
            <div class="col-md-12">
                <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
            </div>

        </div>
    </div>
</div>