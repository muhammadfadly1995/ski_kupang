<?php

use common\models\PtMasterDistrikSki;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\export\ExportMenu;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel common\models\RfcZZcsdShiptoSkiSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
 
$this->title = 'Master Customer';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzapp-select-sysplan-ski-index">

            <h3><?= Html::encode($this->title) ?></h3>
            <ul class="nav nav-tabs pull-right">
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/rfc-z-zcsd-shipto-ski']) ?>"> Data Soldto</a>
                <a class="btn btn-warning btn-sm" href="<?= Url::toRoute(['/child-sholdto']) ?>"> Data Shipto</a>
              

            </ul>
           

            <?php // echo $this->render('_search', ['model' => $searchModel]); 
            ?>
            <?php
            $gridColumns = [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'No'],
           
                // 'ID',
                // 'ID_SHOLDTO',
                'KODE_SHIPTO',
                'NAMA_SHIPTO',
                'ALAMAT:ntext',
                //'DISTRIK_CODE',
                'PHONE',
                'EMAIL:email',
                //'CREATE_BY',
                //'UPDATE_BY',
                //'UPDATE_DATE',

              //  ['class' => 'yii\grid\ActionColumn'],
            ];

            echo ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'clearBuffers' => true, //optional
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => $gridColumns
            ]);
            ?>



        </div>
    </div>
</div>