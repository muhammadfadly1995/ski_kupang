<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ChildSholdto */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="child-sholdto-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ID_SHOLDTO')->textInput() ?>

    <?= $form->field($model, 'KODE_SHIPTO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'NAMA_SHIPTO')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ALAMAT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'DISTRIK_CODE')->textInput() ?>

    <?= $form->field($model, 'PHONE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'EMAIL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'CREATE_BY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UPDATE_BY')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'UPDATE_DATE')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
