<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZcsdShiptoSki */

$this->title = 'Ubah Data Master Customer: ' . $model->NAMA_SOLDTO;
$this->params['breadcrumbs'][] = ['label' => 'Data Customer', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->ID, 'url' => ['view', 'id' => $model->ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rfc-zzcsd-shipto-ski-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
