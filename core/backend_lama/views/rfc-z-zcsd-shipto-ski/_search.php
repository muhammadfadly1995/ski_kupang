<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RfcZZcsdShiptoSkiSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rfc-zzcsd-shipto-ski-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'KODE_SOLDTO') ?>

    <?= $form->field($model, 'NAMA_SOLDTO') ?>

    <?= $form->field($model, 'KODE_SHIPTO') ?>

    <?= $form->field($model, 'NAMA_SHIPTO') ?>

    <?php // echo $form->field($model, 'ALAMAT') ?>

    <?php // echo $form->field($model, 'KODE_DSITRIK') ?>

    <?php // echo $form->field($model, 'NAMA_DISTRIK') ?>

    <?php // echo $form->field($model, 'CREATE_BY') ?>

    <?php // echo $form->field($model, 'CREATE_DATE') ?>

    <?php // echo $form->field($model, 'LAST_UPDATE_BY') ?>

    <?php // echo $form->field($model, 'LAST_UPDATE_DATE') ?>

    <?php // echo $form->field($model, 'DELETE_MARK') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
