<?php

use kartik\money\MaskMoney;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model common\models\RfcZZcsdShiptoSki */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="box">
    <div class="box-body">
        <div class="rfc-zzcsd-shipto-ski-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="col-md-6">
                <?= $form->field($model, 'KODE_SOLDTO')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'NAMA_PEMILIK')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'NAMA_SOLDTO')->textInput(['maxlength' => true]) ?>
                <?= $form->field($model, 'NILAI_JAMINAN')->widget(
                    MaskMoney::classname(),
                    [
                        'options' => [
                            //  'id' => 'harga-barang',
                            'onchange' => "myFunction()"
                        ],
                        'pluginOptions' => [
                            'prefix' => 'Rp. ',
                            'suffix' => '',
                            'affixesStay' => TRUE,
                            'precision' => 0,
                            'allowZero' => false,
                            'allowNegative' => false,

                        ],
                    ]
                )->label('Nilai Jaminan ');
                ?>

                <?= $form->field($model, 'KODE_DSITRIK')->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(common\models\PtMasterDistrikSki::find()->asArray()->all(), 'ID', 'KODE_DISTRIK', 'DISTRIK'),
                    'language' => 'eng',
                    'options' => ['placeholder' => 'Pilih Distrik...'],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);
                ?>
                <?= $form->field($model, 'CREATE_BY')->textInput(['maxlength' => true, 'readOnly' => true]) ?>



            </div>
            <div class="col-md-6">



                <?= $form->field($model, 'CREATE_DATE')->textInput(['readOnly' => true]) ?>

                <?= $form->field($model, 'LAST_UPDATE_BY')->textInput(['maxlength' => true, 'readOnly' => true]) ?>

                <?= $form->field($model, 'ALAMAT')->textarea(['rows' => 6]) ?>
            </div>

            <div class="col-md-12">
                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    <?= Html::a('Kembali', ['index'], ['class' => 'btn btn-default']) ?>
                </div>
            </div>


            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>