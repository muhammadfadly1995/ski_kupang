<?php

namespace backend\controllers;

use common\models\DataPo;
use common\models\StagingStock;
use Yii;
use common\models\TimbanganIncoming;
use common\models\TimbanganIncomingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TimbanganIncomingController implements the CRUD actions for TimbanganIncoming model.
 */
class TimbanganIncomingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TimbanganIncoming models.
     * @return mixed
     */
     
     public function actionBackgroundJob(){
         $data=\common\models\TimbanganIncoming::find()->orderBy('id DESC')->one();
         $no=25;
         for ($no=1; $no < 25; $no++ ){
             $model= new \common\models\TimbanganIncoming();
             $model->no_spj=$data->no_spj;
              $model->no_pol=$data->no_pol;
               $model->id_po=$data->id_po;
                $model->tanggal_masuk=$data->tanggal_masuk;
                 $model->tanggal_keluar=$data->tanggal_keluar;
                  $model->jam_masuk=$data->jam_masuk;
                   $model->jam_keluar=$data->jam_keluar;
                    $model->nilai_masuk=$data->nilai_masuk;
                     $model->nilai_keluar=$data->nilai_keluar;
                      $model->subtotal=$data->subtotal;
                       $model->qty_po=$data->qty_po;
                        $model->create_by=$data->create_by;
                         $model->update_by=$data->update_by;
                          $model->delete_mark=$data->delete_mark;
                           $model->line_order=$data->line_order;
                           $model->save(false);
         }
     }
     
    public function actionIndexIn()
    {
        $searchModel = new TimbanganIncomingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index-in', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCetakSpj($id){
        $datatimbangan=TimbanganIncoming::find()->where(['id'=>$id])->one();
        $datapo=DataPo::find()->where(['id'=>$datatimbangan->id_po])->one();
        return $this->renderAjax('cetak-spj', [
          'datatimbangan'=>$datatimbangan,
          'datapo'=>$datapo,
        ]);
    }
    public function actionIndex()
    {
        $searchModel = new TimbanganIncomingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIn()
    {
        $searchModel = new TimbanganIncomingSearch();
        $dataProvider = $searchModel->searchin(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TimbanganIncoming model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionPilihData($id, $idbill)
    {
        $model = new DataPo();
        if ($model->load(Yii::$app->request->post())) {
            $cekdata=DataPo::find()->where(['id'=>$id])->one();
            $cekdata->sisa_qty=$cekdata->sisa_qty-$model->sisa_qty;
            $dataincoming = TimbanganIncoming::find()->where(['id' => $idbill])->one();
            $dataincoming->id_po = $id;
            $dataincoming->qty_po=$model->sisa_qty;
            $cekdata->save(false);
            $dataincoming->save(false);
            return $this->redirect(['view-in', 'id' => $idbill]);
        }


        return $this->renderAjax('tambah-quantity', [
            'model' => $model,
        ]);
    }
    public function actionCancelData($id, $idbill)
    {
        $dataincoming = TimbanganIncoming::find()->where(['id' => $idbill])->one();
        $datapo=DataPo::find()->where(['id'=>$id])->one();
        $datapo->sisa_qty=$datapo->sisa_qty+$dataincoming->qty_po;
        $dataincoming->id_po = 0;
        $dataincoming->qty_po=0;
        $datapo->save(false);
        $dataincoming->save(false);
        return $this->redirect(['view-in', 'id' => $idbill]);
    }
    public function actionViewIn($id)
    {
        $modelpo = new DataPo();
        $model = $this->findModel($id);
        if ($modelpo->load(Yii::$app->request->post())) {
            $modelpo->no_po1 = strtoupper($modelpo->no_po1);
            if ($modelpo->no_po1) {
                $datapo = DataPo::find()->where(['status' => 'Open'])->where(['no_po' => $modelpo->no_po1])->andWhere(['delete_mark' => 0])->all();
            } else {
                $datapo = DataPo::find()->where(['status' => 'Open'])->andWhere(['delete_mark' => 0])->all();
            }
            return $this->render('view-in', [
                'modelpo' => $modelpo,
                'model' => $model,
                'datapo' => $datapo,
            ]);
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->nilai_keluar > 0) {
                if($model->nilai_keluar >= $model->nilai_masuk){
                    \Yii::$app->getSession()->setFlash('error', 'Nilai Timbangan Keluar Tidak Boleh Melebihin Nilai Timbangan Masuk');
                    return $this->redirect(['view-in', 'id' => $model->id]);
                }

                $datapo = DataPo::find()->where(['id' => $model->id_po])->one();
                $datastaging = StagingStock::find()->where(['id_plant' => $datapo->id_plant])->andWhere(['id_mat' => $datapo->material])->orderBy('id DESC')->one();
                if ($datastaging) {
                    $datastaging->qty = $datastaging->qty + ($model->nilai_masuk - $model->nilai_keluar);
                    $datastaging->last_update = date("Y-m-d h:i:sa");
                } else {
                    $datastaging = new StagingStock();
                    $datastaging->id_plant = $datapo->id_plant;
                    $datastaging->id_mat = $datapo->material;
                    $datastaging->qty = $model->nilai_masuk - $model->nilai_keluar;
                    $datastaging->last_update = date("Y-m-d h:i:sa");
                }
                if($datapo->sisa_qty=0){
                    $datapo->status = 'Close';
                }else{
                    $datapo->status = 'Open';
                }
               
                $model->subtotal = $model->nilai_masuk - $model->nilai_keluar;
                $datastaging->save(false);
                $datapo->save(false);
                $model->save(false);
                \Yii::$app->getSession()->setFlash('success', 'Timbangan Keluar Berhasil Diinput');
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                if ($model->nilai_masuk < 1) {
                    \Yii::$app->getSession()->setFlash('error', 'Nilai Timbangan Tidak Boleh 0');
                    return $this->redirect(['view-in', 'id' => $model->id]);
                }
                $datapo = DataPo::find()->where(['id' => $model->id_po])->one();
                $datapo->status = 'Timbangan Masuk';
                $datapo->save(false);
                $model->save(false);
                \Yii::$app->getSession()->setFlash('success', 'Timbangan Masuk Berhasil Diinput');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }
        if ($model->id_po > 0) {
            $datapo = DataPo::find()->where(['status' => 'Open'])->orWhere(['status'=>'Timbangan Masuk'])->andWhere(['id' => $model->id_po])->andWhere(['delete_mark' => 0])->all();
        } else {
            $datapo = DataPo::find()->where(['status' => 'Open'])->orWhere(['status'=>'Timbangan Masuk'])->andWhere(['>','sisa_qty',0])->andWhere(['delete_mark' => 0])->all();
        }


        return $this->render('view-in', [
            'modelpo' => $modelpo,
            'model' => $model,
            'datapo' => $datapo,
        ]);
    }

    /**
     * Creates a new TimbanganIncoming model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TimbanganIncoming();
        $model->delete_mark = 0;

        $model->create_by = \yii::$app->user->identity->username;
        //  $model->jam_keluar=date("H:i:s");

        if ($model->load(Yii::$app->request->post())) {
            $model->no_pol = strtoupper($model->no_pol);
            // print_r($model->no_pol);die();
            $cekdata = TimbanganIncoming::find()->where(['id_po' => $model->id_po])->orderBy('id DESC')->one();
            $cekdatanopol = TimbanganIncoming::find()->where(['no_pol' => $model->no_pol])->orderBy('id DESC')->one();

            if ($cekdatanopol && $cekdatanopol->nilai_keluar < 1) {

                //  $cekdata->nilai_keluar = $model->nilai_masuk;
                //  $cekdata->subtotal = $cekdata->nilai_masuk - $cekdata->nilai_keluar;
                $cekdatanopol->update_by = \yii::$app->user->identity->username;
                $cekdatanopol->jam_keluar = date("H:i:s");
                $cekdatanopol->tanggal_keluar = date('Y-m-d');
                // $datapo = DataPo::find()->where(['id' => $model->id_po])->orderBy('id DESC')->one();
                // $datapo->status = 'Close';
                // $datapo->save(false);
                $cekdatanopol->save(false);
                //  \Yii::$app->getSession()->setFlash('success', 'Timbangan Keluar Berhasil Diinput');
                return $this->redirect(['view-in', 'id' => $cekdatanopol->id]);
            } else {
                $cekdata = TimbanganIncoming::find()->where(['delete_mark' => 0])->orderBy('id DESC')->one();
                if ($cekdata) {
                    $linenumber = $cekdata->line_order + 1;
                    $model->line_order = $linenumber;
                    $model->no_spj = 'SPJI-0000000' . $model->line_order;
                } else {
                    $linenumber = 1;
                    $model->line_order = $linenumber;
                    $model->no_spj = 'SPJI-0000000' . $model->line_order;
                }
                $model->delete_mark = 0;
                $model->jam_masuk = date("H:i:s");
                $model->tanggal_masuk = date('Y-m-d');
                $model->create_by = \yii::$app->user->identity->username;
                $model->update_by = \yii::$app->user->identity->username;
                // $datapo = DataPo::find()->where(['id' => $model->id_po])->orderBy('id DESC')->one();
                // $datapo->status = 'Timbangan Masuk';
                // $datapo->save(false);
            }

            $model->save(false);
            \Yii::$app->getSession()->setFlash('success', 'No SPJ Berhasil Digenerate');
            return $this->redirect(['view-in', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TimbanganIncoming model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TimbanganIncoming model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TimbanganIncoming model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TimbanganIncoming the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TimbanganIncoming::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
