<?php

namespace backend\controllers;

use common\models\ChildMatching;
use common\models\ChildOPpSki;
use common\models\ChildSholdto;
use common\models\DataTruck;
use common\models\MasterTax;
use common\models\OPpSki;
use Yii;
use common\models\TableMatching;
use common\models\TableMatchingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TableMatchingController implements the CRUD actions for TableMatching model.
 */
class TableMatchingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TableMatching models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TableMatchingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCancelDataTransaksi($id){
        $model=TableMatching::find()->where(['id'=>$id])->one();
        if ($model->load(Yii::$app->request->post())) {
            $datachil=ChildMatching::find()->where(['id_matching'=>$id])->all();
            foreach ($datachil as $key=>$val){
                $dataso=OPpSki::find()->where(['ID'=>$val->id_so])->one();
                $datachildso=ChildOPpSki::find()->where(['id'=>$val->id_child_so])->one();
                $dataso->status_op=2;
                $datachildso->sisa_quantity=$datachildso->sisa_quantity+$val->quantity;
                $val->delete_mark=1;
                $val->update_by=\yii::$app->user->identity->username;
                $val->save(false);
                $datachildso->save(false);
                $dataso->save(false);
            }
            $datatruck=DataTruck::find()->where(['id'=>$model->id_truck])->one();
            $datatruck->status_free=0;
            $model->delete_mark=1;
            $model->update_by=\yii::$app->user->identity->username;
            $model->update_date=date('Y-m-d');
            $model->time_update=date('H:i:s');
            $model->save(false);
            $datatruck->save(false);
            \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Dicancel');
            return $this->redirect(['cancel-transaksi']);
        }
        return $this->renderAjax('cancel-data', [
           'model'=>$model,
        ]);
    }
    public function actionCancelTransaksi()
    {
        $searchModel = new TableMatchingSearch();
        $dataProvider = $searchModel->searchCancel(Yii::$app->request->queryParams);

        return $this->render('index-cancel', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionIndexDone()
    {
        $searchModel = new TableMatchingSearch();
        $dataProvider = $searchModel->searchDone(Yii::$app->request->queryParams);

        return $this->render('index-done', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TableMatching model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $modelmat = new TableMatching();
        $datasearchso = null;
        $model2 = TableMatching::find()->where(['id' => $id])->one();
        $datachildmatching = ChildMatching::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $id])->all();
        $dataidchild = [];


        foreach ($datachildmatching as $key => $val) {
            $dataidchild[] = $val->id_so;
        }
        $cekdatachildmat=ChildMatching::find()->where(['id_matching'=>$id])->one();
        if($cekdatachildmat){
            $statusdis=$cekdatachildmat->id_so;
        }else{
            $statusdis=0;
        }
        $datachild = ChildOPpSki::find()->where(['id_pp' => $dataidchild])->andWhere(['delete_mark' => 0])->andWhere(['status_konfirmasi' => 'Approval'])->all();
        // print_r($datachild);die();
        if ($modelmat->load(Yii::$app->request->post())) {
            $datasearchso = ChildOPpSki::find()->where(['id_pp' => $modelmat->no_so])->andWhere(['delete_mark' => 0])->andWhere(['status_konfirmasi' => 'Approval'])->all();
            return $this->render('view', [
                'statusdis'=>$statusdis,
                'datasearchso' => $datasearchso,
                'modelmat' => $modelmat,
                'datachild' => $datachild,
                'model' => $this->findModel($id),
            ]);
        }

        return $this->render('view', [
            'statusdis'=>$statusdis,
            'datasearchso' => $datasearchso,
            'modelmat' => $modelmat,
            'datachild' => $datachild,
            'model' => $this->findModel($id),
        ]);
    }
    public function actionViewCetak($id)
    {
        $modelmat = new TableMatching();
        $datasearchso = null;
        $model2 = TableMatching::find()->where(['id' => $id])->one();
        $datachildmatching = ChildMatching::find()->andWhere(['id_matching' => $id])->all();
        $dataidchild = [];


        foreach ($datachildmatching as $key => $val) {
            $dataidchild[] = $val->id_so;
        }

        $datachild = ChildOPpSki::find()->where(['id_pp' => $dataidchild])->andWhere(['delete_mark' => 0])->andWhere(['status_konfirmasi' => 'Approval'])->all();
        // print_r($datachild);die();
        if ($modelmat->load(Yii::$app->request->post())) {
            $datasearchso = ChildOPpSki::find()->where(['id_pp' => $modelmat->no_so])->andWhere(['delete_mark' => 0])->andWhere(['status_konfirmasi' => 'Approval'])->all();
            return $this->render('view', [
                'datasearchso' => $datasearchso,
                'modelmat' => $modelmat,
                'datachild' => $datachild,
                'model' => $this->findModel($id),
            ]);
        }

        return $this->render('view-cetak', [
            'datasearchso' => $datasearchso,
            'modelmat' => $modelmat,
            'datachild' => $datachild,
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TableMatching model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionListregss($id = null)
    {
        if ($id) {
            $hitungdata = \common\models\DataTruck::find()
                ->where(['id_expeditur' => $id])->andWhere(['status_free'=>0])


                ->count();

            $regs = \common\models\DataTruck::find()
                ->where(['id_expeditur' => $id])
                ->andWhere(['status_free'=>0])
                ->orderBy([
                    'id' => SORT_ASC,
                ])
                ->all();


            if ($hitungdata > 0) {
                // $idsatuan=[];

                echo
                "<option value=" . 'Pilih . . .' . "'>" . 'Pilih . . .' . "</option>  <br/>";
                foreach ($regs as $reg) {
                    echo


                    "<option value='" . $reg->id . "'>" . $reg->no_pol . "</option>";
                }
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionCetakDo($id)
    {

        $datasearchso = null;
        $model2 = TableMatching::find()->where(['id' => $id])->one();
        $datachildmatching = ChildMatching::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $id])->all();
        $dataidchild = [];


        foreach ($datachildmatching as $key => $val) {
            $dataidchild[] = $val->id_so;
        }

        $datachild = ChildOPpSki::find()->where(['id_pp' => $dataidchild])->andWhere(['delete_mark' => 0])->andWhere(['status_konfirmasi' => 'Approval'])->all();
        // print_r($datachild);die();


        return $this->renderAjax('cetak', [
            'datasearchso' => $datasearchso,
            //   'modelmat' => $modelmat,
            'datachild' => $datachild,
            'model' => $this->findModel($id),
        ]);
    }
    public function actionInputQuantity($id, $idmat, $idso, $idchilmat)
    {
        $cekdatachild = ChildMatching::find()->where(['id_matching' => $idmat])->andWhere(['id_so' => $idso])->andWhere(['id_child_so' => $id])->one();
        if ($cekdatachild) {
            $model = ChildMatching::find()->where(['id' => $cekdatachild->id])->one();
            $dataqtyawal = $model->quantity;
            if ($model->load(Yii::$app->request->post())) {
                $datachilopp = ChildOPpSki::find()->where(['ID' => $id])->one();
                $datatotal = $datachilopp->sisa_quantity + $dataqtyawal;
                $datachilopp->sisa_quantity = $datachilopp->sisa_quantity + $dataqtyawal;
                $datachilopp->sisa_quantity = $datachilopp->sisa_quantity - $model->quantity;
                if ($datatotal < $model->quantity) {
                    \Yii::$app->getSession()->setFlash('error', 'Sisa Quantity Tidak Mencukupi');
                    return $this->redirect(['view', 'id' => $idmat]);
                }
                $datachilopp->save(false);
                $model->update_by = \yii::$app->user->identity->username;
                $model->update_date = date('Y-m-d');
                $model->save(false);
                \Yii::$app->getSession()->setFlash('success', 'Data DO berhasil Digenerate');
                return $this->redirect(['view', 'id' => $idmat]);
            }
        } else {
            $model = new ChildMatching();
            if ($model->load(Yii::$app->request->post())) {
                $datachilopp = ChildOPpSki::find()->where(['ID' => $id])->one();
                if ($datachilopp->sisa_quantity < $model->quantity) {
                    \Yii::$app->getSession()->setFlash('error', 'Sisa Quantity Tidak Mencukupi');
                    return $this->redirect(['view', 'id' => $idmat]);
                }
                $datachilopp->sisa_quantity = $datachilopp->sisa_quantity - $model->quantity;
                $datachilopp->save(false);
                $cekdata = ChildMatching::find()->orderBy('id DESC')->one();
                // print_r($cekdata);die();
                if ($cekdata) {
                    $number = $cekdata->line_order + 1;
                    $model->line_order = $number;
                    $model->no_do = 'DO-00000000' . $number;
                } else {
                    $number = 1;
                    $model->line_order = $number;
                    $model->no_do = 'DO-00000000' . $number;
                }
                $model->harga = $datachilopp->harga;
                $model->id_tax=$datachilopp->id_tax;
                $mastertax=MasterTax::find()->where(['id'=>$datachilopp->id_tax])->one();
                $model->subtotal=((($mastertax->nominal_tax/100)*$model->harga)+$model->harga)*$model->quantity;
               
                $model->id_matching = $idmat;
                $model->id_so = $idso;
                $model->id_child_so = $id;
                $model->status = 0;
                $model->create_by = \yii::$app->user->identity->username;
                $model->create_date = date('Y-m-d');
                $model->update_by = \yii::$app->user->identity->username;
                $model->update_date = date('Y-m-d');
                $model->save(false);

                \Yii::$app->getSession()->setFlash('success', 'Data DO berhasil Digenerate');
                return $this->redirect(['view', 'id' => $idmat]);
            }
        } 

        return $this->renderAjax('tambah-quantity', [
            'model' => $model,
        ]);
    }
    public function actionSelesaiDo($id)
    {

        $matchingdo = TableMatching::find()->where(['id' => $id])->one();
        $childmatching = ChildMatching::find()->where(['id_matching'=>$matchingdo->id])->all();
        foreach ($childmatching as $key => $val) {
            $dataso = ChildOPpSki::find()->where(['id_pp'=>$val->id_so])->sum('sisa_quantity');
            if ($dataso == 0) {
                $datapp = OPpSki::find()->where(['ID' => $val->id_so])->one();
                $datapp->status_op = 3;
                $datapp->save(false);
            }
        }
        $matchingdo->status_selesai = 1;
        $matchingdo->status_matching = 20; //20  antri, 30 timbangan masuk, 50 timbangan keluar, 70 matching selesai
        $matchingdo->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Matching Data Selesai Dilakukan');
        return $this->redirect(['view-cetak', 'id' => $id]);
    }
    public function actionCreate()
    {
        $model = new TableMatching();
        $model->create_by = \yii::$app->user->identity->username;
        $model->create_date = date('Y-m-d');
        $model->time_create = date("h:i:sa");
        $model->update_by = \yii::$app->user->identity->username;
        $model->update_date = date('Y-m-d');
        $model->time_update = date("h:i:sa");
        $model->delete_mark = 0;
        if ($model->load(Yii::$app->request->post())) {
            
            $cekdata = TableMatching::find()->orderBy('id DESC')->one();
            if ($cekdata) {
                $model->line_number = $cekdata->line_number + 1;
                $model->no_matching = 'MC-00000000' . $model->line_number;
            } else {
                $model->line_number = 1;
                $model->no_matching = 'MC-00000000' . $model->line_number;
            }
            $datatruck=DataTruck::find()->where(['id'=>$model->id_truck])->one();
            $datatruck->status_free=1;
            $model->save(false);
            $datatruck->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TableMatching model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TableMatching model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TableMatching model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TableMatching the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TableMatching::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
