<?php

namespace backend\controllers;

use Yii;
use common\models\DataPo;
use common\models\DataPoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * DataPoController implements the CRUD actions for DataPo model.
 */
class DataPoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all DataPo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new DataPoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DataPo model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DataPo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionListregss($id = null)
    {
        if ($id) {
            $hitungdata = \common\models\DataTruck::find()
                ->where(['id_expeditur' => $id])


                ->count();

            $regs = \common\models\DataTruck::find()
                ->where(['id_expeditur' => $id])

                ->orderBy([
                    'id' => SORT_ASC,
                ])
                ->all();


            if ($hitungdata > 0) {
                // $idsatuan=[];

                echo
                "<option value=" . 'Pilih . . .' . "'>" . 'Pilih . . .' . "</option>  <br/>";
                foreach ($regs as $reg) {
                    echo


                    "<option value='" . $reg->no_pol . "'>" . $reg->no_pol . "</option>";
                }
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    public function actionCreate()
    {
        $model = new DataPo();
        $model->status='Open';
        $model->delete_mark=0;
        $model->update_date=date("Y-m-d H:i:s");
        $model->update_by=\yii::$app->user->identity->username;
        $model->create_date=date("Y-m-d H:i:s");
        $model->create_by=\yii::$app->user->identity->username;
        $model->id_com=9000;
        if ($model->load(Yii::$app->request->post()) ) {
            $model->sisa_qty=$model->qty;
            $cekdata = DataPo::find()->orderBy('id DESC')->one();
            if ($cekdata) {
                $model->line_number = $cekdata->line_number + 1;
               // $model->no_po = 'PO-00000000' . $model->line_number;
            } else {
                $model->line_number = 1;
              //  $model->no_po = 'PO-00000000' . $model->line_number;
            }
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DataPo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DataPo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the DataPo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DataPo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DataPo::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
