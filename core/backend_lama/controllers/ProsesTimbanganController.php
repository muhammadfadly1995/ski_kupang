<?php

namespace backend\controllers;

use common\models\ChildMatching;
use common\models\ChildOPpSki;
use common\models\DataTruck;
use common\models\OPpSki;
use Yii;
use common\models\ProsesTimbangan;
use common\models\ProsesTimbanganSearch;
use common\models\TableMatching;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProsesTimbanganController implements the CRUD actions for ProsesTimbangan model.
 */
class ProsesTimbanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProsesTimbangan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProsesTimbanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ProsesTimbangan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCetakSpj($id = null, $idmat, $status)
    {
        $model2 = TableMatching::find()->where(['no_matching' => $idmat])->one();
        $datachildmatching = ChildMatching::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $model2->id])->all();
        $dataidchild = [];


        foreach ($datachildmatching as $key => $val) {
            $dataidchild[] = $val->id_so;
        }

        $datachild = ChildOPpSki::find()->where(['id_pp' => $dataidchild])->andWhere(['delete_mark' => 0])->andWhere(['status_konfirmasi' => 'Approval'])->all();
        $datachildsoldto = ChildOPpSki::find()->where(['id_pp' => $dataidchild])->andWhere(['delete_mark' => 0])->andWhere(['status_konfirmasi' => 'Approval'])->orderBy('id DESC')->one();

        $sumdatachild = ChildMatching::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $model2->id])->sum('quantity');
        $model = ProsesTimbangan::find()->where(['id' => $id])->one();
        return $this->renderAjax('cetak-spj', [
            'datachildsoldto' => $datachildsoldto,
            'idmat' => $idmat,
            'id' => $id,
            'status' => $status,
            'model2' => $model2,
            'datachild' => $datachild,
            'model' => $model,
        ]);
    }
    public function actionView($id = null, $idmat, $status)
    {
        //  print_r($status);die();
        $model2 = TableMatching::find()->where(['no_matching' => $idmat])->one();
        $datachildmatching = ChildMatching::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $model2->id])->all();
        $dataidchild = [];
        $datachildmatching1 = ChildMatching::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $model2->id])->one();
        $datapp = OPpSki::find()->where(['ID' => $datachildmatching1->id_so])->one();
        // print_r($datachildmatching1);die();

        foreach ($datachildmatching as $key => $val) {
            $dataidchild[] = $val->id_so;
        }

        $datachild = ChildOPpSki::find()->where(['id_pp' => $dataidchild])->andWhere(['delete_mark' => 0])->andWhere(['status_konfirmasi' => 'Approval'])->all();
        $sumdatachild = ChildMatching::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $model2->id])->sum('quantity');

        if ($id != null) {
            $model = ProsesTimbangan::find()->where(['id' => $id])->one();
            if ($status == 'timbangankeluar') {
                $model->nilai_keluar = null;
            }
        } else {
            $model = new ProsesTimbangan();
            $model->id_matching = $model2->id;
            $model->create_by = \yii::$app->user->identity->username;
            $model->create_date = date('Y-m-d');
            $model->update_by = yii::$app->user->identity->username;
            $model->update_date = date('Y-m-d');
            $model->tanggal_masuk = date('Y-m-d');
            $model->jam_masuk = date('H:i:s');
            $model->delete_mark = 0;
            $model->total_nilai = $sumdatachild;
            $model->id_soldto = $datapp->id_soldto;
            $model->id_c_pp = $datachildmatching1->id_so;
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($status == 'timbangankeluar') {
                $cekdata = ProsesTimbangan::find()->where(['delete_mark' => 0])->andWhere(['id_matching' => $model2->id])->orderBy('id DESC')->one();
                $cekdata->tanggal_keluar = date('Y-m-d');
                $cekdata->jam_keluar = date('H:i:s');
                $cekdata->update_by = \yii::$app->user->identity->username;
                $cekdata->update_date = date('Y-m-d');
                $cekdata->nilai_keluar = $model->nilai_keluar;
                $cekdata->nilai_akhir = $model->nilai_keluar - $cekdata->nilai_masuk;
                $datamatching = TableMatching::find()->where(['id' => $model2->id])->one();
                $datatruck = DataTruck::find()->where(['id' => $datamatching->id_truck])->one();
                $datatruck->status_free = 0;
                $datamatching->status_matching = 50;
                $datamatching->save(false);
                $cekdata->save(false);
                $datatruck->save(false);
                \Yii::$app->getSession()->setFlash('success', 'Timbangan Keluar Selesai Dilakukan');
                return $this->redirect(['view', 'idmat' => $idmat, 'id' => $model->id, 'status' => 'timbanganmasuk']);
            }
            $id = null;
            $cekdata = ProsesTimbangan::find()->where(['delete_mark' => 0])->orderBy('id DESC')->one();
            if ($cekdata) {
                $linenumber = $cekdata->line_number + 1;
                $model->line_number = $linenumber;
                $model->no_spj = 'SPJ-0000000' . $model->line_number;
            } else {
                $linenumber = 1;
                $model->line_number = $linenumber;
                $model->no_spj = 'SPJ-0000000' . $model->line_number;
            }
            $datamatching = TableMatching::find()->where(['id' => $model2->id])->one();
            $datamatching->status_matching = 30;
            $datamatching->save(false);
            $model->save(false);
            \Yii::$app->getSession()->setFlash('success', 'SPJ Berhasil Digenerate');

            return $this->redirect(['view', 'idmat' => $idmat, 'id' => $model->id, 'status' => $status]);
        }
        return $this->render('view', [
            'idmat' => $idmat,
            'id' => $id,
            'status' => $status,
            'model2' => $model2,
            'datachild' => $datachild,
            'model' => $model,
        ]);
    }

    /**
     * Creates a new ProsesTimbangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCancelTransaksi()
    {
        $searchModel = new ProsesTimbanganSearch();
        $dataProvider = $searchModel->searchcancel(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCancelDataTransaksi($id)
    {
        $model = ProsesTimbangan::find()->where(['id' => $id])->one();
        if ($model->load(Yii::$app->request->post())) {

            $datamatching = TableMatching::find()->where(['id' => $model->id_matching])->one();
            $datatruck = DataTruck::find()->where(['id' => $datamatching->id_truck])->one();
            $datamatching->status_matching=20;
            $datatruck->status_free = 0;
            $model->delete_mark = 1;
            $model->update_by = \yii::$app->user->identity->username;
            $model->update_date = date('Y-m-d');
           // $model->time_update = date('H:i:s');
            $datamatching->save(false);
            $model->save(false);
          //  $datatruck->save(false);
            \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Dicancel');
            return $this->redirect(['cancel-transaksi']);
        }
        return $this->renderAjax('cancel-transaksi', [
            'model' => $model,
        ]);
    }

    public function actionCreate()
    {

        $searchModel = new ProsesTimbanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $modelmat = new TableMatching();
        $model = new ProsesTimbangan();
        $model->create_by = \yii::$app->user->identity->username;
        $model->create_date = date('Y-m-d');
        $model->update_by = yii::$app->user->identity->username;
        $model->update_date = date('Y-m-d');
        $model->tanggal_masuk = date('Y-m-d');
        $model->jam_masuk = date('H:i:s');
        $model->delete_mark = 0;
        if ($modelmat->load(Yii::$app->request->post())) {
            $cekdata = TableMatching::find()->where(['no_matching' => $modelmat->no_matching])->andWhere(['status_selesai' => 1])->andWhere(['delete_mark' => 0])->andWhere(['>', 'status_matching', 0])->orderBy('id DESC')->one();
            if (!$cekdata) {
                \Yii::$app->getSession()->setFlash('error', 'No Matching Tidak Ditemukan, Atau Belum Selesai Di Matching');
                return $this->redirect(['create']);
            } else {
                if ($cekdata->status_matching == 50) {
                    \Yii::$app->getSession()->setFlash('error', 'No Matching Sudah Timbangan Keluar');
                    return $this->redirect(['create']);
                }
                $cekdatatimbangan = ProsesTimbangan::find()->where(['id_matching' => $cekdata->id])->andWhere(['nilai_keluar' => 0])->andWhere(['delete_mark' => 0])->one();
                if ($cekdatatimbangan) {
                    if ($cekdatatimbangan->nilai_akhir > 0) {
                        \Yii::$app->getSession()->setFlash('error', 'No Matching Telah Selesai Melakukan Timbangan Keluar');
                        return $this->redirect(['create']);
                    }
                    $id = $cekdatatimbangan->id;
                    $status = 'timbangankeluar';
                } else {
                    $id = null;
                    $status = 'timbanganmasuk';
                }
            }


            return $this->redirect(['view', 'idmat' => $modelmat->no_matching, 'id' => $id, 'status' => $status]);
        }
        // if ($model->load(Yii::$app->request->post()) && $model->save()) {
        //     return $this->redirect(['view', 'id' => $model->id]);
        // }

        return $this->render('create', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'modelmat' => $modelmat,
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ProsesTimbangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ProsesTimbangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ProsesTimbangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProsesTimbangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProsesTimbangan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
