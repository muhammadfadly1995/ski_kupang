<?php

namespace backend\controllers;

use common\models\RfcZZcsdListMatSalesSki;
use Yii;
use common\models\StockLapangan;
use common\models\StockLapanganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * StockLapanganController implements the CRUD actions for StockLapangan model.
 */
class StockLapanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all StockLapangan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StockLapanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionListregssmat($id = null)
    {
        if ($id) {
            $hitungdata = \common\models\RfcZZcsdListMatSalesSki::find()
                ->where(['ID' => $id])


                ->count();

            $regs = \common\models\RfcZZcsdListMatSalesSki::find()
                ->where(['ID' => $id])

                ->orderBy([
                    'ID' => SORT_ASC,
                ])
                ->all();


            if ($hitungdata > 0) {
                // $idsatuan=[];


                foreach ($regs as $reg) {
                    echo


                    "<option value='" . $reg->UOM . "'>" . $reg->UOM . "</option>";
                }
            } 
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    /**
     * Displays a single StockLapangan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new StockLapangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new StockLapangan();
        $model->create_by=\yii::$app->user->identity->username;
        $model->create_date=date('Y-m-d');
        $model->update_by=\yii::$app->user->identity->username;
        $model->update_date=date('Y-m-d');
        $model->delete_mark=0;
      

        if ($model->load(Yii::$app->request->post()) ) {
            $cekdatamaterial=RfcZZcsdListMatSalesSki::find()->where(['ID'=>$model->id_mat])->one();
            $model->satuan=$cekdatamaterial->UOM;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing StockLapangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            $model->update_by=\yii::$app->user->identity->username;
            $model->update_date=date('Y-m-d');
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing StockLapangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete_mark=1;
        $model->update_by=\yii::$app->user->identity->username;
        $model->update_date=date('Y-m-d');
        $model->save(false);
        return $this->redirect(['index']);
    }

    /**
     * Finds the StockLapangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return StockLapangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = StockLapangan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
