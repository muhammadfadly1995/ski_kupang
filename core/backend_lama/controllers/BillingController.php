<?php

namespace backend\controllers;

use Yii;
use common\models\Billing;
use common\models\BillingSearch;
use common\models\ChildBilling;
use common\models\ChildMatching;
use common\models\ChildOPpSki;
use common\models\OPpSki;
use common\models\ProsesTimbangan;
use common\models\TableMatching;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BillingController implements the CRUD actions for Billing model.
 */
class BillingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Billing models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BillingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCancelDataTransaksi($id)
    {
        $model = Billing::find()->where(['id' => $id])->one();
        if ($model->load(Yii::$app->request->post())) {
            $datachildbilling = ChildBilling::find()->where(['id_billing' => $model->id])->all();
            foreach ($datachildbilling as $key => $val) {
                $datatimbangan = ProsesTimbangan::find()->where(['id' => $val->no_spj])->one();
                $datatimbangan->status_billing = 0;
                $datatimbangan->save(false);
            }
            $model->delete_mark = 1;
            $model->update_by = \yii::$app->user->identity->username;
            $model->update_date = date('Y-m-d');
           // $model->time_update = date('H:i:s');
            $model->save(false);

            \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Dicancel');
            return $this->redirect(['cancel-transaksi']);
        }
        return $this->renderAjax('cancel-data', [
            'model' => $model,
        ]);
    }
    public function actionCancelTransaksi()
    {
        $searchModel = new BillingSearch();
        $dataProvider = $searchModel->searchcancel(Yii::$app->request->queryParams);

        return $this->render('index-cancel', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Displays a single Billing model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCetakData($id)
    {


        $modelbilling = new Billing();
        $childbilling = ChildBilling::find()->where(['id_billing' => $id])->andWhere(['delete_mark' => 0])->all();


        return $this->renderAjax('cetak-data', [
            'modelbilling' => $modelbilling,
            'childbilling' => $childbilling,
            //  'model2'=>$model2,
            'model' => $this->findModel($id),
            // 'sumdatachild'=>$sumdatachild,
            // 'datachild'=>$datachild,
        ]);
    }

    public function actionSimpanBilling($id)
    {
        $databilling = Billing::find()->where(['id' => $id])->one();
        $datachild = ChildBilling::find()->where(['id_billing' => $id])->all();
        foreach ($datachild as $key => $val) {
            $datatimbangan = ProsesTimbangan::find()->where(['id' => $val->no_spj])->one();
            $datatimbangan->status_billing = 1;
            $datatimbangan->save(false);
        }
        $databilling->status_billing = 1;
        $databilling->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Data Billing Berhasil Di Simpan');

        return $this->redirect(['view', 'id' => $id]);
    }
    public function actionSimpanData($id)
    {
        $databilling = Billing::find()->where(['id' => $id])->one();
        $databilling->status_billing = 1;
        $databilling->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Data Billing Berhasil Di Simpan');

        return $this->redirect(['view', 'id' => $id]);
    }
    public function actionPilihData($id, $idbill)
    {
        $datatimbangan = ProsesTimbangan::find()->where(['id' => $id])->one();
        $datapp = OPpSki::find()->where(['ID' => $datatimbangan->id_soldto])->one();
        $sumnominal = ChildOPpSki::find()->where(['id_pp' => $datatimbangan->id_c_pp])->andWhere(['delete_mark' => 0])->sum('subtotal');
        $databilling = Billing::find()->where(['id' => $idbill])->one();
        $databilling->subtotal = $databilling->subtotal + $sumnominal;

        $databilling->save(false);
        $datachildbilling = new ChildBilling();
        $datachildbilling->id_billing = $idbill;
        $datachildbilling->no_spj = $datatimbangan->id;
        $datachildbilling->update_by = \yii::$app->user->identity->username;
        $datachildbilling->update_date = date('Y-m-d');
        $datachildbilling->create_by = \yii::$app->user->identity->username;
        $datachildbilling->create_date = date('Y-m-d');
        $datachildbilling->subtotal = $sumnominal;
        $datachildbilling->save(false);
        \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Dipilih');
        return $this->redirect(['view', 'id' => $idbill]);
    }
    public function actionCancelData($id, $idbill)
    {
        $datatimbangan = ProsesTimbangan::find()->where(['id' => $id])->one();
        $datapp = OPpSki::find()->where(['ID' => $datatimbangan->id_soldto])->one();
        $sumnominal = ChildOPpSki::find()->where(['id_soldto' => $datatimbangan->id_soldto])->andWhere(['delete_mark' => 0])->sum('subtotal');
        $databilling = Billing::find()->where(['id' => $idbill])->one();
        $databilling->subtotal = $databilling->subtotal - $sumnominal;

        $databilling->save(false);
        $datachildbilling = ChildBilling::find()->where(['id_billing' => $idbill])->andWhere(['no_spj' => $datatimbangan->id])->orderBy('id DESC')->one();
        // print_r($idbill);die();
        $datachildbilling->delete();

        \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Dihapus');
        return $this->redirect(['view', 'id' => $idbill]);
    }
    public function actionView($id)
    {

        $modelbilling = new Billing();
        $modelbilling2 = new Billing();
        $modelbilling2->tanggal_billing = date('Y-m-d');
        $childbilling = ChildBilling::find()->where(['id_billing' => $id])->andWhere(['delete_mark' => 0])->all();
        $idspj = [];
        foreach ($childbilling as $key => $val) {
            $idspj[] = $val->no_spj;
        }
        $databilling = Billing::find()->where(['id' => $id])->one();
        if ($databilling->status_billing == 1) {
            $dataspj = ProsesTimbangan::find()->where(['status_billing' => 1])->andWhere(['id' => $idspj])->andWhere(['>', 'nilai_keluar', 0])->all();
        } else {
            $cekchildbilling = ChildBilling::find()->where(['id_billing' => $id])->one();
            if ($cekchildbilling) {
                $dataspj = ProsesTimbangan::find()->where(['id' => $idspj])->andWhere(['>', 'nilai_keluar', 0])->all();
            } else {
                $dataspj =  ProsesTimbangan::find()->where(['status_billing' => 0])->andWhere(['>', 'nilai_keluar', 0])->all();
            }
        }
        if (Yii::$app->request->post('simpan') == 'true') {
            $modelbilling2->load(Yii::$app->request->post());
            $databilling = Billing::find()->where(['id' => $id])->one();
            $datachild = ChildBilling::find()->where(['id_billing' => $id])->all();
            foreach ($datachild as $key => $val) {
                $datatimbangan = ProsesTimbangan::find()->where(['id' => $val->no_spj])->one();
                $datatimbangan->status_billing = 1;
                $datatimbangan->save(false);
            }
            $databilling->status_billing = 1;
            $databilling->id_top = $modelbilling2->id_top;
            $databilling->tanggal_billing = $modelbilling2->tanggal_billing;
            $databilling->save(false);
            // $modelbilling2->save(false);
            \Yii::$app->getSession()->setFlash('success', 'Data Billing Berhasil Di Simpan');

            return $this->redirect(['view', 'id' => $id]);
        }
        if (Yii::$app->request->post('search') == 'true') {
            $modelbilling->load(Yii::$app->request->post());
            if ($modelbilling->cekbilling) {
                $dataspj = ProsesTimbangan::find()->where(['status_billing' => 0])->andWhere(['id_soldto' => $modelbilling->cekbilling])->andWhere(['>', 'nilai_keluar', 0])->all();
            } else {
                $dataspj = ProsesTimbangan::find()->where(['status_billing' => 0])->andWhere(['>', 'nilai_keluar', 0])->all();
            }
        }

        $cekchildbilling = ChildBilling::find()->where(['id_billing' => $id])->one();
        if ($cekchildbilling) {
            $datacekspj = ProsesTimbangan::find()->where(['id' => $cekchildbilling->no_spj])->one();
            $datasoldto = $datacekspj->id_soldto;
            //  $dataspj = ProsesTimbangan::find()->where(['status_billing' => 1])->andWhere(['id' => $idspj])->andWhere(['>', 'nilai_keluar', 0])->all();
        } else {
            $datasoldto = 0;
        }

        return $this->render('view', [
            'modelbilling2' => $modelbilling2,
            'datasoldto' => $datasoldto,
            'dataspj' => $dataspj,
            'modelbilling' => $modelbilling,
            'childbilling' => $childbilling,
            //  'model2'=>$model2,
            'model' => $this->findModel($id),
            // 'sumdatachild'=>$sumdatachild,
            // 'datachild'=>$datachild,
        ]);
    }

    /**
     * Creates a new Billing model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Billing();
        $model->create_by = \yii::$app->user->identity->username;
        $model->create_date = date('Y-m-d');
        $model->update_by = \yii::$app->user->identity->username;
        $model->update_date = date('Y-m-d');
        $cekdatabilling = Billing::find()->orderBy('id DESC')->one();
        $cekdescbilling = Billing::find()->where(['create_by' => \yii::$app->user->identity->username])->andWhere(['status_billing' => 0])->orderBy('id DESC')->one();
        if ($cekdescbilling) {
            \Yii::$app->getSession()->setFlash('error', 'Ada Billing Yang Belum Diselesaikan');
            return $this->redirect(['view', 'id' => $cekdescbilling->id]);
        }
        if ($cekdatabilling) {
            $line = $cekdatabilling->no_line + 1;
            $model->no_line = $line;
            $model->no_billing = 'B-00000000' . $model->no_line;
        } else {
            $model->no_line = 1;
            $model->no_billing = 'B-00000000' . $model->no_line;
        }
        $model->save(false);
        \Yii::$app->getSession()->setFlash('success', 'No Billing Berhasil Digenerate');
        return $this->redirect(['view', 'id' => $model->id]);
        // $model->save(false);
        $dataspj = ProsesTimbangan::find()->where(['status_billing' => 0])->all();
        // if ($model->load(Yii::$app->request->post())) {
        //     $cekdatabilling = Billing::find()->orderBy('id DESC')->one();

        //     if ($cekdatabilling) {
        //         $line = $cekdatabilling->no_line + 1;
        //         $model->no_line = $line;
        //         $model->no_billing = 'B-00000000' . $model->no_line;
        //     } else {
        //         $model->no_line = 1;
        //         $model->no_billing = 'B-00000000' . $model->no_line;
        //     }
        //     // $model->save(false);
        //     // $datachild = new ChildBilling();
        //     // $datachild->id_billing = $model->id;
        //     // $datachild->update_by = \yii::$app->user->identity->username;
        //     // $datachild->update_date = date('Y-m-d');
        //     // $datachild->create_by = \yii::$app->user->identity->username;
        //     // $datachild->create_date = date('Y-m-d');
        //     // $datachild->no_spj = $dataspj->id;
        //     // $datachild->save(false);
        //    // \Yii::$app->getSession()->setFlash('success', 'No Billing Berhasil Digenerate');

        //     return $this->redirect(['view', 'id' => $model->id]);
        // }

        return $this->render('create', [
            'dataspj' => $dataspj,
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Billing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Billing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Billing model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Billing the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Billing::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
