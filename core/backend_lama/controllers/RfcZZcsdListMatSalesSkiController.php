<?php

namespace backend\controllers;

use Yii;
use common\models\RfcZZcsdListMatSalesSki;
use common\models\RfcZZcsdListMatSalesSkiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RfcZZcsdListMatSalesSkiController implements the CRUD actions for RfcZZcsdListMatSalesSki model.
 */
class RfcZZcsdListMatSalesSkiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RfcZZcsdListMatSalesSki models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RfcZZcsdListMatSalesSkiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RfcZZcsdListMatSalesSki model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RfcZZcsdListMatSalesSki model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RfcZZcsdListMatSalesSki();
        $model->CREATE_BY=\yii::$app->user->identity->username;
        $model->CREATE_DATE=date('Y-m-d');
        $model->LAST_UPDATE_BY=\yii::$app->user->identity->username;
        $model->LAST_UPDATE_DATE=date('Y-m-d');
        $model->DELETE_MARK=0;
        $model->BERAT=1;
        if ($model->load(Yii::$app->request->post()) ) {

            $model->save(false);
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RfcZZcsdListMatSalesSki model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RfcZZcsdListMatSalesSki model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RfcZZcsdListMatSalesSki model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RfcZZcsdListMatSalesSki the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RfcZZcsdListMatSalesSki::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
