<?php

namespace backend\controllers;

use Yii;
use common\models\RfcZZappSelectSysplanSki;
use common\models\RfcZZappSelectSysplanSkiSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RfcZZappSelectSysplanSkiController implements the CRUD actions for RfcZZappSelectSysplanSki model.
 */
class RfcZZappSelectSysplanSkiController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RfcZZappSelectSysplanSki models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RfcZZappSelectSysplanSkiSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RfcZZappSelectSysplanSki model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RfcZZappSelectSysplanSki model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RfcZZappSelectSysplanSki();
        $model->LAST_UPDATE_BY=\yii::$app->user->identity->username;
        $model->DELETE_MARK=0;
        $model->CREATE_BY=\yii::$app->user->identity->username;
        $model->CREATE_DATE=date('Y-m-d');
        $model->LAST_UPDATE_DATE=date('Y-m-d');
        if ($model->load(Yii::$app->request->post())) {
            $model->save(false);
            \Yii::$app->getSession()->setFlash('success', 'Data Berhasil Disimpan');
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RfcZZappSelectSysplanSki model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->ID]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RfcZZappSelectSysplanSki model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RfcZZappSelectSysplanSki model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RfcZZappSelectSysplanSki the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RfcZZappSelectSysplanSki::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
