<?php
return [
    'adminEmail' => 'admin@sevensys.id',
    'supportEmail' => 'developprogram2122021@gmail.com',
    'user.passwordResetTokenExpire' => 3600,
];
