<?php

return [
    'name' => 'PT Biner Cipta Data',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'modules' => [
        'gridview' => [
            'class' => 'kartik\grid\Module'
        ],
        'redactor' => [
            'class' => 'yii\redactor\RedactorModule',
            'uploadDir' => '@webroot/uploads',
            'uploadUrl' => '@web/uploads',
            'imageAllowExtensions' => ['jpg', 'png', 'gif']
        ], 
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'dektrium\rbac\components\DbManager',
        ],
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'dateFormat' => 'dd-MM-yyyy',
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'currencyCode' => 'Rp. ',
            'nullDisplay' => '',
            'locale' => 'id_ID.utf8',
            //            'locale' => 'id_ID',
            'timeZone' => 'Asia/Jakarta',
            //'timeFormat' => 'php:H:i:s',
        ],
        'mailerGmail' => [
			'class' => 'yii\swiftmailer\Mailer',
			'viewPath' => '@common/mail',
			'useFileTransport' => false,

			'transport' => [
				'class' => 'Swift_SmtpTransport',
				'host' => 'smtp.gmail.com',
				'username' => 'developprogram2122021@gmail.com',
				'password' => 'FadlyBogel1995',
				'port' => '587',
				'encryption' => 'tls',
			],
		],
    ],
    'bootstrap' => ['log'],
];
