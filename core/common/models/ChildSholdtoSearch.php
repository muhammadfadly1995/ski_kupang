<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ChildSholdto;

/**
 * ChildSholdtoSearch represents the model behind the search form of `common\models\ChildSholdto`.
 */
class ChildSholdtoSearch extends ChildSholdto
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'ID_SHOLDTO', 'DISTRIK_CODE'], 'integer'],
            [['KODE_SHIPTO', 'NAMA_SHIPTO', 'ALAMAT', 'PHONE','NAMA_PEMILIK', 'EMAIL', 'CREATE_BY', 'UPDATE_BY', 'UPDATE_DATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChildSholdto::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'NILAI_JAMINAN' => $this->NILAI_JAMINAN,
            'ID_SHOLDTO' => $this->ID_SHOLDTO,
            'DISTRIK_CODE' => $this->DISTRIK_CODE,
            'UPDATE_DATE' => $this->UPDATE_DATE,
        ]);

        $query->andFilterWhere(['like', 'KODE_SHIPTO', $this->KODE_SHIPTO])
            ->andFilterWhere(['like', 'NAMA_SHIPTO', $this->NAMA_SHIPTO])
            ->andFilterWhere(['like', 'NAMA_PEMILIK', $this->NAMA_PEMILIK])
            ->andFilterWhere(['like', 'ALAMAT', $this->ALAMAT])
            ->andFilterWhere(['like', 'PHONE', $this->PHONE])
            ->andFilterWhere(['like', 'EMAIL', $this->EMAIL])
            ->andFilterWhere(['like', 'CREATE_BY', $this->CREATE_BY])
            ->andFilterWhere(['like', 'UPDATE_BY', $this->UPDATE_BY]);

        return $dataProvider;
    }
}
