<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data_truck".
 *
 * @property int $id
 * @property string $no_pol
 * @property int $id_type_truck
 * @property string $merek_truck
 * @property string $no_rfid
 * @property string $tahun_pembuatan
 * @property int $jumlah_ban
 * @property string $no_stnk
 * @property string $tanggal_berlaku_stnk
 * @property string $no_mesin
 * @property string $no_rangka
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class DataTruck extends \yii\db\ActiveRecord
{
    public $kode_plat,$nomor_plat,$seri_plat;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_truck';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_pol', 'id_type_truck', 'merek_truck', 'no_rfid','kapasitas_angkut','satuan','id_supir','id_expeditur', 'tahun_pembuatan', 'jumlah_ban', 'no_stnk', 'tanggal_berlaku_stnk', 'no_mesin', 'no_rangka', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['id_type_truck', 'jumlah_ban', 'delete_mark','kapasitas_angkut','status_free','id_expeditur','id_supir'], 'integer'],
            [['tanggal_berlaku_stnk', 'create_date', 'update_date','satuan'], 'safe'],
            [['no_pol', 'merek_truck', 'no_rfid', 'no_stnk', 'no_mesin', 'no_rangka', 'create_by', 'update_by'], 'string', 'max' => 255],
            [['kode_plat'], 'string', 'max' => 255],
            [['tahun_pembuatan'], 'string', 'max' => 4],
            [['nomor_plat'], 'integer'],
            [['seri_plat'], 'string', 'max' => 3],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_pol' => 'No Pol(Tanpa -,/ dan spasi)',
            'id_type_truck' => 'Id Type Truck',
            'merek_truck' => 'Merek Truck',
            'no_rfid' => 'No Rfid',
            'tahun_pembuatan' => 'Tahun Pembuatan',
            'jumlah_ban' => 'Jumlah Ban',
            'no_stnk' => 'No Stnk',
            'tanggal_berlaku_stnk' => 'Tanggal Berlaku Stnk',
            'no_mesin' => 'No Mesin',
            'no_rangka' => 'No Rangka',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
