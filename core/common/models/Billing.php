<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "billing".
 *
 * @property int $id
 * @property string $no_billing
 * @property float $subtotal
 * @property int $status_billing
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 * @property int $no_line
 */
class Billing extends \yii\db\ActiveRecord
{ public $cekbilling;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'billing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_billing', 'create_by', 'create_date', 'update_by', 'update_date', 'no_line'], 'required'],
            [['subtotal'], 'number'],
            [['status_billing', 'delete_mark', 'no_line','status_billing','id_top'], 'integer'],
            [['create_date', 'update_date','tanggal_billing'], 'safe'],
            [['no_billing', 'create_by', 'update_by','cekbilling','alasan_cancel'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_billing' => 'No Billing',
            'subtotal' => 'Subtotal',
            'status_billing' => 'Status Billing',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
            'no_line' => 'No Line',
        ];
    }
}
