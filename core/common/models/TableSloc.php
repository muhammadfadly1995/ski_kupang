<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "table_sloc".
 *
 * @property int $id
 * @property string $keterangan
 * @property int $delete_mark
 */
class TableSloc extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'table_sloc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['keterangan'], 'required'],
            [['delete_mark'], 'integer'],
            [['keterangan'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'keterangan' => 'Keterangan',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
