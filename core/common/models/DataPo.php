<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data_po".
 *
 * @property int $id
 * @property int $id_com
 * @property int $id_plant
 * @property string $no_po
 * @property int $vendor
 * @property string $no_pol
 * @property int $material
 * @property float $qty
 * @property int $sloc
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 * @property string $status
 */
class DataPo extends \yii\db\ActiveRecord
{
    public $no_po1;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_po';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_com', 'id_plant', 'no_po', 'vendor', 'material', 'qty', 'sloc', 'create_by', 'create_date', 'update_by', 'update_date', 'status'], 'required'],
            [['id_com', 'id_plant', 'vendor', 'material', 'sloc', 'delete_mark'], 'integer'],
            [['qty','line_order','sisa_qty'], 'number'],
            [['create_date', 'update_date','tanggal_datang'], 'safe'],
            [['no_po','no_po1' ,'no_pol', 'create_by', 'update_by', 'status','nama_kapal','vendor_pbm'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tanggal_datang'=>'Estimasi Kedatangan',
            'nama_kapal'=>'Nama Angkutan',
            'id_com' => 'Company',
            'id_plant' => 'Plant',
            'no_po' => 'No Po',
            'vendor' => 'Vendor',
            'no_pol' => 'No Pol',
            'material' => 'Material',
            'qty' => 'Qty (TO)',
            'sloc' => 'Sloc',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
            'status' => 'Status',
        ];
    }
}
