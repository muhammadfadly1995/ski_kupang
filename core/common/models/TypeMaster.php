<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "type_master".
 *
 * @property int $id
 * @property string $code_type
 * @property string $deskripsi
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class TypeMaster extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'type_master';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_type', 'deskripsi', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['create_date', 'update_date'], 'safe'],
            [['delete_mark'], 'integer'],
            [['code_type', 'deskripsi', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_type' => 'Code Type',
            'deskripsi' => 'Deskripsi',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
