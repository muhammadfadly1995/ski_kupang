<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DataTruck;

/**
 * DataTruckSearch represents the model behind the search form of `common\models\DataTruck`.
 */
class DataTruckSearch extends DataTruck
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_type_truck', 'jumlah_ban', 'delete_mark','id_expeditur','id_supir'], 'integer'],
            [['no_pol', 'merek_truck', 'no_rfid', 'tahun_pembuatan', 'no_stnk', 'tanggal_berlaku_stnk', 'no_mesin', 'no_rangka', 'create_by', 'create_date', 'update_by', 'update_date','kapasitas_angkut','satuan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataTruck::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_type_truck' => $this->id_type_truck,
            'jumlah_ban' => $this->jumlah_ban,
            'tanggal_berlaku_stnk' => $this->tanggal_berlaku_stnk,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
            'kapasitas_angkut' => $this->kapasitas_angkut,
        ]);

        $query->andFilterWhere(['like', 'no_pol', $this->no_pol])
            ->andFilterWhere(['like', 'merek_truck', $this->merek_truck])
            ->andFilterWhere(['like', 'no_rfid', $this->no_rfid])
            ->andFilterWhere(['like', 'tahun_pembuatan', $this->tahun_pembuatan])
            ->andFilterWhere(['like', 'no_stnk', $this->no_stnk])
            ->andFilterWhere(['like', 'no_mesin', $this->no_mesin])
            ->andFilterWhere(['like', 'no_rangka', $this->no_rangka])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
