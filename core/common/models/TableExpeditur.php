<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "table_expeditur".
 *
 * @property int $id
 * @property string $code_expeditur
 * @property string $deskripsi
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class TableExpeditur extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'table_expeditur';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_expeditur', 'deskripsi', 'create_by', 'create_date', 'update_by', 'update_date', 'delete_mark'], 'required'],
            [['create_date', 'update_date'], 'safe'],
            [['delete_mark'], 'integer'],
            [['code_expeditur', 'deskripsi', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_expeditur' => 'Code Expeditur',
            'deskripsi' => 'Deskripsi',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
