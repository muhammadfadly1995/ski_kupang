<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Billing;

/**
 * BillingSearch represents the model behind the search form of `common\models\Billing`.
 */
class BillingSearch extends Billing
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status_billing', 'delete_mark', 'no_line'], 'integer'],
            [['no_billing', 'create_by', 'create_date', 'update_by', 'update_date'], 'safe'],
            [['subtotal'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Billing::find()->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subtotal' => $this->subtotal,
            'status_billing' => $this->status_billing,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
            'no_line' => $this->no_line,
        ]);

        $query->andFilterWhere(['like', 'no_billing', $this->no_billing])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    public function searchcancel($params)
    {
        $query = Billing::find()->where(['delete_mark'=>0])->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'subtotal' => $this->subtotal,
            'status_billing' => $this->status_billing,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
            'no_line' => $this->no_line,
        ]);

        $query->andFilterWhere(['like', 'no_billing', $this->no_billing])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
