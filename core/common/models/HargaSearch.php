<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Harga;

/**
 * HargaSearch represents the model behind the search form of `common\models\Harga`.
 */
class HargaSearch extends Harga
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'HARGA', 'DELETE_MARK'], 'integer'],
            [['PLANT', 'SOLD_TO', 'SHIP_TO', 'DISTRIK', 'KODE', 'DESKRIPSI', 'VALID_FROM', 'VALID_TO', 'CREATE_BY', 'CREATE_DATE', 'LAST_UPDATE_BY', 'LAST_UPDATE_DATE','TAX_PPN','TAX2','TOTAL'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Harga::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'HARGA' => $this->HARGA,
            'VALID_FROM' => $this->VALID_FROM,
            'VALID_TO' => $this->VALID_TO,
            'CREATE_DATE' => $this->CREATE_DATE,
            'LAST_UPDATE_DATE' => $this->LAST_UPDATE_DATE,
            'DELETE_MARK' => $this->DELETE_MARK,
        ]);

        $query->andFilterWhere(['like', 'PLANT', $this->PLANT])
            ->andFilterWhere(['like', 'SOLD_TO', $this->SOLD_TO])
            ->andFilterWhere(['like', 'SHIP_TO', $this->SHIP_TO])
            ->andFilterWhere(['like', 'DISTRIK', $this->DISTRIK])
            ->andFilterWhere(['like', 'KODE', $this->KODE])
            ->andFilterWhere(['like', 'DESKRIPSI', $this->DESKRIPSI])
            ->andFilterWhere(['like', 'CREATE_BY', $this->CREATE_BY])
            ->andFilterWhere(['like', 'LAST_UPDATE_BY', $this->LAST_UPDATE_BY]);

        return $dataProvider;
    }
}
