<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master_tax".
 *
 * @property int $id
 * @property string $deskripsi
 * @property string $tax_code
 * @property float $nominal_tax
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class MasterTax extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_tax';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['deskripsi', 'tax_code', 'nominal_tax', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['nominal_tax'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['delete_mark'], 'integer'],
            [['deskripsi', 'tax_code', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'deskripsi' => 'Deskripsi',
            'tax_code' => 'Tax Code',
            'nominal_tax' => 'Nominal Tax',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
