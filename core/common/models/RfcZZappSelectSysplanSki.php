<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rfc_z_zapp_select_sysplan_ski".
 *
 * @property int $ID
 * @property string $XPARAM
 * @property string $WERKS
 * @property string $NAME1
 * @property string $CREATE_BY
 * @property string $CREATE_DATE
 * @property string $LAST_UPDATE_DATE
 * @property string $LAST_UPDATE_BY
 * @property int $DELETE_MARK
 */
class RfcZZappSelectSysplanSki extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rfc_z_zapp_select_sysplan_ski';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'WERKS', 'NAME1', 'CREATE_BY', 'CREATE_DATE', 'LAST_UPDATE_DATE', 'LAST_UPDATE_BY', 'DELETE_MARK'], 'required'],
            [['CREATE_DATE', 'LAST_UPDATE_DATE','ALAMAT','LONGITUDE','LATITUDE'], 'safe'],
            [['DELETE_MARK','ID_DISTRIK'], 'integer'],
            [[ 'WERKS', 'NAME1', 'CREATE_BY', 'LAST_UPDATE_BY'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
           // 'XPARAM' => 'Xparam',
            'WERKS' => 'Kode Plant',
            'NAME1' => 'Nama Plant',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'LAST_UPDATE_DATE' => 'Last Update Date',
            'LAST_UPDATE_BY' => 'Last Update By',
            'DELETE_MARK' => 'Delete Mark',
        ];
    }
}
