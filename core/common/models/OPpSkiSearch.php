<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OPpSki;

/**
 * OPpSkiSearch represents the model behind the search form of `common\models\OPpSki`.
 */
class OPpSkiSearch extends OPpSki
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'line_number', 'delete_mark', 'id_soldto'], 'integer'],
            [['order_type', 'order_number', 'create_by', 'create_date', 'update_by', 'update_date', 'nama_soldto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (\yii::$app->user->identity->role == 'Distributor' || \yii::$app->user->identity->role == 'Petugas Timbang') {
            $query = OPpSki::find()->orderBy('ID ASC')->where(['status_op' => 1])->andWhere(['delete_mark' => 0])->andWhere(['create_by' => \yii::$app->user->identity->username])->orderBy('ID DESC');
        } else {
            $query = OPpSki::find()->orderBy('ID ASC')->where(['status_op' => 1])->andWhere(['delete_mark' => 0])->orderBy('ID DESC');
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'line_number' => $this->line_number,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
        ]);

        $query->andFilterWhere(['like', 'order_type', $this->order_type])
            ->andFilterWhere(['like', 'order_number', $this->order_number])
            ->andFilterWhere(['like', 'nama_soldto', $this->nama_soldto])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    public function searchcancel($params)
    {
        if (\yii::$app->user->identity->role == 'Distributor' || \yii::$app->user->identity->role == 'Petugas Timbang') {
            $query = OPpSki::find()->orderBy('ID ASC')->where(['status_op' => 1])->andWhere(['delete_mark' => 0])->andWhere(['create_by' => \yii::$app->user->identity->username])->orderBy('ID DESC');
        } else {
            $query = OPpSki::find()->orderBy('ID ASC')->where(['status_op' => 1])->andWhere(['delete_mark' => 0])->orderBy('ID DESC');
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'line_number' => $this->line_number,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
        ]);

        $query->andFilterWhere(['like', 'order_type', $this->order_type])
            ->andFilterWhere(['like', 'order_number', $this->order_number])
            ->andFilterWhere(['like', 'nama_soldto', $this->nama_soldto])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    public function searchpp($params)
    {
        if (\yii::$app->user->identity->role == 'Distributor' || \yii::$app->user->identity->role == 'Petugas Timbang') {
            $query = OPpSki::find()->where(['create_by' => \yii::$app->user->identity->username])->andWhere(['create_by' => \yii::$app->user->identity->username])->orderBy('ID DESC');
        } else {
            $query = OPpSki::find()->where(['create_by' => \yii::$app->user->identity->username])->orderBy('ID DESC');
        }
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'line_number' => $this->line_number,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
        ]);

        $query->andFilterWhere(['like', 'order_type', $this->order_type])
            ->andFilterWhere(['like', 'order_number', $this->order_number])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
