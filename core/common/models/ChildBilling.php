<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "child_billing".
 *
 * @property int $id
 * @property int $id_billing
 * @property int $no_spj
 * @property float $subtotal
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class ChildBilling extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'child_billing';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_billing', 'no_spj', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['id_billing', 'no_spj', 'delete_mark'], 'integer'],
            [['subtotal'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_billing' => 'Id Billing',
            'no_spj' => 'No Spj',
            'subtotal' => 'Subtotal',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
