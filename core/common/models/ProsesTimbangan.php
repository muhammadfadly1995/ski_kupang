<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "proses_timbangan".
 *
 * @property int $id
 * @property string $no_spj
 * @property string $id_matching
 * @property int $id_conveyor
 * @property float $total_nilai
 * @property float $nilai_masuk
 * @property float $nilai_keluar
 * @property float $nilai_akhir
 * @property string $tanggal_masuk
 * @property string|null $tanggal_keluar
 * @property string $jam_masuk
 * @property string|null $jam_keluar
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 * @property int $line_number
 */
class ProsesTimbangan extends \yii\db\ActiveRecord
{
    public $nama_supir,$data_truck;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proses_timbangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_spj', 'id_matching', 'id_conveyor', 'total_nilai', 'nilai_masuk', 'tanggal_masuk', 'jam_masuk', 'create_by', 'create_date', 'update_by', 'update_date', 'line_number'], 'required'],
            [['id_conveyor', 'delete_mark', 'line_number','id_c_pp'], 'integer'],
            [['total_nilai', 'nilai_masuk', 'nilai_keluar', 'nilai_akhir'], 'number'],
            [['tanggal_masuk', 'tanggal_keluar', 'jam_masuk', 'jam_keluar', 'create_date', 'update_date'], 'safe'],
            [['no_spj', 'id_matching', 'create_by', 'update_by','alasan_cancel'], 'string', 'max' => 255],
            [['nama_supir', 'data_truck'], 'string', 'max' => 255],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_spj' => 'No Spj',
            'id_matching' => 'Id Matching',
            'id_conveyor' => 'Id Conveyor',
            'total_nilai' => 'Total Nilai',
            'nilai_masuk' => 'Nilai Masuk',
            'nilai_keluar' => 'Nilai Keluar',
            'nilai_akhir' => 'Nilai Akhir',
            'tanggal_masuk' => 'Tanggal Masuk',
            'tanggal_keluar' => 'Tanggal Keluar',
            'jam_masuk' => 'Jam Masuk',
            'jam_keluar' => 'Jam Keluar',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
            'line_number' => 'Line Number',
        ];
    }
}
