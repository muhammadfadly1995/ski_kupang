<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master_top".
 *
 * @property int $id
 * @property string $code_top
 * @property string $deskripsi
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class MasterTop extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_top';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_top', 'deskripsi', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['create_date', 'update_date'], 'safe'],
            [['delete_mark'], 'integer'],
            [['code_top', 'deskripsi', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_top' => 'Code Top',
            'deskripsi' => 'Deskripsi',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
