<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PtMasterDistrikSki;

/**
 * PtMasterDistrikSkiSearch represents the model behind the search form of `common\models\PtMasterDistrikSki`.
 */
class PtMasterDistrikSkiSearch extends PtMasterDistrikSki
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'DELETE_MARK'], 'integer'],
            [['KODE_DISTRIK', 'DISTRIK', 'CREATE_BY', 'CREATE_DATE', 'LAST_UPDATE_BY', 'LAST_UPDATE', 'PARENT_CODE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PtMasterDistrikSki::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'CREATE_DATE' => $this->CREATE_DATE,
            'LAST_UPDATE' => $this->LAST_UPDATE,
            'DELETE_MARK' => $this->DELETE_MARK,
        ]);

        $query->andFilterWhere(['like', 'KODE_DISTRIK', $this->KODE_DISTRIK])
            ->andFilterWhere(['like', 'KODE_DISTRIK', $this->PARENT_CODE])
            ->andFilterWhere(['like', 'DISTRIK', $this->DISTRIK])
            ->andFilterWhere(['like', 'CREATE_BY', $this->CREATE_BY])
            ->andFilterWhere(['like', 'LAST_UPDATE_BY', $this->LAST_UPDATE_BY]);

        return $dataProvider;
    }
}
