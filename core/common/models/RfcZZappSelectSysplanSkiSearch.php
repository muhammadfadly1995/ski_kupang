<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RfcZZappSelectSysplanSki;

/**
 * RfcZZappSelectSysplanSkiSearch represents the model behind the search form of `common\models\RfcZZappSelectSysplanSki`.
 */
class RfcZZappSelectSysplanSkiSearch extends RfcZZappSelectSysplanSki
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'DELETE_MARK','ID_DISTRIK'], 'integer'],
            [[ 'WERKS', 'NAME1', 'CREATE_BY', 'CREATE_DATE', 'LAST_UPDATE_DATE', 'LAST_UPDATE_BY','ALAMAT','LONGITUDE','LATITUDE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RfcZZappSelectSysplanSki::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'CREATE_DATE' => $this->CREATE_DATE,
            'ID_DISTRIK' => $this->ID_DISTRIK,
            'LAST_UPDATE_DATE' => $this->LAST_UPDATE_DATE,
            'DELETE_MARK' => $this->DELETE_MARK,
        ]);

        $query
            ->andFilterWhere(['like', 'WERKS', $this->WERKS])
            ->andFilterWhere(['like', 'WERKS', $this->ALAMAT])
            ->andFilterWhere(['like', 'WERKS', $this->LONGITUDE])
            ->andFilterWhere(['like', 'WERKS', $this->LATITUDE])
         
            ->andFilterWhere(['like', 'NAME1', $this->NAME1])
            ->andFilterWhere(['like', 'CREATE_BY', $this->CREATE_BY])
            ->andFilterWhere(['like', 'LAST_UPDATE_BY', $this->LAST_UPDATE_BY]);

        return $dataProvider;
    }
}
