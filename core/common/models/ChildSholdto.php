<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "child_sholdto".
 *
 * @property int $ID
 * @property int $ID_SHOLDTO
 * @property string $KODE_SHIPTO
 * @property string $NAMA_SHIPTO
 * @property string $ALAMAT
 * @property int $DISTRIK_CODE
 * @property string $PHONE
 * @property string $EMAIL
 * @property string $CREATE_BY
 * @property string $UPDATE_BY
 * @property string $UPDATE_DATE
 */
class ChildSholdto extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'child_sholdto';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID_SHOLDTO', 'KODE_SHIPTO', 'NAMA_SHIPTO','NAMA_PEMILIK', 'ALAMAT', 'DISTRIK_CODE', 'PHONE', 'EMAIL', 'CREATE_BY', 'UPDATE_BY', 'UPDATE_DATE'], 'required'],
            [['ID_SHOLDTO', 'DISTRIK_CODE','NILAI_JAMINAN'], 'integer'],
            [['ALAMAT'], 'string'],
            [['UPDATE_DATE'], 'safe'],
            [['KODE_SHIPTO', 'NAMA_SHIPTO', 'PHONE', 'EMAIL', 'CREATE_BY', 'UPDATE_BY','NAMA_PEMILIK'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'ID_SHOLDTO' => 'Id Sholdto',
            'KODE_SHIPTO' => 'Kode Shipto',
            'NAMA_SHIPTO' => 'Nama Shipto',
            'ALAMAT' => 'Alamat',
            'DISTRIK_CODE' => 'Distrik Code',
            'PHONE' => 'Phone',
            'EMAIL' => 'Email',
            'CREATE_BY' => 'Create By',
            'UPDATE_BY' => 'Update By',
            'UPDATE_DATE' => 'Update Date',
        ];
    }
}
