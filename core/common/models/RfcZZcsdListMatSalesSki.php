<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rfc_z_zcsd_list_mat_sales_ski".
 *
 * @property int $ID
 * @property string $KODE_MATERIAL
 * @property string $DESKRIPSI
 * @property int $BERAT
 * @property string $UOM
 * @property string $CREATE_BY
 * @property string $CREATE_DATE
 * @property string $LAST_UPDATE_BY
 * @property string $LAST_UPDATE_DATE
 * @property int $DELETE_MARK
 */
class RfcZZcsdListMatSalesSki extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rfc_z_zcsd_list_mat_sales_ski';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KODE_MATERIAL', 'DESKRIPSI', 'BERAT', 'UOM', 'CREATE_BY', 'CREATE_DATE', 'LAST_UPDATE_BY', 'LAST_UPDATE_DATE', 'DELETE_MARK'], 'required'],
            [['DESKRIPSI','TYPE_MATERIAL'], 'string'],
            [['DELETE_MARK'], 'integer'],
            [['CREATE_DATE','BERAT','BERAT_KONFERSI'], 'safe'],
            [['KODE_MATERIAL', 'UOM', 'CREATE_BY', 'LAST_UPDATE_BY', 'LAST_UPDATE_DATE','UOM_KONFERSI'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'KODE_MATERIAL' => 'Kode Material',
            'DESKRIPSI' => 'Deskripsi',
            'BERAT' => 'Berat',
            'UOM' => 'Uom',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'LAST_UPDATE_BY' => 'Last Update By',
            'LAST_UPDATE_DATE' => 'Last Update Date',
            'DELETE_MARK' => 'Delete Mark',
        ];
    }
}
