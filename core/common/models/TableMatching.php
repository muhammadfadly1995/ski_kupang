<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "table_matching".
 *
 * @property int $id
 * @property string|null $no_matching
 * @property string $no_so
 * @property int $id_truck
 * @property int $id_supir
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 * @property int $line_number
 * @property int $id_expeditur
 * @property int $status_selesai
 * @property int|null $status_matching
 */
class TableMatching extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'table_matching';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_so', 'id_truck', 'id_supir', 'create_by', 'create_date', 'update_by', 'update_date', 'delete_mark', 'line_number', 'id_expeditur'], 'required'],
            [['delete_mark', 'line_number', 'id_expeditur', 'status_selesai', 'status_matching'], 'integer'],
            [['create_date', 'update_date','time_create','time_update'], 'safe'],
            [['no_matching', 'no_so', 'create_by', 'update_by','alasan_cancel','id_truck', 'id_supir', ], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_matching' => 'No Matching',
            'no_so' => 'No So',
            'id_truck' => 'Plat Truck',
            'id_supir' => 'Supir',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
            'line_number' => 'Line Number',
            'id_expeditur' => 'Id Expeditur',
            'status_selesai' => 'Status Selesai',
            'status_matching' => 'Status Matching',
        ];
    }
}
