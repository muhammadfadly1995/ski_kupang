<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "data_prod_sk".
 *
 * @property int $id
 * @property string $data_type
 * @property string $data_version
 * @property string $date_prod
 * @property int $material_code
 * @property string $remark
 * @property int $plant
 * @property float $stock_awal
 * @property float $recive
 * @property float $issued
 * @property float $stock_akhir
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class DataProdSk extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_prod_sk';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['data_type', 'data_version', 'date_prod', 'material_code', 'remark', 'plant', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['date_prod', 'create_date', 'update_date'], 'safe'],
            [['material_code', 'plant', 'delete_mark'], 'integer'],
            [['stock_awal', 'recive', 'issued', 'stock_akhir'], 'number'],
            [['data_type', 'data_version', 'remark', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'data_type' => 'Data Type',
            'data_version' => 'Data Version',
            'date_prod' => 'Date Prod',
            'material_code' => 'Material Code',
            'remark' => 'Remark',
            'plant' => 'Plant',
            'stock_awal' => 'Stock Awal',
            'recive' => 'Recive',
            'issued' => 'Issued',
            'stock_akhir' => 'Stock Akhir',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
