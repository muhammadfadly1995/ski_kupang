<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "timbangan_incoming".
 *
 * @property int $id
 * @property string $no_spj
 * @property int $id_po
 * @property string $tanggal_masuk
 * @property string|null $tanggal_keluar
 * @property string $jam_masuk
 * @property string|null $jam_keluar
 * @property float $nilai_masuk
 * @property float $nilai_keluar
 * @property float $subtotal
 * @property string $create_by
 * @property string $update_by
 * @property int $delete_mark
 * @property int $line_order
 */
class TimbanganIncoming extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timbangan_incoming';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_spj', 'id_po', 'tanggal_masuk', 'jam_masuk', 'nilai_masuk', 'create_by', 'update_by', 'line_order','no_pol'], 'required'],
            [['id_po', 'delete_mark', 'line_order'], 'integer'],
            [['tanggal_masuk', 'tanggal_keluar', 'jam_masuk', 'jam_keluar'], 'safe'],
            [['nilai_masuk', 'nilai_keluar', 'subtotal','qty_po'], 'number'],
            [['no_spj', 'create_by', 'update_by','no_pol','nama_supir'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_spj' => 'No Spj',
            'id_po' => 'No Po',
            'tanggal_masuk' => 'Tanggal Masuk',
            'tanggal_keluar' => 'Tanggal Keluar',
            'jam_masuk' => 'Jam Masuk',
            'jam_keluar' => 'Jam Keluar',
            'nilai_masuk' => 'Nilai Masuk',
            'nilai_keluar' => 'Nilai Keluar',
            'subtotal' => 'Subtotal',
            'create_by' => 'Create By',
            'update_by' => 'Update By',
            'delete_mark' => 'Delete Mark',
            'line_order' => 'Line Order',
        ];
    }
}
