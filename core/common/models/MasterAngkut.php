<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "master_angkut".
 *
 * @property int $id
 * @property string $code_angkut
 * @property string $deskripsi
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class MasterAngkut extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'master_angkut';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code_angkut', 'deskripsi', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['create_date', 'update_date'], 'safe'],
            [['delete_mark'], 'integer'],
            [['code_angkut', 'deskripsi', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_angkut' => 'Code Angkut',
            'deskripsi' => 'Deskripsi',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
