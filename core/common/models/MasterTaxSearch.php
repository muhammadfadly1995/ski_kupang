<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MasterTax;

/**
 * MasterTaxSearch represents the model behind the search form of `common\models\MasterTax`.
 */
class MasterTaxSearch extends MasterTax
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'delete_mark'], 'integer'],
            [['deskripsi', 'tax_code', 'create_by', 'create_date', 'update_by', 'update_date'], 'safe'],
            [['nominal_tax'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = MasterTax::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'nominal_tax' => $this->nominal_tax,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
        ]);

        $query->andFilterWhere(['like', 'deskripsi', $this->deskripsi])
            ->andFilterWhere(['like', 'tax_code', $this->tax_code])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
