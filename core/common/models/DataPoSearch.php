<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DataPo;

/**
 * DataPoSearch represents the model behind the search form of `common\models\DataPo`.
 */
class DataPoSearch extends DataPo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_com', 'id_plant', 'vendor', 'material', 'sloc', 'delete_mark'], 'integer'],
            [['no_po', 'no_pol', 'create_by', 'create_date','vendor_pbm', 'update_by', 'update_date', 'status','sisa_qty','tanggal_datang','nama_kapal'], 'safe'],
            [['qty'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DataPo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_com' => $this->id_com,
            'id_plant' => $this->id_plant,
            'vendor' => $this->vendor,
            'material' => $this->material,
            'qty' => $this->qty,
            'sloc' => $this->sloc,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
        ]);

        $query->andFilterWhere(['like', 'no_po', $this->no_po])
            ->andFilterWhere(['like', 'no_pol', $this->no_pol])
            ->andFilterWhere(['like', 'nama_kapal', $this->nama_kapal])
            ->andFilterWhere(['like', 'vendor_pbm', $this->vendor_pbm])
            ->andFilterWhere(['like', 'tanggal_datang', $this->tanggal_datang])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
