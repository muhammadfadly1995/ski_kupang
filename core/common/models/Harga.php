<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "harga".
 *
 * @property int $ID
 * @property string $PLANT
 * @property string $SOLD_TO
 * @property string $SHIP_TO
 * @property string $DISTRIK
 * @property string $KODE
 * @property string $DESKRIPSI
 * @property int $HARGA
 * @property string $VALID_FROM
 * @property string $VALID_TO
 * @property string $CREATE_BY
 * @property string $CREATE_DATE
 * @property string $LAST_UPDATE_BY
 * @property string $LAST_UPDATE_DATE
 * @property int $DELETE_MARK
 */
class Harga extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'harga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PLANT', 'SOLD_TO', 'SHIP_TO', 'DISTRIK', 'KODE', 'DESKRIPSI', 'HARGA', 'VALID_FROM', 'VALID_TO', 'CREATE_BY', 'CREATE_DATE', 'LAST_UPDATE_BY', 'LAST_UPDATE_DATE', 'DELETE_MARK'], 'required'],
            [['DESKRIPSI'], 'string'],
            [['HARGA', 'DELETE_MARK'], 'integer'],
            [['VALID_FROM', 'VALID_TO', 'CREATE_DATE', 'LAST_UPDATE_DATE','TAX_PPN','TAX2','TOTAL'], 'safe'],
            [['PLANT', 'SOLD_TO', 'SHIP_TO', 'DISTRIK', 'KODE', 'CREATE_BY', 'LAST_UPDATE_BY'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'PLANT' => 'Plant',
            'SOLD_TO' => 'Sold To',
            'SHIP_TO' => 'Ship To',
            'DISTRIK' => 'Distrik',
            'KODE' => 'Kode',
            'DESKRIPSI' => 'Deskripsi',
            'HARGA' => 'Harga',
            'VALID_FROM' => 'Valid From',
            'VALID_TO' => 'Valid To',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'LAST_UPDATE_BY' => 'Last Update By',
            'LAST_UPDATE_DATE' => 'Last Update Date',
            'DELETE_MARK' => 'Delete Mark',
        ];
    }
}
