<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "staging_stock".
 *
 * @property int $id
 * @property int $id_plant
 * @property int $id_mat
 * @property float $qty
 * @property string $last_update
 */
class StagingStock extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'staging_stock';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_plant', 'id_mat', 'qty', 'last_update'], 'required'],
            [['id_plant', 'id_mat'], 'integer'],
            [['qty'], 'number'],
            [['last_update'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_plant' => 'Id Plant',
            'id_mat' => 'Id Mat',
            'qty' => 'Qty',
            'last_update' => 'Last Update',
        ];
    }
}
