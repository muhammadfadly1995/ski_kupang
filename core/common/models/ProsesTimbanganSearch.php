<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProsesTimbangan;

/**
 * ProsesTimbanganSearch represents the model behind the search form of `common\models\ProsesTimbangan`.
 */
class ProsesTimbanganSearch extends ProsesTimbangan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_conveyor', 'delete_mark', 'line_number'], 'integer'],
            [['no_spj', 'id_matching', 'tanggal_masuk', 'tanggal_keluar', 'jam_masuk', 'jam_keluar', 'create_by', 'create_date', 'update_by', 'update_date','nama_supir','data_truck'], 'safe'],
            [['total_nilai', 'nilai_masuk', 'nilai_keluar', 'nilai_akhir'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProsesTimbangan::find()->where(['>','nilai_keluar',0])->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_conveyor' => $this->id_conveyor,
            'total_nilai' => $this->total_nilai,
            'nilai_masuk' => $this->nilai_masuk,
            'nilai_keluar' => $this->nilai_keluar,
            'nilai_akhir' => $this->nilai_akhir,
            'tanggal_masuk' => $this->tanggal_masuk,
            'tanggal_keluar' => $this->tanggal_keluar,
            'jam_masuk' => $this->jam_masuk,
            'jam_keluar' => $this->jam_keluar,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
            'line_number' => $this->line_number,
        ]);
        $datamatching=TableMatching::find()->where(['no_matching'=>$this->id_matching])->one();
        if($datamatching){
           $dataid=$datamatching->id;
        }
        $datasupir=TableMatching::find()->where(['id_supir'=>$this->nama_supir])->one();
        if($datasupir){
           $dataid=$datasupir->id;
        }
        $datatruck=TableMatching::find()->where(['id_truck'=>$this->data_truck])->one();
        if($datatruck){
           $dataid=$datatruck->id;
        }
       
       
       if($datamatching || $datatruck || $datasupir){
           $query->andFilterWhere(['like', 'no_spj', $this->no_spj])
         ->andFilterWhere(['like', 'id_matching', $dataid])
          //  ->andFilterWhere(['like', 'id_matching', $this->id_matching])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);
        }else{
            $query->andFilterWhere(['like', 'no_spj', $this->no_spj])
            //   ->andFilterWhere(['like', 'id_matching', $datamatching->id])
            ->andFilterWhere(['like', 'id_matching', $this->id_matching])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);
        }
       

        return $dataProvider;
    }
    public function searchin($params)
    {
        $query = ProsesTimbangan::find()->where(['nilai_keluar'=>0])->andWhere(['nilai_akhir'=>0])->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_conveyor' => $this->id_conveyor,
            'total_nilai' => $this->total_nilai,
            'nilai_masuk' => $this->nilai_masuk,
            'nilai_keluar' => $this->nilai_keluar,
            'nilai_akhir' => $this->nilai_akhir,
            'tanggal_masuk' => $this->tanggal_masuk,
            'tanggal_keluar' => $this->tanggal_keluar,
            'jam_masuk' => $this->jam_masuk,
            'jam_keluar' => $this->jam_keluar,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
            'line_number' => $this->line_number,
        ]);
         $datamatching=TableMatching::find()->where(['no_matching'=>$this->id_matching])->one();
         if($datamatching){
            $dataid=$datamatching->id;
         }
         $datasupir=TableMatching::find()->where(['id_supir'=>$this->nama_supir])->one();
         if($datasupir){
            $dataid=$datasupir->id;
         }
         $datatruck=TableMatching::find()->where(['id_truck'=>$this->data_truck])->one();
         if($datatruck){
            $dataid=$datatruck->id;
         }
        
        
        if($datamatching || $datatruck || $datasupir){
            $query->andFilterWhere(['like', 'no_spj', $this->no_spj])
          ->andFilterWhere(['like', 'id_matching', $dataid])
          //  ->andFilterWhere(['like', 'id_matching', $this->id_matching])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);
        }else{
            $query->andFilterWhere(['like', 'no_spj', $this->no_spj])
            //   ->andFilterWhere(['like', 'id_matching', $datamatching->id])
            ->andFilterWhere(['like', 'id_matching', $this->id_matching])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);
        }
       

        return $dataProvider;
    }
    public function searchcancel($params)
    {
        $query = ProsesTimbangan::find()->where(['delete_mark'=>0])->andWhere(['nilai_keluar'=>0])->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_conveyor' => $this->id_conveyor,
            'total_nilai' => $this->total_nilai,
            'nilai_masuk' => $this->nilai_masuk,
            'nilai_keluar' => $this->nilai_keluar,
            'nilai_akhir' => $this->nilai_akhir,
            'tanggal_masuk' => $this->tanggal_masuk,
            'tanggal_keluar' => $this->tanggal_keluar,
            'jam_masuk' => $this->jam_masuk,
            'jam_keluar' => $this->jam_keluar,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
            'line_number' => $this->line_number,
        ]);
        $datamatching=TableMatching::find()->where(['no_matching'=>$this->id_matching])->one();
        if($datamatching){
           $dataid=$datamatching->id;
        }
        $datasupir=TableMatching::find()->where(['id_supir'=>$this->nama_supir])->one();
        if($datasupir){
           $dataid=$datasupir->id;
        }
        $datatruck=TableMatching::find()->where(['id_truck'=>$this->data_truck])->one();
        if($datatruck){
           $dataid=$datatruck->id;
        }
       
       
       if($datamatching || $datatruck || $datasupir){
           $query->andFilterWhere(['like', 'no_spj', $this->no_spj])
         ->andFilterWhere(['like', 'id_matching', $dataid])
          //  ->andFilterWhere(['like', 'id_matching', $this->id_matching])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);
        }else{
            $query->andFilterWhere(['like', 'no_spj', $this->no_spj])
            //   ->andFilterWhere(['like', 'id_matching', $datamatching->id])
            ->andFilterWhere(['like', 'id_matching', $this->id_matching])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);
        }
       

        return $dataProvider;
    }
}
