<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RfcZZcsdShiptoSki;

/**
 * RfcZZcsdShiptoSkiSearch represents the model behind the search form of `common\models\RfcZZcsdShiptoSki`.
 */
class RfcZZcsdShiptoSkiSearch extends RfcZZcsdShiptoSki
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'DELETE_MARK'], 'integer'],
            [['KODE_SOLDTO', 'NAMA_SOLDTO', 'KODE_SHIPTO','email', 'NAMA_SHIPTO', 'ALAMAT', 'KODE_DSITRIK', 'NAMA_DISTRIK', 'CREATE_BY', 'CREATE_DATE', 'LAST_UPDATE_BY', 'LAST_UPDATE_DATE','NILAI_JAMINAN'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RfcZZcsdShiptoSki::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'CREATE_DATE' => $this->CREATE_DATE,
            'LAST_UPDATE_DATE' => $this->LAST_UPDATE_DATE,
            'NILAI_JAMINAN' => $this->NILAI_JAMINAN,
            'DELETE_MARK' => $this->DELETE_MARK,
        ]);

        $query->andFilterWhere(['like', 'KODE_SOLDTO', $this->KODE_SOLDTO])
        ->andFilterWhere(['like', 'NAMA_PEMILIK', $this->NAMA_PEMILIK])
            ->andFilterWhere(['like', 'NAMA_SOLDTO', $this->NAMA_SOLDTO])
            ->andFilterWhere(['like', 'KODE_SHIPTO', $this->KODE_SHIPTO])
            ->andFilterWhere(['like', 'NAMA_SHIPTO', $this->NAMA_SHIPTO])
            ->andFilterWhere(['like', 'ALAMAT', $this->ALAMAT])
            ->andFilterWhere(['like', 'KODE_DSITRIK', $this->KODE_DSITRIK])
            ->andFilterWhere(['like', 'NAMA_DISTRIK', $this->NAMA_DISTRIK])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'CREATE_BY', $this->CREATE_BY])
            ->andFilterWhere(['like', 'LAST_UPDATE_BY', $this->LAST_UPDATE_BY]);

        return $dataProvider;
    }
}
