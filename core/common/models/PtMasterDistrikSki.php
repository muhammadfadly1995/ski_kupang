<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pt_master_distrik_ski".
 *
 * @property int $ID
 * @property string $KODE_DISTRIK
 * @property string $DISTRIK
 * @property string $CREATE_BY
 * @property string $CREATE_DATE
 * @property string $LAST_UPDATE_BY
 * @property string $LAST_UPDATE
 * @property int $DELETE_MARK
 */
class PtMasterDistrikSki extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pt_master_distrik_ski';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KODE_DISTRIK', 'DISTRIK', 'CREATE_BY', 'CREATE_DATE', 'LAST_UPDATE_BY', 'LAST_UPDATE', 'DELETE_MARK'], 'required'],
            [['CREATE_DATE', 'LAST_UPDATE'], 'safe'],
            [['DELETE_MARK'], 'integer'],
            [['KODE_DISTRIK', 'DISTRIK', 'CREATE_BY', 'LAST_UPDATE_BY','PARENT_CODE'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'KODE_DISTRIK' => 'Kode Distrik',
            'DISTRIK' => 'Distrik',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'LAST_UPDATE_BY' => 'Last Update By',
            'LAST_UPDATE' => 'Last Update',
            'DELETE_MARK' => 'Delete Mark',
        ];
    }
}
