<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\DataProdSk;

/**
 * DataProdSkSearch represents the model behind the search form of `common\models\DataProdSk`.
 */
class DataProdSkSearch extends DataProdSk
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'material_code', 'plant', 'delete_mark'], 'integer'],
            [['data_type', 'data_version', 'date_prod', 'remark', 'create_by', 'create_date', 'update_by', 'update_date'], 'safe'],
            [['stock_awal', 'recive', 'issued', 'stock_akhir'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(\yii::$app->user->identity->role!='admin'){
            $query = DataProdSk::find()->where(['data_version'=>\yii::$app->user->identity->company])->orderBy('id DESC');
        }else{
            $query = DataProdSk::find()->orderBy('id DESC');
        }
       

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_prod' => $this->date_prod,
            'material_code' => $this->material_code,
            'plant' => $this->plant,
            'stock_awal' => $this->stock_awal,
            'recive' => $this->recive,
            'issued' => $this->issued,
            'stock_akhir' => $this->stock_akhir,
            'create_date' => $this->create_date,
            'update_date' => $this->update_date,
            'delete_mark' => $this->delete_mark,
        ]);

        $query->andFilterWhere(['like', 'data_type', $this->data_type])
            ->andFilterWhere(['like', 'data_version', $this->data_version])
            ->andFilterWhere(['like', 'remark', $this->remark])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
