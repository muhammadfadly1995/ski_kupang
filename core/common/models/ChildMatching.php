<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "child_matching".
 *
 * @property int $id
 * @property string $no_do
 * @property int $id_matching
 * @property int $id_so
 * @property int $id_child_so
 * @property float $quantity
 * @property int $status
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 * @property int $line_order
 */
class ChildMatching extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'child_matching';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_do', 'id_matching', 'id_so', 'id_child_so', 'quantity', 'status', 'create_by', 'create_date', 'update_by', 'update_date', 'line_order'], 'required'],
            [['id_matching', 'id_so', 'id_child_so', 'status', 'delete_mark', 'line_order','id_tax'], 'integer'],
            [['quantity','harga','subtotal'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['no_do', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_do' => 'No Do',
            'id_matching' => 'Id Matching',
            'id_so' => 'Id So',
            'id_child_so' => 'Id Child So',
            'quantity' => 'Quantity',
            'status' => 'Status',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
            'line_order' => 'Line Order',
        ];
    }
}
