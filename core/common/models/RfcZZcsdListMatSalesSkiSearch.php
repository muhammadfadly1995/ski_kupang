<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RfcZZcsdListMatSalesSki;

/**
 * RfcZZcsdListMatSalesSkiSearch represents the model behind the search form of `common\models\RfcZZcsdListMatSalesSki`.
 */
class RfcZZcsdListMatSalesSkiSearch extends RfcZZcsdListMatSalesSki
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'BERAT', 'DELETE_MARK'], 'integer'],
            [['KODE_MATERIAL', 'DESKRIPSI', 'UOM', 'CREATE_BY','TYPE_MATERIAL', 'CREATE_DATE', 'LAST_UPDATE_BY', 'LAST_UPDATE_DATE','BERAT_KONFERSI','UOM_KONFERSI'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RfcZZcsdListMatSalesSki::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'BERAT' => $this->BERAT,
            'CREATE_DATE' => $this->CREATE_DATE,
            'DELETE_MARK' => $this->DELETE_MARK,
        ]);

        $query->andFilterWhere(['like', 'KODE_MATERIAL', $this->TYPE_MATERIAL])
            ->andFilterWhere(['like', 'KODE_MATERIAL', $this->TYPE_MATERIAL])
            ->andFilterWhere(['like', 'DESKRIPSI', $this->DESKRIPSI])
            ->andFilterWhere(['like', 'UOM', $this->UOM])
            ->andFilterWhere(['like', 'CREATE_BY', $this->CREATE_BY])
            ->andFilterWhere(['like', 'LAST_UPDATE_BY', $this->LAST_UPDATE_BY])
            ->andFilterWhere(['like', 'LAST_UPDATE_DATE', $this->LAST_UPDATE_DATE]);

        return $dataProvider;
    }
}
