<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TimbanganIncoming;

/**
 * TimbanganIncomingSearch represents the model behind the search form of `common\models\TimbanganIncoming`.
 */
class TimbanganIncomingSearch extends TimbanganIncoming
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_po', 'delete_mark', 'line_order'], 'integer'],
            [['no_spj', 'tanggal_masuk', 'tanggal_keluar', 'jam_masuk', 'jam_keluar', 'create_by', 'update_by','no_pol','nama_supir'], 'safe'],
            [['nilai_masuk', 'nilai_keluar', 'subtotal','qty_po'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimbanganIncoming::find()->where(['>','nilai_keluar',0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_po' => $this->id_po,
            'tanggal_masuk' => $this->tanggal_masuk,
            'tanggal_keluar' => $this->tanggal_keluar,
            'jam_masuk' => $this->jam_masuk,
            'jam_keluar' => $this->jam_keluar,
            'nilai_masuk' => $this->nilai_masuk,
            'nilai_keluar' => $this->nilai_keluar,
            'subtotal' => $this->subtotal,
            'delete_mark' => $this->delete_mark,
            'line_order' => $this->line_order,
        ]);

        $query->andFilterWhere(['like', 'no_spj', $this->no_spj])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    public function searchin($params)
    {
        $query = TimbanganIncoming::find()->where(['<','nilai_keluar',1])->andWhere(['>','nilai_masuk',0]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_po' => $this->id_po,
            'tanggal_masuk' => $this->tanggal_masuk,
            'tanggal_keluar' => $this->tanggal_keluar,
            'jam_masuk' => $this->jam_masuk,
            'jam_keluar' => $this->jam_keluar,
            'nilai_masuk' => $this->nilai_masuk,
            'nilai_keluar' => $this->nilai_keluar,
            'subtotal' => $this->subtotal,
            'delete_mark' => $this->delete_mark,
            'line_order' => $this->line_order,
        ]);

        $query->andFilterWhere(['like', 'no_spj', $this->no_spj])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'no_pol', $this->no_pol])
            ->andFilterWhere(['like', 'nama_supir', $this->nama_supir])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
