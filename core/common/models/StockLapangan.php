<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "stock_lapangan".
 *
 * @property int $id
 * @property int $id_mat
 * @property string $satuan
 * @property float $jumlah
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class StockLapangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'stock_lapangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_mat', 'satuan', 'jumlah', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['id_mat', 'delete_mark'], 'integer'],
            [['jumlah'], 'number'],
            [['create_date', 'update_date'], 'safe'],
            [['satuan', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_mat' => ' Material',
            'satuan' => 'Satuan',
            'jumlah' => 'Qty',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
