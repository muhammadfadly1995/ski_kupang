<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "child_o_pp_ski".
 *
 * @property int $ID
 * @property int $id_pp
 * @property int $id_soldto
 * @property int $id_shipto
 * @property int $id_plant
 * @property int $id_material
 * @property int $id_angkut
 * @property float $quantity
 * @property string $date_order
 * @property string $date_receipt
 * @property int $id_top
 * @property float $harga
 * @property int $id_tax
 * @property float $subtotal
 * @property string $create_by
 * @property string $create_date
 * @property string|null $approval_by
 * @property string|null $approval_date
 * @property string|null $approval_reject
 * @property string|null $approval_date_reject
 * @property int $delete_mark
 * @property string $status_konfirmasi
 * @property string $uom
 */
class ChildOPpSki extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'child_o_pp_ski';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_pp', 'id_soldto', 'id_shipto', 'id_plant', 'id_material', 'id_angkut', 'quantity', 'date_order', 'date_receipt', 'id_top', 'harga', 'id_tax', 'subtotal', 'create_by', 'create_date', 'uom'], 'required'],
            [['id_pp', 'id_soldto', 'id_shipto', 'id_plant', 'id_material', 'id_angkut', 'id_top', 'id_tax', 'delete_mark','line_number'], 'integer'],
            [['quantity', 'harga', 'subtotal','sisa_quantity'], 'number'],
            [['date_order', 'date_receipt', 'create_date', 'approval_date', 'approval_date_reject','time_order','time_create'], 'safe'],
            [['create_by', 'approval_by', 'approval_reject', 'status_konfirmasi', 'uom'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'id_pp' => 'Id Pp',
            'id_soldto' => 'Id Soldto',
            'id_shipto' => 'Id Shipto',
            'id_plant' => 'Id Plant',
            'id_material' => 'Id Material',
            'id_angkut' => 'Id Angkut',
            'quantity' => 'Quantity',
            'date_order' => 'Date Order',
            'date_receipt' => 'Date Receipt',
            'id_top' => 'Id Top',
            'harga' => 'Harga',
            'id_tax' => 'Id Tax',
            'subtotal' => 'Subtotal',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'approval_by' => 'Approval By',
            'approval_date' => 'Approval Date',
            'approval_reject' => 'Approval Reject',
            'approval_date_reject' => 'Approval Date Reject',
            'delete_mark' => 'Delete Mark',
            'status_konfirmasi' => 'Status Konfirmasi',
            'uom' => 'Uom',
        ];
    }
}
