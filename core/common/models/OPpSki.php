<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "o_pp_ski".
 *
 * @property int $ID
 * @property string $order_type
 * @property string $order_number
 * @property int $line_number
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class OPpSki extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'o_pp_ski';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_type', 'order_number', 'line_number', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['line_number', 'delete_mark','status_op','id_soldto','status_timbangan_keluar'], 'integer'],
            [['create_date', 'update_date','time_create','time_update'], 'safe'],
            [['order_type', 'create_by', 'update_by','nama_soldto','alasan_cancel'], 'string', 'max' => 255],
            [['order_number'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'order_type' => 'SO Type',
            'order_number' => 'SO Number',
            'line_number' => 'Line Number',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
