<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TableMatching;

/**
 * TableMatchingSearch represents the model behind the search form of `common\models\TableMatching`.
 */
class TableMatchingSearch extends TableMatching
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_truck', 'delete_mark','id_expeditur'], 'integer'],
            [['no_matching', 'no_so', 'create_by', 'create_date', 'update_by', 'update_date','id_truck', 'id_supir'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TableMatching::find()->where(['status_selesai'=>0])->orderBy('id ASC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $datamatching=TableExpeditur::find()->where(['code_expeditur'=>$this->id_expeditur])->one();
        if($datamatching){
            $query->andFilterWhere([
                'id' => $this->id,
                'id_truck' => $this->id_truck,
                'id_expeditur' => $datamatching->id,
                'create_date' => $this->create_date,
                'update_date' => $this->update_date,
                'delete_mark' => $this->delete_mark,
            ]);
        }else{
            $query->andFilterWhere([
                'id' => $this->id,
                'id_truck' => $this->id_truck,
              //  'id_expeditur' => $datamatching->id,
                'create_date' => $this->create_date,
                'update_date' => $this->update_date,
                'delete_mark' => $this->delete_mark,
            ]);
        }
       
       
        $query->andFilterWhere(['like', 'no_matching', $this->no_matching])
            ->andFilterWhere(['like', 'no_so', $this->no_so])
            ->andFilterWhere(['like', 'id_expeditur', $this->id_expeditur])
            ->andFilterWhere(['like', 'id_truck', $this->id_truck])
            ->andFilterWhere(['like', 'id_supir', $this->id_supir])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    public function searchDone($params)
    {
        $query = TableMatching::find()->where(['status_selesai'=>1])->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $datamatching=TableExpeditur::find()->where(['code_expeditur'=>$this->id_expeditur])->one();
        if($datamatching){
            $query->andFilterWhere([
                'id' => $this->id,
                'id_truck' => $this->id_truck,
                'id_expeditur' => $datamatching->id,
                'create_date' => $this->create_date,
                'update_date' => $this->update_date,
                'delete_mark' => $this->delete_mark,
            ]);
        }else{
            $query->andFilterWhere([
                'id' => $this->id,
                'id_truck' => $this->id_truck,
              //  'id_expeditur' => $datamatching->id,
                'create_date' => $this->create_date,
                'update_date' => $this->update_date,
                'delete_mark' => $this->delete_mark,
            ]);
        }

        $query->andFilterWhere(['like', 'no_matching', $this->no_matching])
            ->andFilterWhere(['like', 'no_so', $this->no_so])
            ->andFilterWhere(['like', 'id_truck', $this->id_truck])
            ->andFilterWhere(['like', 'id_supir', $this->id_supir])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
    public function searchCancel($params)
    {
        $query = TableMatching::find()->where(['status_selesai'=>1])->andWhere(['delete_mark'=>0])->andWhere(['status_matching'=>20])->orderBy('id DESC');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $datamatching=TableExpeditur::find()->where(['code_expeditur'=>$this->id_expeditur])->one();
        if($datamatching){
            $query->andFilterWhere([
                'id' => $this->id,
                'id_truck' => $this->id_truck,
                'id_expeditur' => $datamatching->id,
                'create_date' => $this->create_date,
                'update_date' => $this->update_date,
                'delete_mark' => $this->delete_mark,
            ]);
        }else{
            $query->andFilterWhere([
                'id' => $this->id,
                'id_truck' => $this->id_truck,
              //  'id_expeditur' => $datamatching->id,
                'create_date' => $this->create_date,
                'update_date' => $this->update_date,
                'delete_mark' => $this->delete_mark,
            ]);
        }

        $query->andFilterWhere(['like', 'no_matching', $this->no_matching])
            ->andFilterWhere(['like', 'no_so', $this->no_so])
            ->andFilterWhere(['like', 'id_truck', $this->id_truck])
            ->andFilterWhere(['like', 'id_supir', $this->id_supir])
            ->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'update_by', $this->update_by]);

        return $dataProvider;
    }
}
