<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ChildOPpSki;

/**
 * ChildOPpSkiSearch represents the model behind the search form of `common\models\ChildOPpSki`.
 */
class ChildOPpSkiSearch extends ChildOPpSki
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ID', 'id_soldto', 'id_shipto', 'id_plant', 'id_material', 'id_angkut', 'id_top', 'id_tax', 'delete_mark', 'status_konfirmasi'], 'integer'],
            [['quantity', 'harga', 'subtotal'], 'number'],
            [['date_order', 'date_receipt', 'create_by', 'create_date', 'approval_by', 'approval_date', 'approval_reject', 'approval_date_reject'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChildOPpSki::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'ID' => $this->ID,
            'id_soldto' => $this->id_soldto,
            'id_shipto' => $this->id_shipto,
            'id_plant' => $this->id_plant,
            'id_material' => $this->id_material,
            'id_angkut' => $this->id_angkut,
            'quantity' => $this->quantity,
            'date_order' => $this->date_order,
            'date_receipt' => $this->date_receipt,
            'id_top' => $this->id_top,
            'harga' => $this->harga,
            'id_tax' => $this->id_tax,
            'subtotal' => $this->subtotal,
            'create_date' => $this->create_date,
            'approval_date' => $this->approval_date,
            'approval_date_reject' => $this->approval_date_reject,
            'delete_mark' => $this->delete_mark,
            'status_konfirmasi' => $this->status_konfirmasi,
        ]);

        $query->andFilterWhere(['like', 'create_by', $this->create_by])
            ->andFilterWhere(['like', 'approval_by', $this->approval_by])
            ->andFilterWhere(['like', 'approval_reject', $this->approval_reject]);

        return $dataProvider;
    }
}
