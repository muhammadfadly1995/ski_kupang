<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "email_user".
 *
 * @property int $id
 * @property string $nama_user
 * @property string $no_hp
 * @property string $email
 * @property string $create_by
 * @property string $create_date
 * @property string $update_by
 * @property string $update_date
 * @property int $delete_mark
 */
class EmailUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_user', 'no_hp', 'email', 'create_by', 'create_date', 'update_by', 'update_date'], 'required'],
            [['create_date', 'update_date'], 'safe'],
            [['delete_mark'], 'integer'],
            [['nama_user', 'no_hp', 'email', 'create_by', 'update_by'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nama_user' => 'Nama User',
            'no_hp' => 'No Hp',
            'email' => 'Email',
            'create_by' => 'Create By',
            'create_date' => 'Create Date',
            'update_by' => 'Update By',
            'update_date' => 'Update Date',
            'delete_mark' => 'Delete Mark',
        ];
    }
}
