<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "rfc_z_zcsd_shipto_ski".
 *
 * @property int $ID
 * @property string $KODE_SOLDTO
 * @property string $NAMA_SOLDTO
 * @property string $KODE_SHIPTO
 * @property string $NAMA_SHIPTO
 * @property string $ALAMAT
 * @property string $KODE_DSITRIK
 * @property string $NAMA_DISTRIK
 * @property string $CREATE_BY
 * @property string $CREATE_DATE
 * @property string $LAST_UPDATE_BY
 * @property string $LAST_UPDATE_DATE
 * @property int $DELETE_MARK
 */
class RfcZZcsdShiptoSki extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rfc_z_zcsd_shipto_ski';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['KODE_SOLDTO', 'NAMA_SOLDTO', 'ALAMAT', 'KODE_DSITRIK', 'NAMA_DISTRIK','NAMA_PEMILIK', 'CREATE_BY', 'CREATE_DATE', 'LAST_UPDATE_BY', 'LAST_UPDATE_DATE', 'DELETE_MARK'], 'required'],
            [['ALAMAT'], 'string'],
            [['CREATE_DATE', 'LAST_UPDATE_DATE','NILAI_JAMINAN','id_user','email'], 'safe'],
            [['DELETE_MARK'], 'integer'],
            [['KODE_SOLDTO', 'NAMA_SOLDTO', 'KODE_SHIPTO', 'NAMA_SHIPTO', 'KODE_DSITRIK', 'NAMA_DISTRIK', 'CREATE_BY', 'LAST_UPDATE_BY','NAMA_PEMILIK'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'KODE_SOLDTO' => 'Kode Soldto',
            'NAMA_SOLDTO' => 'Nama Soldto',
            'KODE_SHIPTO' => 'Kode Shipto',
            'NAMA_SHIPTO' => 'Nama Shipto',
            'ALAMAT' => 'Alamat',
            'KODE_DSITRIK' => 'Kode Dsitrik',
            'NAMA_DISTRIK' => 'Nama Distrik',
            'CREATE_BY' => 'Create By',
            'CREATE_DATE' => 'Create Date',
            'LAST_UPDATE_BY' => 'Last Update By',
            'LAST_UPDATE_DATE' => 'Last Update Date',
            'DELETE_MARK' => 'Delete Mark',
            'NILAI_JAMINAN'=>'Nilai Jaminan',
        ];
    }
}
